<?php

class AppController extends Controller
{
	// VARS
//	private $adminEmail = 'joemommabee@yahoo.com';
	private $adminEmail = 'lunarvision@gmail.com';
	private $devEmail = 'lunarvision@gmail.com';

	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('sendEmail',
					'calcStandings',
					'createFighter',
					'autoCreateByWiki',
					'createEvent',
					'createMatchup',
                    'getContents',
                    'sendEmail',
					'enrollUser'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(''),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'create','update','delete'),
				'users'=>array('admin', 'lunarvision@gmail.com'),
				'roles'=>array('1'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCreateFighter() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;
		$fighterId = 0;

		if(isset($_POST))
		{
			// post vars
			$firstName = $_POST['firstName'];
			$lastName = $_POST['lastName'];

			// fighter
			$fighterModel = new Fighter;
			$fighter = Fighter::model()->find(array(
				'condition'=>'firstName=:firstName AND lastName=:lastName',
				'params'=>array(
					':firstName'=>$firstName,
					':lastName'=>$lastName
				)
			));

			if($fighter) {
				$fighterId = $fighter->id;
				$message = '[duplicate] Skipping... ' .$fighter->firstName . ' '. $fighter->lastName;
			} else {
				$fighterModel->firstName = $firstName;
				$fighterModel->lastName = $lastName;

				if($fighterModel->save()){
					$fighterId = $fighterModel->id;
					$message = '[SUCCESS] New fighter created:' .$fighterModel->firstName . ' '. $fighterModel->lastName;
					$response['state'] = true;
				} else {
					$message = '[ERROR] Fighter not saved: ' .$fighterModel->firstName . ' '. $fighterModel->lastName;
				}
			}

			$response['msg'] = $message;
			$response['data']['fighterId'] = $fighterId;
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionCreateEvent() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;
		$eventId = 0;

		if(isset($_POST))
		{
			// post vars
			$eventName = $_POST['eventName'];
			$eventDate = $_POST['eventDate'];
			$eventLocation = $_POST['eventLocation'];

			// fighter
			$eventModel = new Event;
			$event = Event::model()->find(array(
				'condition'=>'name=:name',
				'params'=>array(
					':name'=>$eventName
				)
			));

			if($event) {
				$eventId = $event->id;
				$message = '[duplicate] Skipping... ' .$event->name;
			} else {
				$eventModel->name = $eventName;
				$eventModel->date = $eventDate;
				$eventModel->location = $eventLocation;
				$eventModel->round = Yii::app()->user->getState("activeRound"); // todo: create setting
				$eventModel->status = 1; // TODO: Do not auto set this

				if($eventModel->save()){
					$eventId = $eventModel->id;
					$message = '[SUCCESS] New event created:' .$eventModel->name . ' - '. $eventModel->date . ' - '. $eventModel->location;
					$response['state'] = true;
				} else {
					$message = '[ERROR] Event not saved: ' .$eventModel->name . ' - '. $eventModel->date . ' - '. $eventModel->location;
				}
			}

			$response['msg'] = $message;
			$response['data']['eventId'] = $eventId;
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionCreateMatchup() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;
		$matchupId = '';

		if(isset($_POST))
		{
			// post vars
			$eventId = $_POST['eventId'];
			$fighter1Id = $_POST['fighter1Id'];
			$fighter2Id = $_POST['fighter2Id'];
			$hasTiebreaker = $_POST['hasTiebreaker'];

			// fighter
			$matchupModel = new Matchup;
			$matchup = Matchup::model()->find(array(
				'condition'=>'eventId=:eventId AND fighter1Id=:fighter1Id AND fighter2Id=:fighter2Id',
				'params'=>array(
					':eventId'=>$eventId,
					':fighter1Id'=>$fighter1Id,
					':fighter2Id'=>$fighter2Id,
				)
			));

			if($matchup) {
				$matchupId = $matchup->id;
				$message = '[duplicate] Skipping... ' .$matchup->id;
			} else {
				$matchupModel->eventId = $eventId;
				$matchupModel->fighter1Id = $fighter1Id;
				$matchupModel->fighter2Id = $fighter2Id;

				if($hasTiebreaker == "true") {
					$matchupModel->hasTiebreaker = 1;
				}

				if($matchupModel->save()){
					$matchupId = $matchupModel->id;
					$message = '[SUCCESS] New matchup created: ' .$matchupModel->id .' - '.$matchupModel->fighter1->firstName .' '.$matchupModel->fighter1->lastName. ' vs ' .$matchupModel->fighter2->firstName .' '. $matchupModel->fighter2->lastName;
					$response['state'] = true;
				} else {
					$message = '[ERROR] Matchup not saved: ' .$matchupModel->id;
				}
			}

			$response['msg'] = $message;
			$response['data']['matchup'] = $matchupId;
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

    public function actionGetContents() {
        $response = array();
        $response['state'] = false;
        $response['msg'] = '';
        $response['data'] = false;

        /* gets the data from a URL */
        $url = $_GET['url'];

        function get_data($url) {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        }

        $data = get_data($url);
        $response['data'] = $data;

        // return request
        $this->layout=false;
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
    }

	public function actionEnrollUser() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		if(isset($_POST)) {
			// vars
			$userId = Yii::app()->user->id;
			$hasPaid = 0; // hardcode for now until we get an auto pay system implemented.
			$defaultRole = 2; // hardcode for now

			// fetch active user
			$activeUser = ActiveUser::model()->find(array(
				'condition'=>'userId=:userId',
				'params'=>array(
					':userId'=>$userId
				)
			));

			// found user
			if($activeUser) {
				$response['msg'] .= 'User '.$userId.' already exists';

				// update model
				$activeUser->hasPaid = $hasPaid; // update user's role to 1
				$activeUser->updatedOn = new CDbExpression('NOW()');

				// save
				if($activeUser->save()) {
					// success
					Yii::app()->user->setState("isActiveUser", $hasPaid);
					$response['msg'] .= 'SUCCESS: Updated user(id): ' . $userId;
				} else {
					// error
					$response['msg'] .= 'ERROR: Failed to say user(id): ' . $userId;
				}

				$response['state'] = true;
			} else {
				// create new active user
				$activeUserModel = new ActiveUser;
				$activeUserModel->userId = $userId;
				$activeUserModel->hasPaid = $hasPaid;
				$activeUserModel->createdOn = new CDbExpression('NOW()');
				$activeUserModel->updatedOn = new CDbExpression('NOW()');

				// save
				if($activeUserModel->save()) {
					// success
					$response['msg'] .= 'SUCCESS: Updated user(id): ' . $userId;
					$response['state'] = true;
				} else {
					// error
					$response['msg'] = 'ERROR: Failed to save active user';
				}
			}

			// update active user's server state
			Yii::app()->user->setState("isActiveUser", $hasPaid);
		} // end if POST

		// return request
		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionAutoCreateByWiki() {
		$wikiLink = $_GET['wikiLink'];
		$this->render('createFighterByWiki', array('wikiLink'=>$wikiLink));
	}

    /*
     * @type
     * open enrollment, new event, event closing, event closed, event over, event update
     *
     * @message
     */
    public function actionSendEmail() {
        $response = array();
        $response['state'] = false;
        $response['message'] = '';
        $response['data'] = false;

        // constants
        define("OPEN_ENROLLMENT", 'openEnrollment');
        define("CLOSE_ENROLLMENT", 'closeEnrollment');
        define("PICKS_REMINDER", 'picksReminder');
        define("EVENT_OPEN", 'eventOpen');
        define("EVENT_CLOSING", 'evemtClosing');
        define("PAYMENT_REMINDER", 'paymentReminder');
        define("MATCHUP_CHANGE", 'matchupChange');

        // vars
        $eventId = null;
        $type = null;
        $emailMessage = '';

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $json = json_decode(file_get_contents("php://input"));

            // if has data property then submitted via app
            if(isset($json->{'data'})) {
                $json = $json->{'data'};
            }

            // post vars
            $type = $json->{'type'};
            $emailMessage = $json->{'emailMessage'};

            if(isset($json->{'eventId'})) {
                $eventId = $json->{'eventId'};
            }

            $response['type'] = $type;
            $response['eventId'] = $eventId;
            $response['emailMessage'] = $emailMessage;

            $subject = '';

            $resultList = array();

            switch ($type) {
                case constant('OPEN_ENROLLMENT'):
                    $subject = '[ufcpickems] Open Enrollment';
                    $resultList = $this->getAllUsers();
                    break;
                case constant('PICKS_REMINDER'):
                    $subject = '[ufcpickems] Pick Reminder';
                    $resultList = $this->getUsersNotSubmittedPicks($eventId);
                    break;
                case constant('EVENT_OPEN'):
                    $subject = '[ufcpickems] Event Open';
                    $resultList = $this->getAllActiveUsers();
                    break;
                case constant('EVENT_CLOSING'):
                    $subject = '[ufcpickems] Event Closed';
                    $resultList = $this->getAllActiveUsers();
                    break;
                case constant('MATCHUP_CHANGE'):
                    $subject = '[ufcpickems] Matchup Change';
                    $resultList = $this->getAllActiveUsers();
                    break;
                default:
                    $response['message'] = "No results for " .$type;
                    $resultList = array();
                    $resultList[] = (object) [
                        'email' => 'lunarvision@gmail.com'
                    ];
                    break;
            }

            $emailList = array();
            $emailListString = '';
            if(count($resultList) > 0) {
                foreach($resultList as $result) {
                    $email = $result['email']; // get when raw params
                    // $email = $result->email; // get when x-www-form
                    $emailList[] = $email;
                    $emailListString .= $email . ', ';

                    // temp remove for dev
                    $this->sendEmail($subject, $email, $emailMessage);
                }

                $response['data'] = $emailList;
            } else {
                $response['message'] = 'There are no results to send email to';
            }

            // email dev a copy
            $emailMessage .= '<br/><br/>';
            $emailMessage .= '[dev]';
            $emailMessage .= '<br/>';
            $emailMessage .= 'Send to: ' . $emailListString;
            $email = 'lunarvision@gmail.com';
            $this->sendEmail($subject, $email, $emailMessage);

            $response['state'] = true;
        }

        // return request
        $this->layout=false;
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
    }

    function getAllUsers() {
        $criteria = new CDbCriteria();
        $data = User::model()->findAll($criteria);

        return $data;
    }

    function getAllActiveUsers() {
        $sql = 'select u.id, u.firstname, u.lastname, u.email
            from ActiveUser a
            join User u on a.userId = u.id';

        $list= Yii::app()->db->createCommand($sql)->queryAll();

        $rs = array();
        foreach($list as $item){
            //process each item here
            //$rs[]= (object) ['id' => $item['id']];
            $rs[] = $item;
        }

        return $rs;
    }

    function getUsersNotSubmittedPicks($eventId) {
        $sql = 'select u.id, u.firstname, u.lastname, u.email
            from ActiveUser as a
            join User as u on a.userId = u.id
            where a.userId not in (
                select p.userId from Pick p
                where eventId = '.$eventId.'
                group by (p.userId)
            )';

        $list= Yii::app()->db->createCommand($sql)->queryAll();

        $rs = array();
        foreach($list as $item){
            //process each item here
            //$rs[]= (object) ['id' => $item['id']];
            $rs[] = $item;
        }

        return $rs;
    }

    function getUsersSubmittedPicks($eventId) {
        $sql = 'select u.id, u.firstname, u.lastname, u.email
            from ActiveUser as a
            join User as u on a.userId = u.id
            where a.userId in (
                select p.userId from Pick p
                where eventId = '.$eventId.'
                group by (p.userId)
            )';

        $list= Yii::app()->db->createCommand($sql)->queryAll();

        $rs = array();
        foreach($list as $item){
            //process each item here
            //$rs[]= (object) ['id' => $item['id']];
            $rs[] = $item;
        }

        return $rs;
    }

    function sendEmail($subject, $to, $msg) {
        $email = Yii::app()->email;
        $email->from = '<noreply@ufcpickems.com>';
        $email->to = $to; // user email
        $email->subject = $subject;
        $email->message = $msg;
        $email->send();
    }

	/*public function actionSendEmail() {
		$response = array();
		$response['state'] = true;
		$response['msg'] = '';
		$response['data'] = false;

		if(isset($_POST))
		{
			// vars
			$userId = Yii::app()->user->id;
			$emailMsg = '';
			$eventS = '';
			$fightersS = '';
			$tiebreakerS = '';

			// post vars
			$fighters = $_POST['fighters'];
			$eventId = $_POST['eventId'];
			$userEmail = $_POST['userEmail'];
			$userName = $_POST['userName'];

			$eventModel = Event::model()->findByPk($eventId);
			$eventS .= 'Event: ' . $eventModel->name;

			// &userId=0&eventId=4&selectedFighters=135_137_139_141_143&tiebreakers=135_5_1__137_2_2
			$fighters = explode(',', $fighters);
			foreach($fighters as $fighter){
				$fighterModel = Fighter::model()->findByPk($fighter);
				if($fighterModel){
					$fighterId = $fighterModel->id;
					$firstName = $fighterModel->firstName;
					$lastName = $fighterModel->lastName;
					$fightersS .= $firstName .' '. $lastName;

					$pickModel = Pick::model()->with('finishType', 'round')->find( array(
						'condition'=>'userId=:userId AND fighterId=:fighterId AND eventId=:eventId',
						'params'=>array(':userId'=>$userId, ':fighterId'=>$fighterId, ':eventId'=>$eventId),
					));

					if($pickModel){
						$finishTypeId = $pickModel->finishTypeId;
						$finishName = $pickModel->finishTypeId;
						$roundId = $pickModel->roundId;
						$roundName = $pickModel->finishTypeId;

						if(!is_null($finishTypeId) && !is_null($roundId)) {
							$fightersS .= ' ('.$pickModel->finishType->finishName.' / ' .$pickModel->round->round. ')';
						}
					}

					$fightersS .='<br/>';
				}
				$response['msg'] .= $fightersS;
			}

			// send confirmation to admin
			$emailMsg .= '**** UFC PICKEMS ****';
			$emailMsg .= '<br/>';
			$emailMsg .= 'Picks From: '.$userName.'<br/><br/>';
			$emailMsg .= $fightersS;

			$email = Yii::app()->email;
			$email->to = $this->adminEmail;
			//$email->cc = $userEmail;
			$email->subject = $eventS.' Picks!';
			$email->message = $emailMsg;
			$email->send();

			// send confirmation to user
			$emailMsg = ''; // clear to hide users picks
			$emailMsg .= '**** UFC PICKEMS ****';
			$emailMsg .= '<br/>';
			$emailMsg .= 'You have submitted your picks!';
			$emailMsg .= '<br/><br/>';
			$emailMsg .= $fightersS;

			$email = Yii::app()->email;
			$email->to = $userEmail;
			$email->subject = $eventS.' Picks!';
			$email->message = $emailMsg;
			$email->send();

			// send confirmation to developer
			$emailMsg = ''; // clear to hide users picks
			$emailMsg .= '**** UFC PICKEMS ****';
			$emailMsg .= '<br/>';
			$emailMsg .= $userName .' has submitted their picks.<br/>';

			$email = Yii::app()->email;
			$email->to = $this->devEmail;
			$email->subject = $eventS.' Picks!';
			$email->message = $emailMsg;
			$email->send();

			$response['state'] = true;
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}*/

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}