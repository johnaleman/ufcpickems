<?php
/*
 * If Auth is required add at the beginning of each method
 * $this->_checkAuth();
 */
class ApiController extends Controller
{
    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    /*
     * Get all Models List Action
     */
    public function actionList()
    {
        // response
        $response['status'] = false;
        $response['message'] = false;
        $response['data'] = false;
        $response['data']['list'] = null;
        $response['data']['count'] = 0;

        // Get the respective model instance
        switch ($_GET['model']) {
            case 'event':
                // check for params
                $criteriaParams = array();
                foreach ($_GET as $key => $value) {
                    if($key == 'model') {
                        continue;
                    }

                    $criteriaParams[$key] = $value;
                }

                // search
                // catch special search queries
                if(isset($_GET['round'])) {
                    if($_GET['round'] == 'all') {
                        unset($criteriaParams['round']);
                    }

                    if($_GET['round'] == 'active') {
                        // select the active round in settings
                        $settingsModel = Settings::model()->findByPk(1);
                        $criteriaParams['round'] = $settingsModel->round;
                    }
                }

                $criteria = new CDbCriteria();
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'round=:round';
                    $criteria->params = $criteriaParams;
                }

                $criteria->order = 't.date DESC';
                $models = Event::model()->findAll($criteria);
                break;
            case 'fighter':
                $criteria = new CDbCriteria();
                $criteria->order = 't.lastname DESC';
                $models = Fighter::model()->findAll($criteria);
                break;
            case 'matchup':
                // check for params
                $criteriaParams = array();
                foreach ($_GET as $key => $value) {
                    if($key == 'model') {
                        continue;
                    }

                    $criteriaParams[$key] = $value;
                }

                // search
                $criteria = new CDbCriteria();
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'eventId=:eventId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->select = array('*');
                $criteria->join = 'JOIN Fighter AS f ON f.id = t.fighter1Id';
                $criteria->order = 't.id DESC';
                $models = Matchup::model()->findAll($criteria);
                break;
            case 'user':
                $criteria = new CDbCriteria();
                $criteria->order = 't.id DESC';
                $models = User::model()->findAll($criteria);
                break;
            default:
                // Model not implemented error
                $msg = sprintf('Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']);
                $response['message'] = sprintf('Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']);
                $this->_sendResponse(501, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                Yii::app()->end();
        }

        // Did we get some results?
        if (empty($models)) {
            // No
            $response['message'] = sprintf('No items where found for model %s', $_GET['model']);
            $this->_sendResponse(200, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        } else {
            // Prepare response
            $rows = array();

            switch($_GET['model']) {
                case 'matchup':
                    foreach ($models as $model) {
                        $rows[] = (object) array(
                            'id' => $model->id,
                            'fighter1Id' => $model->fighter1Id,
                            'fighter2Id' => $model->fighter2Id,
                            'eventId' => $model->eventId,
                            'hasTiebreaker' => $model->hasTiebreaker,
                            'fighter1firstName' => $model->fighter1->firstName,
                            'fighter1lastName' => $model->fighter1->lastName,
                            'fighter2firstName' => $model->fighter2->firstName,
                            'fighter2lastName' => $model->fighter2->lastName,
                        );
                    }

                    break;

                default:
                    foreach ($models as $model) {
                        $rows[] = $model->attributes;
                    }
                    break;
            }



            // set count
            $response['data']['count'] = count($rows);

            // set data
            $response['status'] = true;
            $response['message'] = '';
            $response['data']['list'] = $rows;

            // Send the response
            $this->_sendResponse(200, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        }
    }

    /*
     * Get a Single Model Action
     * /api/post/123 (also GET )
    */
    public function actionView()
    {
        // response
        $response['status'] = false;
        $response['message'] = false;
        $response['data'] = false;

        // Check if id was submitted via GET
        if (!isset($_GET['id'])) {
            $response['message'] = 'Error: Parameter <b>id</b> is missing';
            $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        }

        switch ($_GET['model']) {
            // Find respective model
            case 'setting':
                $model = Settings::model()->findByPk($_GET['id']);
                break;
            case 'event':
                $model = Event::model()->findByPk($_GET['id']);
                break;
            case 'fighter':
                $model = Fighter::model()->findByPk($_GET['id']);
                break;
            case 'matchup':
                $model = Matchup::model()->findByPk($_GET['id']);
                break;
            case 'user':
                $model = User::model()->findByPk($_GET['id']);
                $model['password'] = '';
                break;
            default:
                $response['message'] = sprintf('Mode <b>view</b> is not implemented for model <b>%s</b>', $_GET['model']);
                $this->_sendResponse(501, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                Yii::app()->end();
        }
        // Did we find the requested model? If not, raise an error
        if (is_null($model)) {
            $response['message'] = 'No Item found with id ' . $_GET['id'];
            $this->_sendResponse(404, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        } else {
            // do not return response, return the model otherwise updating issues will occur
            $this->_sendResponse(200, htmlspecialchars(CJSON::encode($model), ENT_NOQUOTES));
        }
    }

    /*
     * Create a new Model Action
     * /api/posts (POST)
     */
    public function actionCreate()
    {
        // response
        $response['status'] = false;
        $response['message'] = false;
        $response['isDuplicate'] = false;
        $response['data'] = false;

        switch ($_GET['model']) {
            // Get an instance of the respective model
            case 'event':
                $model = new Event;
                $model->round = Settings::model()->findByPk(1)->round;
                break;
            case 'fighter':
                $model = new Fighter;
                break;
            case 'matchup':
                $model = new Matchup;
                break;
            case 'user':
                $model = new User;
                break;
            default:
                $response['message'] = sprintf('Mode <b>create</b> is not implemented for model <b>%s</b>', $_GET['model']);
                $this->_sendResponse(501, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                Yii::app()->end();
        }

        // Try to assign POST values to attributes
        foreach ($_POST as $key => $value) {
            // Does the model have this attribute? If not raise an error
            if ($model->hasAttribute($key)) {
                $model->$key = $value; // map params
            } else {
                $response['message'] = sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $key, $_GET['model']);
                $this->_sendResponse(500, $response);
            }
        }

        // check if record already exists
        switch ($_GET['model']) {
            case 'event':
                // set check params
                $criteriaParams = array();
                $criteriaParams['name'] = $_POST['name'];
                // when auto creating event the time us unknown

                // create criteria obj
                $criteria = new CDbCriteria();

                // there are params
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'name=:name';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $eventModel = Event::model()->findAll($criteria);
                if(count($eventModel) > 0) {
                    $response['data'] = $eventModel;
                    $response['status'] = true;
                    $response['isDuplicate'] = true;
                    $response['message'] = 'WARNING: Event already exists.';
                    $this->_sendResponse(200, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }
                break;
            case 'fighter':
                // set check params
                $criteriaParams = array();
                $criteriaParams['firstName'] = $_POST['firstName'];
                $criteriaParams['lastName'] = $_POST['lastName'];

                // create criteria obj
                $criteria = new CDbCriteria();
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'firstName=:firstName AND lastName=:lastName';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $fighterModel = Fighter::model()->findAll($criteria);
                if(count($fighterModel) > 0) {
                    $response['data'] = $fighterModel;
                    $response['status'] = true;
                    $response['isDuplicate'] = true;
                    $response['message'] = 'WARNING: Fighter already exists.';
                    $this->_sendResponse(200, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }
                break;
            case 'matchup':
                // set check params
                $criteriaParams = array();
                $criteriaParams['fighter1Id'] = $_POST['fighter1Id'];
                $criteriaParams['fighter2Id'] = $_POST['fighter2Id'];
                $criteriaParams['eventId'] = $_POST['eventId'];

                // create criteria obj
                $criteria = new CDbCriteria();
                if(count($criteriaParams) > 0) {
                    $criteria->condition = 'fighter1Id=:fighter1Id AND fighter2Id=:fighter2Id AND eventId=:eventId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $matchupModel = Matchup::model()->findAll($criteria);
                if(count($matchupModel) > 0) {
                    $response['data'] = $matchupModel;
                    $response['status'] = true;
                    $response['isDuplicate'] = true;
                    $response['message'] = 'WARNING: Matchup already exists.';
                    $this->_sendResponse(200, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }
                break;
            case 'user':
                break;
            default:
                $response['message'] = sprintf('Mode <b>create</b> is not implemented for model <b>%s</b>', $_GET['model']);
                $this->_sendResponse(501, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                Yii::app()->end();
        }

        // if user
        if($_GET['model'] == 'user') {
            // update properties
            $model->password = md5($model->password); // encrypt password
            // set date created on
            $model->createdOn = date('Y-m-d H:i:s'); // NOW()
            $model->updatedOn = date('Y-m-d H:i:s'); // NOW()
        }

        // try to save the model
        if ($model->save()) {
            $response['status'] = true;
            $response['message'] = 'Success: Created!';
            $response['data'] = $model;
            $this->_sendResponse(201, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        } else {
            // errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $msg .= "<ul>";
            foreach ($model->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";

            $response['message'] = $msg;
            $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        }
    }

    /*
     * Update a Model Action
     * /api/posts/123 (PUT)
     */
    public function actionUpdate()
    {
        // response
        $response['status'] = false;
        $response['message'] = false;
        $response['data'] = false;


        // Parse the PUT parameters. This didn't work: parse_str(file_get_contents('php://input'), $put_vars);
        $json = file_get_contents('php://input'); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
        $put_vars = CJSON::decode($json, true); //true means use associative array

        switch ($_GET['model']) {
            // Find respective model
            case 'event':
                $model = Event::model()->findByPk($_GET['id']);
                break;
            case 'fighter':
                $model = Fighter::model()->findByPk($_GET['id']);
                break;
            case 'matchup':
                $model = Matchup::model()->findByPk($_GET['id']);
                break;
            case 'user':
                $model = User::model()->findByPk($_GET['id']);
                break;
            default:
                $response['message'] = sprintf('Error: Mode <b>update</b> is not implemented for model <b>%s</b>', $_GET['model']);
                $this->_sendResponse(501, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                Yii::app()->end();
        }
        // Did we find the requested model? If not, raise an error
        if ($model === null) {
            $response['message'] = sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",$_GET['model'], $_GET['id']);
            $this->_sendResponse(400, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        }

        // Try to assign POST values to attributes
        foreach ($put_vars as $key => $value) {
            // Does the model have this attribute? If not raise an error
            if ($model->hasAttribute($key)) {
                $model->$key = $value; // map params
            } else {
                $response['message'] = sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $key, $_GET['model']);
                $this->_sendResponse(500, $response);
            }
        }

        // if user
        if($_GET['model'] == 'user') {
            // update properties
            $model->password = md5($put_vars['password']); // encrypt password
            $model->updatedOn = date('Y-m-d H:i:s'); // NOW()
        }

        // Try to save the model
        if ($model->save()) {
            $response['status'] = true;
            $response['message'] = 'SUCCESS: Saved!';
            $response['data'] = $model;
            $this->_sendResponse(200, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        } else {
            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $msg .= "<ul>";
            foreach ($model->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";

            $response['message'] = $msg;
            $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        }
    }

    /*
     * Delete a Model Action
     * /api/posts/123 (DELETE)
     */
    public function actionDelete()
    {
        // response
        $response['status'] = false;
        $response['message'] = false;
        $response['data'] = false;

        switch ($_GET['model']) {
            // Load the respective model
            case 'event':
                $model = Event::model()->findByPk($_GET['id']);

                // find all matchups that connect with event id
                // check for params
                $criteriaParams = array();
                if(isset($_GET['id'])) {
                    $criteriaParams['eventId'] = $_GET['id'];
                }

                $criteria = new CDbCriteria();
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'eventId=:eventId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $matchupModel = Matchup::model()->findAll($criteria);
                if(count($matchupModel) > 0) {
                    $response['data'] = $matchupModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: Matchup constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }
                break;
            case 'fighter':
                $model = Fighter::model()->findByPk($_GET['id']);

                // check for foreign key violations
                // user param to set the foreign key <fighterId>
                $criteriaParams = array();
                if(isset($_GET['id'])) {
                    $criteriaParams['fighterId'] = $_GET['id'];
                }

                // setup query criteria
                $criteria = new CDbCriteria();

                // check Matchup for possible key constraints
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'fighter1ID=:fighterId OR fighter2ID=:fighterId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $checkModel = Matchup::model()->findAll($criteria);
                if(count($checkModel) > 0) {
                    $response['data'] = $checkModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: Matchup constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }

                // check Pick for possible key constraints
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'fighterId=:fighterId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if picks are found
                $checkModel = Pick::model()->findAll($criteria);
                if(count($checkModel) > 0) {
                    $response['data'] = $checkModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: Pick constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }

                // Check Result for possible key constraints
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'fighterId=:fighterId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if Results are found
                $checkModel = Result::model()->findAll($criteria);
                if(count($checkModel) > 0) {
                    $response['data'] = $checkModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: Result constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }
                break;
            case 'matchup':
                $model = Matchup::model()->findByPk($_GET['id']);
                break;
            case 'user':
                $model = User::model()->findByPk($_GET['id']);

                // check for foreign key violations
                // user param to set the foreign key <fighterId>
                $criteriaParams = array();
                if(isset($_GET['id'])) {
                    $criteriaParams['userId'] = $_GET['id'];
                }

                // setup query criteria
                $criteria = new CDbCriteria();

                // ActiveUser
                // check ActiveUser for possible key constraints
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'userId=:userId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $checkModel = ActiveUser::model()->findAll($criteria);
                if(count($checkModel) > 0) {
                    $response['data'] = $checkModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: ActiveUser constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }

                // Following
                // check Following for possible key constraints
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'userId=:userId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $checkModel = Following::model()->findAll($criteria);
                if(count($checkModel) > 0) {
                    $response['data'] = $checkModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: Following constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }

                // Pick
                // check Following for possible key constraints
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'userId=:userId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $checkModel = Pick::model()->findAll($criteria);
                if(count($checkModel) > 0) {
                    $response['data'] = $checkModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: Pick constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }

                // Standing
                // check Standing for possible key constraints
                if(count($criteriaParams) > 0) {
                    // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
                    $criteria->condition = 'userId=:userId';
                    $criteria->params = $criteriaParams;
                }
                $criteria->order = 't.id DESC';

                // if records are found
                $checkModel = Standing::model()->findAll($criteria);
                if(count($checkModel) > 0) {
                    $response['data'] = $checkModel;
                    $response['status'] = true;
                    $response['message'] = 'ERROR: Standing constraints found.';
                    $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                    Yii::app()->end();
                }
                break;
            default:
                $response['message'] = sprintf('Error: Mode <b>delete</b> is not implemented for model <b>%s</b>', $_GET['model']);
                $this->_sendResponse(501, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
                Yii::app()->end();
        }
        // Was a model found? If not, raise an error
        if ($model === null) {
            $response['message'] = sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_GET['id']);
            $this->_sendResponse(400, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        }

        // Delete the model
        if ($model->delete()) {
            $response['status'] = true;
            $response['message'] = 'SUCCESS: Deleted!';
            $response['data'] = $model;
            $this->_sendResponse(200, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
        } else {
            $response['message'] = sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_GET['id']);
            $this->_sendResponse(500, htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES));
            Yii::app()->end();
        }
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        if($this->format == 'json') {
            $content_type = 'application/json';
        }

        header('Content-type: ' . $content_type);


        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
        } // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                </head>
                <body>
                    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                    <p>' . $message . '</p>
                    <hr />
                    <address>' . $signature . '</address>
                </body>
                </html>';

            echo $body;
        }
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    // If we want to have the API user authorize themselves
    private function _checkAuth()
    {
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if (!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $_SERVER['HTTP_X_USERNAME'];
        $password = $_SERVER['HTTP_X_PASSWORD'];
        // Find the user
        $user = User::model()->find('LOWER(username)=?', array(strtolower($username)));
        if ($user === null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if (!$user->validatePassword($password)) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }
}

?>