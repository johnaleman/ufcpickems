<?php

class ResultController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'standings'),
				'users'=>array('*'),
				'roles'=>array('1', '2'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('viewPicks'),
				//'users'=>array('*'),
				//'users'=>array('1', 'lunarvision@gmail.com'),
				'roles'=>array('1'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
//				'users'=>array('admin', 'lunarvision@gmail.com'),
				'roles'=>array('1'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        // response
        $response = [];
        $response['data'] = [];
        $response['status'] = false;
        $response['errors'];
        $response['message'] = '';

        $response['data'] = $this->loadModel($id);
        $response['status'] = true;

        // prep headers and return json response
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
	}

    public function actionCreate()
	{
        // response
        $response = [];
        $response['data'] = [];
        $response['status'] = false;
        $response['errors'];
        $response['message'] = '';

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $json = json_decode(file_get_contents('php://input'));
            if(isset($json->{'data'})) {
                $json = $json->{'data'};
            }

            $selectedFighterID = $json->{'selectedFighterID'};
            $selectedEventID = $json->{'selectedEventID'};
            $selectedRoundID = $json->{'selectedRoundID'};
            $selectedFinishTypeID = $json->{'selectedFinishTypeID'};

            // check to see if fighter has been selected on the given event
            $criteria = new CDbCriteria();
            $criteria->select = array('t.id', 't.eventId');
            $criteria->order = 't.id ASC';
            $criteria->addInCondition('t.fighterId', array($selectedFighterID));
            $criteria->addInCondition('t.eventId', array($selectedEventID));
            $result = Result::model()->findAll($criteria);

            if($result == false) {
                // create
                $result = new Result();
                $result->fighterId = $selectedFighterID;
                $result->eventId = $selectedEventID;
                $result->roundId = $selectedRoundID;
                $result->finishTypeId = $selectedFinishTypeID;

                if($result->save()) {
                    $response['status'] = true;
//                    $response['data'] = $result;
                    $response['data'] = $this->_setResponseData($response['data'], $result);
                    $response['message'] = 'SUCCESS: Result created';
                } else {
                    // erroru
                    $response['message'] = 'ERROR saving result';
                }
            } else {
                // update
                $id = $result[0]['id'];
                $result = $this->loadModel($id);
                $result->fighterId = $selectedFighterID;
                $result->eventId = $selectedEventID;
                $result->roundId = $selectedRoundID;
                $result->finishTypeId = $selectedFinishTypeID;

                if($result->save()) {
                    $response['data'] = $this->_setResponseData($response['data'], $result);
                    $response['status'] = true;
                    $response['message'] = 'SUCCESS: Result updated';
                }
            }
        } else {
            // get data to populate view
            $response['data']['fighterList'] = $this->_getListOfFighters();
            $response['data']['eventList'] = $this->_getListOfEvents();
            $response['data']['roundList'] = $this->_getListOfRounds();
            $response['data']['finishTypeList'] = $this->_getListOfFinishTypes();
            $response['status'] = true;
        }

        // prep headers and return json response
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
	}

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        // response
        $response = [];
        $response['data'] = [];
        $response['status'] = false;
        $response['errors'];
        $response['message'] = '';

        if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            // TODO: if post save and then redirect
            $json = json_decode(file_get_contents("php://input"));
            // if has data property then submitted via app
            if(isset($json->{'data'})) {
                // start with data as root
                $json = $json->{'data'};
            }

            // clean $json
            $response['data']['sent'] = $json;
            $selectedFighterID = $json->{'selectedFighterID'};
            $selectedEventID = $json->{'selectedEventID'};
            $selectedRoundID = $json->{'selectedRoundID'};
            $selectedFinishTypeID = $json->{'selectedFinishTypeID'};

            // update properties
            $result = $this->loadModel($id);
            $result->fighterId = $selectedFighterID;
            $result->eventId = $selectedEventID;
            $result->roundId = $selectedRoundID;
            $result->finishTypeId = $selectedFinishTypeID;

            if($result->save()) {
                $response['data'] = $this->_setResponseData($response['data'], $result);
                $response['status'] = true;
                $response['message'] = 'SUCCESS: Result updated';
            }
        } else { // get
            $result = $this->loadModel($id);
            // $response['data'] = $this->_setResponseData($response['data'], array_values($result)[0]);
            $response['data'] = $this->_setResponseData($response['data'], $result);
        }

        // prep headers and return json response
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
    }

    private function _getListOfFighters() {
        $criteria = new CDbCriteria();
        $criteria->select = 't.id, t.firstName, t.lastName';
        $criteria->order = 't.lastName ASC';
        $data = Fighter::model()->findAll($criteria);
        // return all attributes

        // filter only selected attributes
        foreach ($data as $model) {
            $rows[] = array_filter($model->attributes);
        }

        return $rows;
    }

    private function _getListOfEvents() {
        $criteria = new CDbCriteria();
        $criteria->select = array('t.id', 't.name', 't.round', 't.date');
        $criteria->addInCondition('t.round', array($this->getActiveRound()));
        $criteria->order = 't.date DESC';
        $data = Event::model()->findAll($criteria);
        $rows = []; // holds data with selected attributes

        // filter only selected attributes
        foreach ($data as $model) {
            $rows[] = array_filter($model->attributes);
        }

        return $rows;
    }

    private function _getListOfRounds() {
        $criteria = new CDbCriteria();
        $criteria->select = array('t.id', 't.round');
        $criteria->order = 't.round ASC';
        $data = Round::model()->findAll($criteria);
        $rows = []; // holds data with selected attributes

        // filter only selected attributes
        foreach ($data as $model) {
            $rows[] = array_filter($model->attributes);
        }

        return $rows;
    }

    private function _getListOfFinishTypes() {
        $criteria = new CDbCriteria();
        $criteria->select = array('t.id', 't.finishName');
        $criteria->order = 't.id ASC';
        $data = FinishType::model()->findAll($criteria);
        $rows = []; // holds data with selected attributes

        // filter only selected attributes
        foreach ($data as $model) {
            $rows[] = array_filter($model->attributes);
        }

        return $rows;
    }

    private function getActiveRound() {
        $settingsModel = Settings::model()->findByPk(1);
        if($settingsModel) {
            return $settingsModel->round;
        }

        return false;
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        // response
        $response = [];
        $response['data'] = [];
        $response['status'] = false;
        $response['errors'];
        $response['message'] = '';

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $model = $this->loadModel($id);

            if($model->delete()) {
                $response['data'] = $model;
                $response['status'] = true;
                $response['message'] = 'SUCCESS: Result removed';
            }
        }

        // prep headers and return json response
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        // response
        $response = [];
        $response['data'] = [];
        $response['status'] = false;
        $response['errors'] = [];
        $response['message'] = '';

        // vars
        $activeRound = Settings::model()->findByPk(1)->round;

        // get event list for current round
        $criteria = new CDbCriteria;
        $criteria->order = 'date DESC';
        $eventModel = Event::model()->findAll(array(
            'condition' => 'round=:round',
            'params' => array(
                ':round' => $activeRound
            ),
            'order' => 'date DESC'
        ));
        $eventArr = array(); // holds list of formatted event objects
        $response['data']['count'] = count($eventModel);

        // loop through the event list
        foreach($eventModel as $event) {
            // vars
            $eventId = $event->id;

            // get the matchup model for a given event id
            $criteria = new CDbCriteria;
            $criteria->addInCondition('eventId', array($eventId));
            $matchupArr = array(); // holds list of formatted matchup objects
            $matchupModel = Matchup::model()->findAll($criteria);
            foreach ($matchupModel as $matchup) {
                // vars
                $matchupId = $matchup->id;
                $hasTiebreaker = $matchup->hasTiebreaker;

                $fighter1ID = $matchup->fighter1->id;
                $fighter1FirstName = $matchup->fighter1->firstName;
                $fighter1LastName = $matchup->fighter1->lastName;
                $fighter1FullName = $fighter1FirstName .' '. $fighter1LastName;

                $fighter2ID = $matchup->fighter2->id;
                $fighter2FirstName = $matchup->fighter2->firstName;
                $fighter2LastName = $matchup->fighter2->lastName;
                $fighter2FullName = $fighter2FirstName .' '. $fighter2LastName;

                // get result object
                // by event and fighter
                $criteria = new CDbCriteria;
                $criteria->condition = 'eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID';
                $criteria->params = array(':eventId' => $eventId, ':fighter1ID' => $fighter1ID, ':fighter2ID' => $fighter2ID);

                $tempObj = (object) [];
                $tempObj->matchupId = $matchupId;
                $tempObj->hasTiebreaker = $hasTiebreaker;
                $tempObj->fighter1ID = $fighter1ID;
                $tempObj->fighter1FullName = $fighter1FullName;
                $tempObj->fighter2ID = $fighter2ID;
                $tempObj->fighter2FullName = $fighter2FullName;
                $tempObj->resultId = null;
                $tempObj->winnerID = null;
                $tempObj->winnerFighterName = null;
                $tempObj->roundID = null;
                $tempObj->roundName = null;
                $tempObj->finishTypeID = null;
                $tempObj->finishTypeName = null;

                // get the result
                $resultModel = Result::model()->find($criteria);
                if($resultModel) {
                    // result found
                    $resultId = $resultModel->id;
                    $winnerId = $resultModel->fighterId;
                    $winnerFighterName = $resultModel->fighter->firstName . ' ' . $resultModel->fighter->lastName;
                    $roundId = $resultModel->roundId;
                    $selectedRoundName = $resultModel->round->round;
                    $finishTypeId = $resultModel->finishTypeId;
                    $finishTypeName = $resultModel->finishType->finishName;

                    $tempObj->resultId = $resultId;
                    $tempObj->winnerID = $winnerId;
                    $tempObj->winnerFighterName = $winnerFighterName;
                    $tempObj->roundID = $roundId;
                    $tempObj->roundName = $selectedRoundName;
                    $tempObj->finishTypeID = $finishTypeId;
                    $tempObj->finishTypeName = $finishTypeName;
                }

                $matchupArr[] = $tempObj;
            }

            $eventArr[] = (object) array('event' => $event, 'matchup' => $matchupArr);
        }

        $response['data']['resultList'] = $eventArr;

        // prep headers and return json response
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
	}

	public function actionViewPicks()
	{
		$this->layout='//layouts/column1';
		$dataProvider=new CActiveDataProvider('Result');
		$this->render('viewpicks',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 *  select * from Standing t
        join User AS u
        on t.userId = u.id and t.win !=0
        and t.win != 0
        ORDER BY t.win DESC, t.tiebreakerWin DESC
	 */
	public function actionStandings()
    {
        // response
        $response = [];
        $response['data'] = [];
        $response['status'] = false;
        $response['errors'];
        $response['message'] = '';

		// TODO: mimic going to results first
		$this->layout='//layouts/column1';
		$this->updateStandingsTable();

        $criteria = new CDbCriteria;
        $criteria->select = array('*');
        $criteria->join = 'join User AS u on t.userId = u.id and t.win !=0';
        $criteria->order = 't.win DESC, t.tiebreakerWin DESC';

        $model = Standing::model()->findAll($criteria);

        $index = 0;
        foreach ($model as $result) {
            $index++;
            $resultArr[] = (object) array(
                'id' => $result->id,
                'rank' => (string) $index,
                'userId' => $result->user->id,
                'firstName' => ucwords($result->user->firstname),
                'lastName' => ucwords($result->user->lastname),
                'fullName' => ucwords($result->user->firstname) . ' ' . ucwords(substr($result->user->lastname, 0, 1)),
                'winCount' => $result->win,
                'lossCount' => $result->loss,
                'tiebreakerCount' => $result->tiebreakerWin
            );
        }

        $count = count($model);
        $response['data'] = $resultArr;
        $response['count'] = $count;

        // prep headers and return json response
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
	}

	private function updateStandingsTable(){
        // get event for current round
        $settingsModel = Settings::model()->findByPk(1);
        $activeRound = $settingsModel->round;

        $userModel = User::model()->findAll();

		foreach($userModel as $user){
			$userId = $user->id;

			// win count
			$criteria = new CDbCriteria;
			$criteria->addInCondition('userId', array($userId));
			$criteria->addInCondition('win', array(1));
			$criteria->addInCondition('event.round', array($activeRound));
			$criteria->with = array(
				'event' => array(
					'together' => true
				)
			);

			$pickModel = Pick::model()->findAll($criteria);
			$winCount = count($pickModel);

			// loss count
            $criteria = new CDbCriteria;
            $criteria->addInCondition('event.round', array($activeRound));
            $criteria->with = array(
                'event' => array(
                    'together' => true
                )
            );

            $resultLength = count(Result::model()->findAll($criteria));

			$lossCount = $resultLength - $winCount;

			// tiebreaker win count
			$criteria = new CDbCriteria;
			$criteria->addInCondition('userId', array($userId));
			$criteria->addInCondition('tiebreakerWin', array(1, 2));
			$criteria->addInCondition('event.round', array($activeRound));
			$criteria->with = array(
				'event' => array(
					'together' => true
				)
			);
			$pickModel = Pick::model()->findAll($criteria);
			$tiebreakerWinCount = 0;

			// loop through to get point value per record
			foreach($pickModel as $pick){
				$tiebreakerWinCount += $pick->tiebreakerWin; // calc tie breaker points
			}

			// populate records
			$criteria = new CDbCriteria;
			$criteria->addInCondition('userId', array($userId));
			$standingModel = Standing::model()->find($criteria);

			if($standingModel){
				$standingModel->win = $winCount;
				$standingModel->loss = $lossCount;
				$standingModel->tiebreakerWin = $tiebreakerWinCount;
			} else {
				// create a new one
				$standingModel = new Standing();
				$standingModel->userId = $userId;
				$standingModel->win = $winCount;
				$standingModel->loss = $lossCount;
				$standingModel->tiebreakerWin = $tiebreakerWinCount;
			}

			if($standingModel->save()){}
		}
	}

	protected function beforeAction($action)
	{
		return true;
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        // response
        $response = [];
        $response['data'] = [];
        $response['status'] = false;
        $response['errors'];
        $response['message'] = '';

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $settingsModel = Settings::model()->findByPk(1);
            $activeRound = $settingsModel->round;

            $criteria = new CDbCriteria;
            $criteria->select = array('*');
            $criteria->join = 'JOIN Event AS e ON t.eventId = e.id AND e.round = '.$activeRound.'';
            $criteria->order = 't.id DESC';

            $model = Result::model()->findAll($criteria);

            foreach ($model as $result) {
                $resultArr[] = (object) array(
                    'id' => $result->id,
                    'fighterId' => $result->fighterId,
                    'fighterName' => $result->fighter->firstName .' '. $result->fighter->lastName,
                    'finishTypeId' => $result->finishTypeId,
                    'finishType' => $result->finishType->finishName,
                    'round' => $result->round->round,
                    'eventId' => $result->eventId,
                    'event' => $result->event->name
                );
            }

            $count = count($model);
            $response['data'] = $resultArr;
            $response['count'] = $count;
        }

        // prep headers and return json response
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Result the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Result::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    private function _setResponseData($responseData, $result) {
        $responseData['id'] = $result->id;
        $responseData['fighterId'] = $result->fighterId;
        $responseData['fighterName'] = $result->fighter->firstName .' '. $result->fighter->lastName;
        $responseData['finishTypeId'] = $result->finishTypeId;
        $responseData['finishType'] = $result->finishType->finishName;
        $responseData['roundId'] = $result->round->id;
        $responseData['round'] = $result->round->round;
        $responseData['eventId'] = $result->eventId;
        $responseData['event'] = $result->event->name;

        return $responseData;
    }

	/**
	 * Performs the AJAX validation.
	 * @param Result $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='result-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
