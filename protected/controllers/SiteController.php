<?php

class SiteController extends Controller
{
	const ENV = 'dev'; // dev; prod

	// VARS
	private $adminEmail = 'lunarvision@gmail.com';
	private $devRootPath = '/sandbox/pickems/dev/web/';
	private $prodRootPath = '';

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
//    public $layout='//layouts/login';

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('forgotPassword'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 *
	 * renders the view file 'protected/views/site/index.php'
	 * using the default layout 'protected/views/layouts/main.php'
	 */
	public function actionIndex()
	{
		$userId = Yii::app()->user->getId(); // get user id
		$roles = Yii::app()->user->getState("roles");

		$settingsModel = Settings::model()->findByPk(1);
		$isOpenEnrollment = $settingsModel->isOpenEnrollment;
		$round = $settingsModel->round;
		Yii::app()->user->setState("activeRound", $round);

		if($userId) {
			$model = array(
				'roles'=> $roles,
				'userId'=>$userId,
				'isActiveUser'=>Yii::app()->user->getState("isActiveUser"),
				'isOpenEnrollment'=>$isOpenEnrollment,
				'round'=> $round
			);

			// tried to switch context.  refactor this
			/*if ($roles == 10) {
				$this->render('setuser');
			} else {
				$this->render('index', array('model' => $model));
			}*/

			$this->render('index', array('model'=>$model));
		} else {
			$this->render('index');
		}
	}

		/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionInventory()
	{
		$dataProvider=new CActiveDataProvider('CarInventory');
		$this->render('inventory',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		// response
		$response = [];
		$response['data'] = null;
		$response['status'] = false;
		$response['errors'];
		$response['message'] = '';

		// check if LoginForm is set
		if(isset($_POST['LoginForm']))
		{
			// validation
			if (empty($_POST['LoginForm']['email']))
				$response['errors']['email'] = 'Email is required.';

			if (empty($_POST['LoginForm']['password']))
				$response['errors']['password'] = 'Password is required.';

			// if errors
			if (!empty($response['errors'])) {
				// if there are items in our errors array, return those errors
				$data['status'] = false;
				$data['errors'] = $response['errors'];
			} else {
				// no form errors
				$model = new LoginForm;
				$model->attributes=$_POST['LoginForm'];

				// validate user input and login
				if($model->validate() && $model->login()) {
					// success
					$hasPaid = 1;
					$condition = 'userId=:userId AND hasPaid=:hasPaid';
					$params = array(
						':userId' => Yii::app()->user->id,
						':hasPaid' => $hasPaid
					);

					// fetch active user
					$activeUser = ActiveUser::model()->find(array(
						'condition'=>$condition,
						'params' => $params
					));

					// if found
					if($activeUser) {
						Yii::app()->user->setState("isActiveUser", 1); // set active state

						// set response attributes
						$resAttr['id'] = $activeUser->user->id;
						$resAttr['firstName'] = $activeUser->user->firstname;
						$resAttr['lastName'] = $activeUser->user->lastname;
						$resAttr['email'] = $activeUser->user->email;
						$resAttr['role'] = $activeUser->user->roles;
						$resAttr['hasPaid'] = $activeUser->hasPaid; // not from user model

						// set response
						$response['status'] = true;
						$response['data'] = $resAttr;
					} else {
						// user found, but not active
						// TODO: fetch user
					}
				} else {
					// failed to login
					$response['message'] = 'Login failed';
				}
			}
		} else {
			// not from post

			// set response attributes
			$resAttr['user'] = Yii::app()->user;

			// set response
			$response['status'] = true;
			$response['data'] = $resAttr;
		}

		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		// response
		$response = [];
		$response['data'] = null;
		$response['status'] = false;
		$response['errors'];
		$response['message'] = '';

		Yii::app()->user->logout();
//		$this->redirect(Yii::app()->homeUrl);

		$isGuest = Yii::app()->user->isGuest;
		$resAttr['isGuest'] = $isGuest;
		$response['data'] = $resAttr;

		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	/**** forgot password ****/
	private function randomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

	public function actionForgotPassword() {
		// json response
		$response = array();
		$response['state'] = true;
		$response['message'] = '';
		$response['data'] = false;

		// vars
		$userEmail = '';

		// if post request
		if(isset($_POST))
		{
			// find user
			$userEmail = $_POST['email'];

			$user = User::model()->findAll('email=:email',
				array('email'=>$userEmail));

			if (count($user) != 1) {
				$response['status'] = false;
				$response['message'] = 'Unable to find user';
			} else {
				// vars
				$pathToUserUpdate = '';

				if(self::ENV == 'dev'){
					$pathToUserUpdate = $this->prodRootPath;
				}

				// generate new password
				$newPassword = $this->randomPassword();
				$md5Pass = md5($newPassword);
				$user = $user[0]; // get first user record
				$user->password = $md5Pass; // update with new password
				$user->update();

				// set path to update password
				// $pathToUserUpdate .= '/user/update?id=' . $user->id;

                // http://dev.ufcpickems.com/#!/user/1/edit
                $pathToUserUpdate .= '/#!/user/'.$user->id.'/edit';

				// construct email
				$from = '<noreply@ufcpickems.com>';
				$to = $userEmail;
				$subject = 'UFC Pickems Password Reset Request';

				$message = 'A request has been made to reset your password.';
				$message .= '<br/>';
				$message .= 'Your temporary password is: ' . $newPassword;
				$message .= '<br/><br/>';
				$message .= 'To update your password first Login and then goto http://' . $_SERVER["HTTP_HOST"] . $pathToUserUpdate;

				$email = Yii::app()->email;
				$email->to = $to;
				$email->from = $from;
				$email->bcc = $this->adminEmail;
				$email->subject = $subject;
				$email->message = $message;
				$email->send();

				$response['message'] = 'Your request has successfully been sent.  Please check your email (spam maybe?).';

				$response['status'] = true;
			}
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

}