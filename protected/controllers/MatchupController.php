<?php

class MatchupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'getAll'),
				'users'=>array('*'),
				'roles'=>array('1'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'removeAllByEventId'),
//				'users'=>array('admin', 'lunarvision@gmail.com'),
				'roles'=>array('1'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionRemoveAllByEventId() {
        $response = array();
        $response['state'] = false;
        $response['msg'] = '';
        $response['data'] = false;

        // body type: raw
        $json = json_decode(file_get_contents("php://input"));

        // if has data property then submitted via app
        if(isset($json->{'data'})) {
            $json = $json->{'data'};
        }

        $eventId = $json->{'eventId'};

        // set check params
        $criteriaParams = array();
        $criteriaParams['eventId'] = $eventId;

        // create criteria obj
        $criteria = new CDbCriteria();

        // there are params
        if(count($criteriaParams) > 0) {
            // condition ex: eventId=:eventId AND fighterId=:fighter1ID OR eventId=:eventId AND fighterId=:fighter2ID
            $criteria->condition = 'eventId=:eventId';
            $criteria->params = $criteriaParams;
        }

        // if records are found
        $model = Matchup::model()->deleteAll($criteria);
        $response['data'] = $model;
        $response['state'] = true;
        $response['msg'] = 'Removed ' .$model. ' records';

        $this->layout=false;
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
    }

	public function actionGetAll() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		// helpers
		$didWin = function ($selectedFighterId, $winnerId) {
			if ($selectedFighterId == $winnerId) {
				return true;
			} else {
				return false;
			}
		};

		if(isset($_POST))
		{
			$message = '';
			$eventStatus = null;

			// post vars
			// set event id
			$eventId = null;
			$eventId = 21; // TODO: Remove after testing.
			if($_POST['eventId']) {
				$eventId = $_POST['eventId'];
				$message = '$eventId: ' . $eventId;
			} else {
				$message = 'ERROR: No eventId';
			}

			// matchup
			$matchups = array();

			// get matchups for the event
			$criteria = new CDbCriteria;
			$criteria->addInCondition('eventId', array($eventId));
			$matchupModel = Matchup::model()->findAll($criteria);

			foreach ($matchupModel as $matchup) {
				// pass event status $matchup-event_status
				$eventStatus = $matchup->event->status;

				// vars
				$winnerId = '';
				$hasTiebreaker = $matchup->hasTiebreaker;
				$defaultWinnerFinishTypeId = '999';
				$defaultWinnerRoundId = '999';

				/**** fighter 1 ****/
				$fighter1Id = $matchup->fighter1Id;
				$fighter1firstName = $matchup->fighter1->firstName;
				$fighter1lastName = $matchup->fighter1->lastName;

				// check if fighter 1 won
				$criteria = new CDbCriteria;
				$criteria->addInCondition('eventId', array($eventId));
				$criteria->addInCondition('fighterId', array($fighter1Id));
				$resultModel = Result::model()->findAll($criteria);

				// if winner found set properties
				if ($resultModel) {
					foreach ($resultModel as $result) {
						$winnerId = $result->fighter->id;
						$winnerFinishTypeId = $result->finishType == null ? $defaultWinnerFinishTypeId : $result->finishType->id;

						if ($winnerFinishTypeId) {
							$winnerFinishName = $result->finishType->finishName;
						}

						$winnerRoundId = $result->round == null ? $defaultWinnerRoundId : $result->round->id;
					}
				}

				/**** fighter 2 ****/
				$fighter2Id = $matchup->fighter2Id;
				$fighter2firstName = $matchup->fighter2->firstName;
				$fighter2lastName = $matchup->fighter2->lastName;

				// check if fighter 2 won
				$criteria = new CDbCriteria;
				$criteria->addInCondition('eventId', array($eventId));
				$criteria->addInCondition('fighterId', array($fighter2Id));
				$resultModel = Result::model()->findAll($criteria);

				// if winner found set properties
				if ($resultModel) {
					foreach ($resultModel as $result) {
						$winnerId = $result->fighter->id;
						$winnerFinishTypeId = $result->finishType == null ? $defaultWinnerFinishTypeId : $result->finishType->id;

						if($winnerFinishTypeId) {
							$winnerFinishName = $result->finishType->finishName;
						}

						$winnerRoundId = $result->round == null ? $defaultWinnerRoundId : $result->round->id;
					}
				}

				// create matchupObj
				$matchupO = new stdClass();
				$matchupO->hasTiebreaker = $hasTiebreaker;

				// fighter 1
				$fighterO = new stdClass();
				$fighterO->id = $fighter1Id;
				$fighterO->firstName = $fighter1firstName;
				$fighterO->lastName = $fighter1lastName;
				$matchupO->fighter1 = $fighterO; // set

				// fighter 2
				$fighterO = new stdClass();
				$fighterO->id = $fighter2Id;
				$fighterO->firstName = $fighter2firstName;
				$fighterO->lastName = $fighter2lastName;
				$matchupO->fighter2 = $fighterO; // set

				// winner id
				$matchupO->winnerId = $winnerId;
				$matchupO->winnerFinishTypeId = $winnerFinishTypeId;
				$matchupO->winnerFinishName = $winnerFinishName;
				$matchupO->winnerRoundId = $winnerRoundId;



				// picks
				$picks = false;

				// select all picks with user
				$criteria = new CDbCriteria;
				$criteria->with = array('user');
				$criteria->together = true;
				$criteria->select = array('*');
				$criteria->order = 'user.firstName ASC, user.lastName ASC';
				$criteria->addInCondition('fighterId', array($fighter1Id, $fighter2Id), 'OR');
				$criteria->addInCondition('eventId', array($eventId)); // have this below OR clause
				$picksModel = Pick::model()->findAll($criteria);

				// if picks are made
				if($picksModel) {
					$picks = array();

					// display each user's pick
					foreach ($picksModel as $pick) {
						$pickO = new stdClass();

						// vars
						$userId = $pick->user->id;
						$firstname = $pick->user->firstname;
						$lastInitial = substr($pick->user->lastname, 0, 1);
						$selectedFighter = $pick->fighter->firstName . ' ' . $pick->fighter->lastName;
						$selectedFighterId = $pick->fighter->id;
						$selectedFinishTypeId = $pick->finishType == null ? '' : $pick->finishType->id;
						$selectedFinishTypeName = $pick->finishType == null ? '' : $pick->finishType->finishName;
						$selectedRoundId = $pick->round == null ? '' : $pick->round->id;
						$selectedRoundName = $pick->round == null ? '' : $pick->round->round;
						$eventStatus = $pick->event->status;

						// calc standings
						if ($didWin($selectedFighterId, $winnerId) == true) {
							$pick->win = 1;
							if ($pick->save()) {}
						} else {
							$pick->win = 0;
							if ($pick->save()) {}
						}

						// has a tiebreaker
						$totalTbp = false; // tie breaker points
						if ($hasTiebreaker == '1') {
							$totalTbp = 0; // tie breaker points

							// user did win
							if ($didWin($selectedFighterId, $winnerId)) {
								// 1 point for round
								if ($selectedRoundId == $winnerRoundId) {
									// round selected correctly
									$totalTbp++;
								}

								// 1 point for finish type, but not decision (id 3)
								if ($selectedFinishTypeId == $winnerFinishTypeId && $winnerFinishTypeId != 3) {
									// finish method selected correctly
									$totalTbp++;
								}

								// set and save
								$pick->tiebreakerWin = $totalTbp;
								if ($pick->save()) {
								}
							} else {
								// user did not win
								$pick->tiebreakerWin = 0;
								if ($pick->save()) {
								}
							}
						}

						// set pick object
						$pickO->userId = $userId;
						$pickO->firstName = $firstname;
						$pickO->lastInitial = $lastInitial;
						$pickO->selectedFighter = $selectedFighter;
						$pickO->selectedFighterId = $selectedFighterId;
						$pickO->selectedFinishTypeId = $selectedFinishTypeId;
						$pickO->selectedFinishTypeName = $selectedFinishTypeName;
//						$pickO->selectedRoundId = $selectedRoundId;
						$pickO->selectedRoundName = $selectedRoundName;
//						$pickO->eventStatus = $eventStatus;
						$pickO->win = $pick->win;
						$pickO->totalTbp = $totalTbp;


						array_push($picks, $pickO); // push to array
						$matchupO->picks = $picks; // set on matchup object
					}
				}

				array_push($matchups, $matchupO);
			} // end matchup loop

			// rest result
			$result = array('eventId' => $eventId,
				'eventStatus' => $eventStatus,
				'matchups' => $matchups
			);

			$response['state'] = true;
			$response['msg'] = $message;
			$response['data'] = $result;
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Matchup;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Matchup']))
		{
			$model->attributes=$_POST['Matchup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Matchup']))
		{
			$model->attributes=$_POST['Matchup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// settings
		$msg = '';
		$defaultEventId = 1;

		// vars
		$eventModel = Event::model()->findByAttributes(array('status' => 1));
		if($eventModel){
			$eventId = $eventModel->id;
		} else {
			$eventId = $defaultEventId;
		}

		// set condition
		$condition = 'eventId='.$eventId;

		// create criteria
		$criteria = new CDbCriteria();
		$criteria->condition = $condition;

		// set data provider
		$dataProviderMatchup=new CActiveDataProvider('Matchup', array(
			'criteria' => $criteria
		));



		// render
		$this->render('index',array(
			'dataProvider'=>$dataProviderMatchup,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Matchup('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Matchup']))
			$model->attributes=$_GET['Matchup'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Matchup the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Matchup::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Matchup $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='matchup-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
