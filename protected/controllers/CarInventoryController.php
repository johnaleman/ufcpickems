<?php

class CarInventoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
//	public $layout='//layouts/column2';
	public $layout='//layouts/inventory';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index',
					'view',
					'requestLoadVehicleGallery'
				),
				'users' => array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(
					'create',
					'update',
					'delete',
					'requestSaveGallery',
					'requestSaveCarInventory',
					'requestLoadVehicleGallery'
				),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CarInventory;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CarInventory']))
		{
			$model->attributes=$_POST['CarInventory'];
			if($model->save())
				$this->redirect(array('index','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionRequestSaveCarInventory() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;
		$response['data']['isNew'] = 0;

		$carInventoryId = $_POST['carInventoryId'];
		$make = $_POST['make'];
		$carModel = $_POST['model'];
		$year = $_POST['year'];
		$vin = $_POST['vin'];
		$stockId = $_POST['stockId'];
		$interior = $_POST['interior'];
		$transmission = $_POST['transmission'];
		$miles = $_POST['miles'];
		$price = $_POST['price'];
		$mainImage = $_POST['mainImage'];

		if (isset($_POST)) {
			//init vars
			$model = CarInventory::model()->findByPk($carInventoryId);
			$model->make = $make;
			$model->model = $carModel;
			$model->year = $year;
			$model->vin = $vin;
			$model->stockId = $stockId;
			$model->interior = $interior;
			$model->transmission = $transmission;
			$model->miles = $miles;
			$model->price = $price;
			$model->mainImage = $mainImage;

			if ($model->save()) {
				$response['state'] = true;
				$response['data'] = $model;
			} else {
				$response['msg'] = 'CarInventory ID='+$carInventoryId+' did not save.';
			}
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionRequestSaveGallery() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;
		$response['data'] = 0;

		if (isset($_POST)) {
			//id vars
			$galleryId = $_POST['galleryId'];
			$carInventoryId = $_POST['carInventoryId'];

			// vars
			$title = $_POST['title'];
			$description = $_POST['description'];
			$imgUrl = $_POST['imgUrl'];

			// AR
//			$model = CarInventory::model()->findByPk($carInventoryId);
			$model = Gallery::model()->findByPk($galleryId);

			// a vehicle in car inventory exists
			// if gallery image exist do an update
			if($model)
			{
				// gallery already exists
				// update vars
			} else {
				// if a gallery for the vehicle does not exist
				$model = new Gallery();
				$response['msg'] = 'Error: CarInventoryId does not exist.';
			}

			$model->carInventoryId = $carInventoryId;
			$model->title = $title;
			$model->description = $description;
			$model->imgUrl = $imgUrl;

			if ($model->save()) {
				$response['state'] = true;
				$response['data'] = $model;
			} else {
				$response['msg'] = 'GalleryId ID='+$galleryId+' on Gallery did not save.';
			}
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionRequestLoadVehicleGallery() {
		// response
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		// vars
		$model = '';

		// validation
		if (isset($_POST)) {
			// vars
			$carInventoryId = $_POST['carInventoryId'];

			// define model
			$model = Gallery::model()->findAll(array(
				'condition'=>'carInventoryId=:carInventoryId',
				'params'=>array(
					':carInventoryId'=>$carInventoryId
				)
			));

			// return response
			if ($model) {
				$response['state'] = true;
				$response['data'] = $model;
			} else {
				$response['msg'] = 'ERROR on $carInventoryId ='+$carInventoryId+' does not exist.';
			}
		}

		// return
		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		error_log("actionUpdate");
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		/*if(isset($_POST['CarInventory']))
		{
			$model->attributes=$_POST['CarInventory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}*/

		if(isset($_POST['CarInventory']))
		{
			$model->attributes=$_POST['CarInventory'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('CarInventory');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CarInventory('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CarInventory']))
			$model->attributes=$_GET['CarInventory'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CarInventory the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CarInventory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CarInventory $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='car-inventory-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
