<?php

class FollowingController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionUnFollowUser() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		if($_POST['userId'] && $_POST['followUserId'])
		{
			$userId = $_POST['userId'];
			$followUserId = $_POST['followUserId'];

			// check to see if user is already following user
			$criteria = new CDbCriteria;
			$criteria->addInCondition('userId', array($userId));
			$criteria->addInCondition('following', array($followUserId));
			if(Following::model()->deleteAll($criteria)) {
				$response['state'] = true;
			}
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionFollowUser() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		if($_POST['userId'] && $_POST['followUserId'])
		{
			$userId = $_POST['userId'];
			$followUserId = $_POST['followUserId'];

			// check to see if user is already following user
			$criteria = new CDbCriteria;
			$criteria->addInCondition('userId', array($userId));
			$criteria->addInCondition('following', array($followUserId));
			$followingModel = Following::model()->findAll($criteria);

			if(!$followingModel) {
				// no match found
				// creat new one
				$followingModel = new Following();
				$followingModel->userId = $userId;
				$followingModel->following = $followUserId;

				if($followingModel->save()) {
					$response['state'] = true;
					$response['data'] = $followingModel;
				} else {
					$response['state'] = true;
					$response['msg'] = 'ERROR Saving';
				}
			} else {
				// found match
				$response['msg'] = 'User is already following';
			}
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionGetAll() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		if($_POST['userId'])
		{
			// vars
			$jsonObj = new stdClass();
			$tempArr = array();

			// post vars
			$userId = 1;
			if($_POST['userId']) {
				$userId = $_POST['userId'];
				$message = '$userId: ' . $userId;
			} else {
				$message = 'ERROR: No userId';
			}

			// find all users who I am following
			$criteria = new CDbCriteria;
			$criteria->with = array('userFollowing');
			$criteria->together = true;
			$criteria->select = array('*');
			$criteria->order = 'userFollowing.firstname ASC, userFollowing.lastname ASC';
			$criteria->addInCondition('userId', array($userId));
			$followingModel = Following::model()->findAll($criteria);

			if($followingModel) {
				$tempArr = array();
				foreach($followingModel as $following) {
					$followingObj = new stdClass();
					$followingObj->userId = $following->userFollowing->id;
					$followingObj->userName = $following->userFollowing->firstname . ' '. substr($following->userFollowing->lastname, 0, 1);
					array_push($tempArr, $followingObj);
				}
			}

			$jsonObj->followingCount = count($tempArr);
			$jsonObj->following = $tempArr;

			// find all users who are following me
			$criteria = new CDbCriteria;
			$criteria->with = array('user');
			$criteria->together = true;
			$criteria->select = array('*');
			$criteria->order = 'user.firstname ASC, user.lastname ASC';
			$criteria->addInCondition('following', array($userId));
			$followingModel = Following::model()->findAll($criteria);

			if($followingModel) {
				$tempArr = array();
				foreach($followingModel as $following) {
					$followingObj = new stdClass();
					$followingObj->userId = $following->user->id;
					$followingObj->userName = $following->user->firstname . ' '. substr($following->user->lastname, 0, 1);
					array_push($tempArr, $followingObj);
				}
			}

			$jsonObj->followersCount = count($tempArr);
			$jsonObj->followers = $tempArr;

			// set response data
			$response['data'] = $jsonObj;
			$response['state'] = true;
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionGetFollowing() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		$jsonArr = array();

		// set userId
		$userId = 3;

		$criteria = new CDbCriteria;
		$criteria->with = array('userFollowing'); // all of users who I am following
		$criteria->together = true;
		$criteria->select = array('*');
		$criteria->order = 'userFollowing.firstname ASC, userFollowing.lastname ASC';
		$criteria->addInCondition('userId', array($userId));
		$followingModel = Following::model()->findAll($criteria);

		if($followingModel)
		{
			$followingArr = array();
			foreach($followingModel as $following) {
				$followingObj = new stdClass();
				$followingObj->id = $following->id;
//				$followingObj->following = $following->following;
				$followingObj->following = $following->userFollowing->id;
				$followingObj->followingName = $following->userFollowing->firstname . ' '. substr($following->userFollowing->lastname, 0, 1);
				array_push($followingArr, $followingObj);
			}
		}

		array_push($jsonArr, $followingArr);
		$response['data'] = $followingArr;

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	public function actionGetFollowers() {
		$response = array();
		$response['state'] = false;
		$response['msg'] = '';
		$response['data'] = false;

		$jsonArr = array();

		// set userId
		$userId = 3;

		$criteria = new CDbCriteria;
		$criteria->with = array('user'); // all of users who I am following
		$criteria->together = true;
		$criteria->select = array('*');
		$criteria->order = 'user.firstname ASC, user.lastname ASC';
		$criteria->addInCondition('following', array($userId));
		$followingModel = Following::model()->findAll($criteria);

		if($followingModel)
		{
			$followingArr = array();
			foreach($followingModel as $following) {
				$followingObj = new stdClass();
				$followingObj->id = $following->id;
				$followingObj->user = $following->user->id;
				$followingObj->userName = $following->user->firstname . ' '. substr($following->user->lastname, 0, 1);
				array_push($followingArr, $followingObj);
			}
		}

		array_push($jsonArr, $followingArr);
		$response['data'] = $followingArr;

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}