<?php

class PickController extends Controller
{
    // VARS
//    private $adminEmail = 'joemommabee@yahoo.com';
	private $adminEmail = 'lunarvision@gmail.com';
    private $devEmail = 'lunarvision@gmail.com';

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('makePick', 'submitUserPicks', 'getAllPicksByEvent'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'create','update','delete'),
				//'users'=>array('admin', 'lunarvision@gmail.com'),
				'roles'=>array('1'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pick;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pick']))
		{
			$model->attributes=$_POST['Pick'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pick']))
		{
			$model->attributes=$_POST['Pick'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pick');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 *
	 */
	// TODO: [refactor] move action to events to have endpoint event/matchupList
	public function actionMakePick() {
		// response
		$response = [];
		$response['data'] = [];
		$response['status'] = false;
		$response['errors'];
		$response['message'] = '';

        // vars
        $userId = Yii::app()->user->getId(); // get user id

		// get events
        $settingsModel = Settings::model()->findByPk(1);
        $activeRound = $settingsModel->round;

        // criteria
        $criteria = new CDbCriteria;
        $criteria->addInCondition('round', array($activeRound));
        $criteria->order = 'status DESC, date DESC';
		$eventModel = Event::model()->findAll($criteria);

		// get events
		$eventArr = array();
		foreach($eventModel as $event) {
			// get the matchup model for a given event id
			$eventId = $event->id;
			$criteria = new CDbCriteria;
			$criteria->addInCondition('eventId', array($eventId));
			$matchupModel = Matchup::model()->findAll($criteria);

			// list matchups
			$matchupArr = array();
			foreach ($matchupModel as $matchup) {
				// vars
				$fighter1Id = $matchup->fighter1Id;
				$fighter2Id = $matchup->fighter2Id;
				$hasTiebreaker = $matchup->hasTiebreaker;
				$winnerId = '';
                $selectedFighterName = '';
                $roundId = '';
                $selectedRoundName = '';
                $finishTypeId = '';
                $selectedFinishTypeName = '';

				$id = $matchup->id;
				$fighter1ID = $matchup->fighter1->id;
				$fighter1FirstName = $matchup->fighter1->firstName;
				$fighter1LastName = $matchup->fighter1->lastName;
				$fighter1FullName = $fighter1FirstName .' '. $fighter1LastName;

				$fighter2ID = $matchup->fighter2->id;
				$fighter2FirstName = $matchup->fighter2->firstName;
				$fighter2LastName = $matchup->fighter2->lastName;
				$fighter2FullName = $fighter2FirstName .' '. $fighter2LastName;

                // check if fighter 1 is selected
                $criteria = new CDbCriteria;
                $criteria->addInCondition('userId', array($userId));
                $criteria->addInCondition('fighterId', array($fighter1Id));
                $criteria->addInCondition('eventId', array($eventId));
                $pickModel = Pick::model()->findAll($criteria);

                // set values if fighter 1 selected
                if($pickModel){
                    foreach($pickModel as $pick){
                        $winnerId = $pick->fighterId;
                        $selectedFighterName = $fighter1FullName;
                        $roundId = $pick->roundId;
                        $selectedRoundName = $pick->round->round;
                        $finishTypeId = $pick->finishTypeId;
                        $selectedFinishTypeName = $pick->finishType->finishName;
                    }
                }

                // check if fighter 2 is selected
                $criteria = new CDbCriteria;
                $criteria->addInCondition('userId', array($userId));
                $criteria->addInCondition('fighterId', array($fighter2Id));
                $criteria->addInCondition('eventId', array($eventId));
                $pickModel = Pick::model()->findAll($criteria);

                // set values if fighter 2 selected
                if($pickModel){
                    foreach($pickModel as $pick){
                        $winnerId = $pick->fighterId;
                        $selectedFighterName = $fighter2FullName;
                        $roundId = $pick->roundId;
                        $selectedRoundName = $pick->round->round;
                        $finishTypeId = $pick->finishTypeId;
                        $selectedFinishTypeName = $pick->finishType->finishName;
                    }
                }

				$matchupArr[] = (object) [
					'id' => $id,
                    'hasTiebreaker' => $hasTiebreaker,
					'fighter1ID' => $fighter1ID,
					'fighter1FullName' => $fighter1FullName,
					'fighter2ID' => $fighter2ID,
					'fighter2FullName' => $fighter2FullName,
                    'selectedFighterID' => $winnerId,
                    'selectedFighterName' => $selectedFighterName,
                    'selectedRoundID' => $roundId,
                    'selectedRoundName' => $selectedRoundName,
                    'selectedFinishTypeID' => $finishTypeId,
                    'selectedFinishTypeName' => $selectedFinishTypeName
				];
			}

			$eventArr[] = (object) array('event' => $event, 'matchup' => $matchupArr);
			$response['data'] = $eventArr;
		}

		$response['status'] = true;

		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	/**
	 *
	 */
	public function actionSubmitUserPicks(){
		$response = array();
		$response['status'] = false;
		$response['message'] = '';
		$response['data'] = false;

		// vars
		$userId = Yii::app()->user->getId(); // get user id

		if(isset($_POST))
		{
            $json = json_decode(file_get_contents("php://input"));

            // if has data property then submitted via app
            if(isset($json->{'data'})) {
                $json = $json->{'data'};
            }

            // post vars
            $matchupList = $json->{'matchup'};
            $eventId = $json->{'event'}->{'id'};
            $eventS = $json->{'event'}->{'name'};

            // remove previously selected picks
            $criteria = new CDbCriteria;
            $criteria->addInCondition('eventId', array($eventId));
            $criteria->addInCondition('userId', array($userId));
            Pick::model()->deleteAll($criteria);
            if(Pick::model()->deleteAll($criteria)){$response['status'] = true;}

            // loop through matchups
            $emailMsg = '';
            $fightersS = '';
            $selectedFighters = array();
            foreach($matchupList as $matchup) {
                // create new Pick
                $pick = new Pick();
                $pick->userId = $userId;
                $pick->fighterId = $matchup->{'selectedFighterID'};
                $pick->eventId = $eventId;

                // set selected fighter name
                $selectedFighterName = $matchup->{'selectedFighterName'};
                $fightersS .= $selectedFighterName;

                // if has tiebreaker
                if($matchup->{'hasTiebreaker'}) {
                    // set round id and finish type
                    $pick->roundId = $matchup->{'selectedRoundID'};
                    $pick->finishTypeId = $matchup->{'selectedFinishTypeID'};

                    if(!is_null($matchup->{'selectedFinishTypeID'}) && !is_null($matchup->{'selectedRoundID'})) {
                        $fightersS .= ' ('.$pick->finishType->finishName.' / ' .$pick->round->round. ')';
                    }
                }

                array_push($selectedFighters, $selectedFighterName);

                $fightersS .= '<br/>';

                // save
                if($pick->save()){
                    $response['status'] = true;
                    $response['message'] = 'SUCCESS: Picks submitted';
                }
            }

            $user = User::model()->findByPK($userId);

            //$lastInitial = substr($user->lastname, 0, 1);
            $userName = $user->firstname. ' ' .$user->lastname;
            $response['userName'] = $userName;
            $response['userEmail'] = $user->email;
            $response['data'] = $selectedFighters;

            // send confirmation to user
            $emailMsg = ''; // clear
            $emailMsg .= '**** UFC PICKEMS ****';
            $emailMsg .= '<br/>';
            $emailMsg .= 'You have submitted your picks!';
            $emailMsg .= '<br/><br/>';
            $emailMsg .= $fightersS;

            $subject = '[ufcpickems] ' .$eventS.' Picks!';

            $this->sendEmail($subject, $user->email, $emailMsg);

            // dev
            $emailMsg .= '[dev] copy submitted by ' .$userName;
            $this->sendEmail($subject, $this->devEmail, $emailMsg);
		}

		$this->layout=false;
		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

    function sendEmail($subject, $to, $msg) {
        $email = Yii::app()->email;
        $email->from = '<noreply@ufcpickems.com>';
        $email->to = $to; // user email
        $email->subject = $subject;
        $email->message = $msg;
        $email->send();
    }

    public function actionGetAllPicksByEvent($eventId = null) {
        $response = array();
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = false;

        if (isset($eventId) == false) {
            $response['message'] = 'Event id is required';
        } else {
            $userList = array();

            // result count
            $criteria = new CDbCriteria();
            $criteria->addInCondition('eventId', array($eventId));
            $resultLength = count(Result::model()->findAll($criteria));

            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                // get active user list and sort by first name, last name
                $criteria = new CDbCriteria();
                $criteria->with = array('user');
                $criteria->together = true;
                $criteria->select = array('*');
                $criteria->order = 'user.firstName ASC, user.lastName ASC';

                // loop through all active users
                $activeUserList = ActiveUser::model()->findAll($criteria);
                if($activeUserList) {
                    foreach($activeUserList as $user) {
                        $userID = $user->user->id;

                        // get all of the user's picks by event id and user id
                        $criteria = new CDbCriteria();
                        $criteria->addInCondition('eventId', array($eventId));
                        $criteria->addInCondition('userId', array($userID));
                        $pickModel = Pick::model()->findAll($criteria);

                        // clear pick list and loss win total
                        $pickIdx = 0;
                        $pickList = array();
                        $winTotal = 0;
                        $lossTotal = $resultLength;
                        $tiebreakerTotal = 0;
                        if($pickModel) {
                            // loop through each pick
                            foreach($pickModel as $pick) {
                                // update win and tiebreaker
                                $criteria = new CDbCriteria();
                                $criteria->addInCondition('eventId', array($eventId));
                                $criteria->addInCondition('fighterId', array($pick->fighterId));
                                $resultModel = Result::model()->find($criteria);
                                $pick->win = $resultModel ? 1 : 0;

                                if($pick->win == 1) {
                                    $winTotal++;
                                }
                                $lossTotal = $resultLength - $winTotal;

                                // check if match up has a tiebreaker
                                $hasTiebreaker = 0;

                                // check against fighter 1 id
                                $criteria = new CDbCriteria();
                                $criteria->addInCondition('eventId', array($eventId));
                                $criteria->addInCondition('hasTiebreaker', array(1));
                                $criteria->addInCondition( 'fighter1Id', array($pick->fighterId));
                                $matchupModel = Matchup::model()->find($criteria);
                                if($matchupModel) {$hasTiebreaker = 1;}

                                // check against fighter 2 id
                                $criteria = new CDbCriteria();
                                $criteria->addInCondition('eventId', array($eventId));
                                $criteria->addInCondition('hasTiebreaker', array(1));
                                $criteria->addInCondition( 'fighter2Id', array($pick->fighterId));
                                $matchupModel = Matchup::model()->find($criteria);
                                if($matchupModel) {$hasTiebreaker = 1;}

                                if($hasTiebreaker) {
                                    $total = 0;

                                    // did user make correct pick
                                    if($pick->win == 1) {
                                        // is qualified to get tiebreaker points per business rules

                                        // if correct finish type and type is NOT decision
                                        if($pick->finishTypeId == $resultModel->finishTypeId && $resultModel->finishTypeId != 3) {
                                            $total++;
                                        }

                                        // if correct round
                                        if($pick->roundId == $resultModel->roundId) {
                                            $total++;
                                        }

                                        $pick->tiebreakerWin = $total;
                                        $tiebreakerTotal = $total;
                                    }
                                }

                                // add to pick list
                                $pickList[] = (object) [
                                    'id' => $pick->id,
                                    'fighterID' => $pick->fighterId,
                                    'fighterName' => $pick->fighter->firstName . ' ' . $pick->fighter->lastName,
                                    'win' => $pick->win,
                                    'finishTypeID' => $pick->finishTypeId,
                                    'finishTypeName' => $pick->finishType->finishName,
                                    'roundID' => $pick->roundId,
                                    'roundName' => $pick->round->round,
                                    'tiebreakerWin' => $pick->tiebreakerWin,
                                    'hasTiebreaker' => $hasTiebreaker
                                ];

                                // save
                                if ($pick->save()) {}
                            }
                        }

                        $userList[] = (object) [
                            'id' => $userID,
                            'name' => $user->user->firstname . ' ' . mb_substr($user->user->lastname, 0, 1, 'utf-8'),
                            'eventID' => $eventId,
                            'winTotal' => $winTotal,
                            'lossTotal' => $lossTotal,
                            'tiebreakerTotal' => $tiebreakerTotal,
                            'pickList' => $pickList
                        ];
                    }
                }

                // set response
                $response['status'] = true;
                $response['data'] = (object) array('eventId' => $eventId, 'userList' => $userList);
            }
        }

        $this->layout=false;
        header('Content-type: application/json');
        echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
        Yii::app()->end();
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pick('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pick']))
			$model->attributes=$_GET['Pick'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pick the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pick::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pick $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pick-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
