<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				//'actions'=>array('view', 'update', 'create'),
				'actions'=>array('view', 'create', 'status'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin', 'index', 'create','update','delete'),
//				'users'=>array('admin'),
				'roles'=>array('1', '2'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin', 'John'),
				'roles'=>array('2'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		// response
		$response = [];
		$response['data'] = null;
		$response['status'] = false;
		$response['errors'];
		$response['message'] = '';
		$resAttr = null;

		if(isset($_POST['User']))
		{
			// validation
			if (empty($_POST['User']['firstname']))
				$response['errors']['firstname'] = 'Firstname is required.';

			if (empty($_POST['User']['lastname']))
				$response['errors']['lastname'] = 'Lastname is required.';

			if (empty($_POST['User']['username']))
				$response['errors']['username'] = 'Username is required.';

			if (empty($_POST['User']['email']))
				$response['errors']['email'] = 'Email is required.';

			if (empty($_POST['User']['password']))
				$response['errors']['password'] = 'Password is required.';

			// if errors
			if (!empty($response['errors'])) {
				// if there are items in our errors array, return those errors
				$data['status'] = false;
				$data['errors'] = $response['errors'];
			} else {
				// TODO: Check to see is user (email) already exists.

				$model=new User;
				$model->attributes=$_POST['User'];
				$model->password = md5($_POST['User']['password']);
				$model->phone = $_POST['User']['phone'];
                $now = date('Y-m-d H:i:s');
                $model->createdOn = $now;

				if($model->save()) {
					// set response attributes
					$resAttr['id'] = $model->id;
				} else {
					// failed to save
				}

				// set response
				$response['status'] = true;
				$response['data'] = $resAttr;
			}
		}

		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		if(Yii::app()->user->isGuest){
			$route = 'site/login';
			$url=$this->createUrl($route);
			$this->redirect($url);

			return;
		}

		$roles = Yii::app()->user->getState("roles");
		$userId = Yii::app()->user->getId(); // get user id

		if($id != $userId) {
			if ($roles != 1) {
				// if you are not the admin the go home
				$route = 'site/index';
				$url = $this->createUrl($route);
				$this->redirect($url);

				return;
			}
		}

		$model=$this->loadModel($id);
		$origPass = null;
		if($model) {
			$origPass = $model->password;
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];

			if($origPass != $_POST['User']['password']) {
				$model->password = md5($_POST['User']['password']);
			}

			$model->phone = $_POST['User']['phone'];

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionStatus() {
		// response
		$response = [];
		$response['data'] = [];
		$response['status'] = false;
		$response['errors'];
		$response['message'] = '';

		// vars
		$resAttr = [];
		$isGuest = Yii::app()->user->isGuest;
		$resAttr['isGuest'] = $isGuest;

		if(!$isGuest) {
			// check if active user
			$userId = Yii::app()->user->id;
			/*$hasPaid = 1;
			$condition = 'userId=:userId AND hasPaid=:hasPaid';
			$params = array(
				':userId' => $userId,
				':hasPaid' => $hasPaid
			);*/

            // do not check hasPaid
            $condition = 'userId=:userId';
            $params = array(
                ':userId' => $userId
            );

			// fetch active user
			$activeUser = ActiveUser::model()->find(array(
				'condition'=>$condition,
				'params' => $params
			));

			// if found
			if($activeUser) {
				Yii::app()->user->setState("isActiveUser", 1); // set active state

				// set response attributes
				$resAttr['id'] = $activeUser->user->id;
				$resAttr['firstName'] = $activeUser->user->firstname;
				$resAttr['lastName'] = $activeUser->user->lastname;
				$resAttr['username'] = $activeUser->user->username;
				$resAttr['email'] = $activeUser->user->email;
				$resAttr['role'] = $activeUser->user->roles;
				$resAttr['hasPaid'] = $activeUser->hasPaid; // not from user model
				$resAttr['isActiveUser'] = true;

				// set response
				$response['status'] = true;
				$response['data'] = $resAttr;
			} else {
				// user found, but not active
				// TODO: fetch user
				$user = $this->loadModel($userId);

				if($user) {
					$resAttr['id'] = $user->id;
					$resAttr['firstName'] = $user->firstname;
					$resAttr['lastName'] = $user->lastname;
                    $resAttr['username'] = $user->username;
					$resAttr['email'] = $user->email;
					$resAttr['role'] = $user->roles;
					$resAttr['isActiveUser'] = false;
				}
			}
		}

		$response['data'] = $resAttr;
		$response['status'] = true;

		header('Content-type: application/json');
		echo htmlspecialchars(CJSON::encode($response), ENT_NOQUOTES);
		Yii::app()->end();
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
