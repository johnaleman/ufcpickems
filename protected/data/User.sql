/*
 Navicat MySQL Data Transfer

 Source Server         : Local
 Source Server Version : 50529
 Source Host           : localhost
 Source Database       : pickems

 Target Server Version : 50529
 File Encoding         : utf-8

 Date: 04/19/2014 16:24:24 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `User`
-- ----------------------------
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `roles` tinyint(2) unsigned NOT NULL DEFAULT '2',
  `haspaid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `User`
-- ----------------------------
BEGIN;
INSERT INTO `User` VALUES ('1', 'John', 'Aleman', 'johna', 'lunarvision@gmail.com', '4b2d7a9f679d5dcfb67010bec01a345b', '1', '0'), ('2', 'admin', 'admin', 'admin', 'lunarvision@gmail.com', 'admin', '1', '0'), ('3', 'Jake', 'Phelps', 'jakep', 'jakedesilva1@gmail.com', '6297a8ca3e9edc006ac283873ed1a3b3', '1', '0'), ('6', 'Jeff', 'Brundage', 'jeffb', 'jephprospect@me.com', '6297a8ca3e9edc006ac283873ed1a3b3', '1', '0'), ('7', 'Joe', 'Victor', 'joev', 'joemommabee@yahoo.com', '6297a8ca3e9edc006ac283873ed1a3b3', '1', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
