# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 68.178.143.39 (MySQL 5.5.43-37.2-log)
# Database: devpickems
# Generation Time: 2016-06-23 23:48:24 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ActiveUser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ActiveUser`;

CREATE TABLE `ActiveUser` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `userId` smallint(4) unsigned NOT NULL,
  `hasPaid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_active_user_user_id` (`userId`),
  CONSTRAINT `fk_active_user_user_id` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Event`;

CREATE TABLE `Event` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '0',
  `round` smallint(4) unsigned DEFAULT '1',
  `sendReminder` tinyint(1) unsigned DEFAULT '0',
  `simpleTime` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Fighter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Fighter`;

CREATE TABLE `Fighter` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table FinishType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FinishType`;

CREATE TABLE `FinishType` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `finishName` varchar(20) NOT NULL DEFAULT 'KO/TKO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Following
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Following`;

CREATE TABLE `Following` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `userId` smallint(4) unsigned NOT NULL DEFAULT '0',
  `following` smallint(4) unsigned NOT NULL DEFAULT '0',
  `createdOn` datetime DEFAULT '0000-00-00 00:00:00',
  `updatedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_following_userId_user_id` (`userId`),
  KEY `fk_following_following_user_id` (`following`),
  CONSTRAINT `fk_following_following_user_id` FOREIGN KEY (`following`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_following_userId_user_id` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Matchup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Matchup`;

CREATE TABLE `Matchup` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `fighter1Id` smallint(4) unsigned NOT NULL,
  `fighter2Id` smallint(4) unsigned NOT NULL,
  `eventId` smallint(4) unsigned NOT NULL,
  `hasTiebreaker` smallint(4) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fighter1Id` (`fighter1Id`,`fighter2Id`),
  KEY `fighter1Id_2` (`fighter1Id`,`fighter2Id`),
  KEY `fighter1Id_3` (`fighter1Id`,`fighter2Id`),
  KEY `fighter1Id_4` (`fighter1Id`,`fighter2Id`),
  KEY `fighter2Id` (`fighter2Id`),
  KEY `fighter2Id_2` (`fighter2Id`),
  KEY `fighter2Id_3` (`fighter2Id`),
  KEY `eventId` (`eventId`),
  KEY `eventId_2` (`eventId`),
  CONSTRAINT `fk_matchup_2_fighter_Id` FOREIGN KEY (`fighter2Id`) REFERENCES `Fighter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matchup_event_id` FOREIGN KEY (`eventId`) REFERENCES `Event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matchup_fighter_id` FOREIGN KEY (`fighter1Id`) REFERENCES `Fighter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Pick
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Pick`;

CREATE TABLE `Pick` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `userId` smallint(4) unsigned NOT NULL,
  `fighterId` smallint(4) unsigned DEFAULT NULL,
  `eventId` smallint(4) unsigned NOT NULL,
  `win` smallint(4) unsigned DEFAULT NULL,
  `finishTypeId` smallint(4) unsigned DEFAULT NULL,
  `roundId` smallint(4) unsigned DEFAULT NULL,
  `tiebreakerWin` smallint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `userId_2` (`userId`),
  KEY `eventId` (`eventId`),
  KEY `fighterId` (`fighterId`),
  KEY `finishTypeId` (`finishTypeId`),
  KEY `roundId` (`roundId`),
  CONSTRAINT `fk_pick_event_id` FOREIGN KEY (`eventId`) REFERENCES `Event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pick_fighter_id` FOREIGN KEY (`fighterId`) REFERENCES `Fighter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pick_finish_type_id` FOREIGN KEY (`finishTypeId`) REFERENCES `FinishType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pick_round_id` FOREIGN KEY (`roundId`) REFERENCES `Round` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pick_user_Id` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Result
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Result`;

CREATE TABLE `Result` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `fighterId` smallint(4) unsigned NOT NULL,
  `eventId` smallint(4) unsigned NOT NULL,
  `finishTypeId` smallint(4) unsigned DEFAULT NULL,
  `roundId` smallint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fighterId` (`fighterId`),
  KEY `eventId` (`eventId`),
  KEY `finishTypeId` (`finishTypeId`),
  KEY `roundId` (`roundId`),
  CONSTRAINT `fk_result_event_id` FOREIGN KEY (`eventId`) REFERENCES `Event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_result_fighter_id` FOREIGN KEY (`fighterId`) REFERENCES `Fighter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_result_finish_type_id` FOREIGN KEY (`finishTypeId`) REFERENCES `FinishType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_result_round_id` FOREIGN KEY (`roundId`) REFERENCES `Round` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Round
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Round`;

CREATE TABLE `Round` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `round` varchar(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Settings`;

CREATE TABLE `Settings` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `isOpenEnrollment` smallint(4) NOT NULL DEFAULT '0',
  `round` smallint(4) unsigned NOT NULL DEFAULT '0',
  `createdon` datetime DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Standing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Standing`;

CREATE TABLE `Standing` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `userId` smallint(4) unsigned NOT NULL,
  `win` smallint(4) unsigned DEFAULT NULL,
  `loss` smallint(4) unsigned NOT NULL DEFAULT '0',
  `tiebreakerWin` smallint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `fk_standing_user_id` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table User
# ------------------------------------------------------------

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `roles` tinyint(2) unsigned NOT NULL DEFAULT '2',
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
