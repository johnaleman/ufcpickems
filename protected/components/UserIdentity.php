<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * @var string email
	 */
	public $email;
	public $firstname;

	/**
	 * @var int id
	 */
	private $_id;

	const ERROR_EMAIL_INVALID=1;

	public function __construct($username, $password, $email)
	{
		parent::__construct($username, $password);
		$this->email=$email;
	}

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the email and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$model=User::model()->findByAttributes(array('email'=>$this->email));
		if($model===null)
			$this->errorCode=self::ERROR_EMAIL_INVALID;
		//elseif(!crypt($this->password, $model->password))
		else if($model->password!==md5($this->password)) // md5 encryption
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else {
			// success login
			$this->errorCode=self::ERROR_NONE;
			$this->_id=$model->id;
			$this->firstname=$model->firstname;
			$this->setState('roles', $model->roles);
			$this->setState('email', $model->email);
		}

		return !$this->errorCode;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}