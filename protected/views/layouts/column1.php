<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content" class="text-center">
	<?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>