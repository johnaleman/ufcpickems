<?php
/* @var $this FighterController */
/* @var $model Fighter */

$this->menu=array(
	array('label'=>'List Fighter', 'url'=>array('index')),
	array('label'=>'Create Fighter', 'url'=>array('create')),
	array('label'=>'View Fighter', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Fighter', 'url'=>array('admin')),
);
?>

<h1>Update Fighter <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>