<?php
/* @var $this FighterController */
/* @var $model Fighter */

$this->menu=array(
	array('label'=>'List Fighter', 'url'=>array('index')),
	array('label'=>'Create Fighter', 'url'=>array('create')),
	array('label'=>'Update Fighter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Fighter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Fighter', 'url'=>array('admin')),
);
?>

<h1>View Fighter #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'firstName',
		'lastName',
	),
)); ?>
