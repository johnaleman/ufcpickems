<?php
/* @var $this FighterController */
/* @var $model Fighter */

$this->menu=array(
	array('label'=>'List Fighter', 'url'=>array('index')),
	array('label'=>'Manage Fighter', 'url'=>array('admin')),
);
?>

<h1>Create Fighter</h1>

<div class="errorSummary">
	<p><?php echo $message; ?></p>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>