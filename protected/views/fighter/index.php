<?php
/* @var $this FighterController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Create Fighter', 'url'=>array('create')),
	array('label'=>'Manage Fighter', 'url'=>array('admin')),
);
?>

<h1>Fighters</h1>

<?php $this->widget('bootstrap.widgets.BsListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
