<?php
/* @var $this StandingController */
/* @var $model Standing */

$this->breadcrumbs=array(
	'Standings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Standing', 'url'=>array('index')),
	array('label'=>'Create Standing', 'url'=>array('create')),
	array('label'=>'View Standing', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Standing', 'url'=>array('admin')),
);
?>

<h1>Update Standing <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>