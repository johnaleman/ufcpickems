<?php
/* @var $this StandingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Standings',
);

$this->menu=array(
	array('label'=>'Create Standing', 'url'=>array('create')),
	array('label'=>'Manage Standing', 'url'=>array('admin')),
);
?>

<h1>Standings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
