<?php
/* @var $this StandingController */
/* @var $data Standing */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('win')); ?>:</b>
	<?php echo CHtml::encode($data->win); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loss')); ?>:</b>
	<?php echo CHtml::encode($data->loss); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiebreaker')); ?>:</b>
	<?php echo CHtml::encode($data->tiebreaker); ?>
	<br />


</div>