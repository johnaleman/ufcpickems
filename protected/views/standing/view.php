<?php
/* @var $this StandingController */
/* @var $model Standing */

//$this->breadcrumbs=array(
//	'Standings'=>array('index'),
//	$model->id,
//);

$this->menu=array(
	array('label'=>'List Standing', 'url'=>array('index')),
	array('label'=>'Create Standing', 'url'=>array('create')),
	array('label'=>'Update Standing', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Standing', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Standing', 'url'=>array('admin')),
);
?>

<h1>View Standing #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userId',
		'win',
		'loss',
		'tiebreaker',
	),
)); ?>
