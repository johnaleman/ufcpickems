<?php
/* @var $this StandingController */
/* @var $model Standing */

$this->breadcrumbs=array(
	'Standings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Standing', 'url'=>array('index')),
	array('label'=>'Manage Standing', 'url'=>array('admin')),
);
?>

<h1>Create Standing</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>