<?php
/* @var $this StandingController */
/* @var $model Standing */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget(('bootstrap.widgets.BsActiveForm'), array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'win'); ?>
		<?php echo $form->textField($model,'win'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'loss'); ?>
		<?php echo $form->textField($model,'loss'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tiebreaker'); ?>
		<?php echo $form->textField($model,'tiebreaker'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->