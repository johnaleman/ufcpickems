<?php
/* @var $this StandingController */
/* @var $model Standing */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget(('bootstrap.widgets.BsActiveForm'), array(
	'id'=>'standing-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
		<?php echo $form->error($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'win'); ?>
		<?php echo $form->textField($model,'win'); ?>
		<?php echo $form->error($model,'win'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'loss'); ?>
		<?php echo $form->textField($model,'loss'); ?>
		<?php echo $form->error($model,'loss'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tiebreaker'); ?>
		<?php echo $form->textField($model,'tiebreaker'); ?>
		<?php echo $form->error($model,'tiebreaker'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->