<?php
/**
 * Created by PhpStorm.
 * User: johnaleman
 * Date: 12/30/14
 * Time: 10:39 PM
 */
?>

<!--
<script>
	alert( <?php echo $wikiLink ?> );
</script>
-->

<div id="create-fighter" class="container">
	<div class="input-holder text-center">
		<input id="input-wiki-link" class="input-wiki-link" type="text" placeholder="WIKIPEDIA LINK" value="http://en.wikipedia.org/wiki/UFC_on_Fox:_Gustafsson_vs._Johnson"/>
	</div>

	<div class="submit-btn-row">
		<button id="submit-create-fighter-btn" type="submit" class="btn btn-primary">Submit</button>
	</div>

	<div id="create-fighter-output" class="create-fighter-output"></div>
</div>