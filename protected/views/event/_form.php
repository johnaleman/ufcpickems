<?php
/* @var $this EventController */
/* @var $model Event */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
	'id'=>'event-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'date'); ?>
<!--		--><?php //echo $form->textField($model,'date'); ?>
<!--		--><?php //echo $form->error($model,'date'); ?>
<!--	</div>-->

	<div class="row">
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
			array(
				'name' => 'date',
				'attribute' => 'date',
				'model'=>$model,
				'options'=> array(
					'dateFormat' =>'yy-mm-dd',
					'altFormat' =>'yy-mm-dd',
					'changeMonth' => true,
					'changeYear' => true,
//					'onSelect'=>'js:function(i,j){
//						   function JSClock() {
//							  var time = new Date();
//							  var hour = time.getHours();
//							  var minute = time.getMinutes();
//							  var second = time.getSeconds();
//							  var temp="";
//							  temp +=(hour<10)? "0"+hour : hour;
//							  temp += (minute < 10) ? ":0"+minute : ":"+minute ;
//							  temp += (second < 10) ? ":0"+second : ":"+second ;
//							  return temp;
//							}
//
//							$v=$(this).val();
//							$(this).val($v+" "+JSClock());
//
//					 }',
					//'appendText' => 'yyyy-mm-dd',
				),
				'htmlOptions'=>array(
//					'style'=>'height:20px;'
					'class' => 'form-control',
					'placeholder' => 'yyyy-mm-dd',
				),
			));
		?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'round'); ?>
		<?php echo $form->textField($model,'round'); ?>
		<?php echo $form->error($model,'round'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sendReminder'); ?>
		<?php echo $form->textField($model,'sendReminder'); ?>
		<?php echo $form->error($model,'sendReminder'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'simpleTime'); ?>
		<?php echo $form->textField($model,'simpleTime'); ?>
		<?php echo $form->error($model,'simpleTime'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->