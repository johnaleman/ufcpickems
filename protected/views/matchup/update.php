<?php
/* @var $this MatchupController */
/* @var $model Matchup */

$this->menu=array(
	array('label'=>'List Matchup', 'url'=>array('index')),
	array('label'=>'Create Matchup', 'url'=>array('create')),
	array('label'=>'View Matchup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Matchup', 'url'=>array('admin')),
);
?>

<h1>Update Matchup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>