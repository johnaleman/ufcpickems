<?php
/* @var $this MatchupController */
/* @var $model Matchup */
/* @var $form CActiveForm */
?>

<?php
	//$fighterModel = new Fighter();
	$fighterModel = Fighter::model()->findAllByPk($model->id);
?>

<div class="wide form">

<?php $form=$this->beginWidget('BsActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'id'); ?>
<!--		--><?php //echo $form->textField($model,'id'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->label($model,'fighter1'); ?>
		<?php echo $form->textField($model,'fighter1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fighter2Id'); ?>
		<?php echo $form->textField($model,'fighter2Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eventId'); ?>
		<?php echo $form->textField($model,'eventId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hasTiebreaker'); ?>
		<?php echo $form->textField($model,'hasTiebreaker'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->