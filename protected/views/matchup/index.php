<?php
/* @var $this MatchupController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->menu=array(
	array('label'=>'Create Matchup', 'url'=>array('create')),
	array('label'=>'Manage Matchup', 'url'=>array('admin')),
);
?>

<div id="event-name-section">
	<h2>
		<div class="event-name">
<!--			--><?php //echo $dataProvider->getData()[0]->event->name;?>

			<?php
				// vars
				$eventModel = Event::model()->findByAttributes(array('status' => 1));
				if($eventModel){
					$eventId = $eventModel->id;
				} else {
					return;
				}
			?>

			<?php echo $eventModel->name; ?>
		</div>
	</h2>
</div>

<?php $this->widget('bootstrap.widgets.BsListView', array(
	'dataProvider'=>$dataProvider,
	'enablePagination'=>false,
	'summaryText'=>'',
	'itemView'=>'_view',
)); ?>
