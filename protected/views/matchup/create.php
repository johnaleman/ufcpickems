<?php
/* @var $this MatchupController */
/* @var $model Matchup */


?>

<h1>Create Matchup</h1>

<div class="admin">
	<?php
	$this->menu=array(
		array('label'=>'List Matchup', 'url'=>array('index')),
		array('label'=>'Manage Matchups', 'url'=>array('admin')),
	);
	?>
</div>

<div class="content">
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
