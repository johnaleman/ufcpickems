<?php
/* @var $this MatchupController */
/* @var $model Matchup */

$this->menu=array(
	array('label'=>'List Matchup', 'url'=>array('index')),
	array('label'=>'Create Matchup', 'url'=>array('create')),
	array('label'=>'Update Matchup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Matchup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Matchup', 'url'=>array('admin')),
);
?>

<h1>View Matchup #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'label'=>'Event',
			'value'=>$model->event->name,
		),
		array(
			'label'=>'Fighter 1 ID',
			'value'=>$model->fighter1->id,
		),
		array(
			'label'=>'Fighter 1',
			'value'=>$model->fighter1->firstName .' '. $model->fighter1->lastName,
		),
		array(
			'label'=>'Fighter 2 ID',
			'value'=>$model->fighter2->id,
		),
		array(
			'label'=>'Fighter 2',
			'value'=>$model->fighter2->firstName .' '. $model->fighter2->lastName,
		),
		array(
			'label'=>'Tiebreaker',
			'value'=>$model->hasTiebreaker,
		)
	)
)); ?>
