<?php
/* @var $this MatchupController */
/* @var $model Matchup */
/* @var $form CActiveForm */
?>

<div id="admin-create-matchup" class="form">

	<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
		'id'=>'matchup-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col">
			<div class="name">Select Fighter 1</div>
			<?php
			$criteria = new CDbCriteria();
			$criteria->with = array('matchups1');
//			$criteria->together = true;
			$criteria->select = 't.id, t.firstName, t.lastName, matchups1.id'; // you can only select relation.id
			$criteria->order = 't.lastName ASC';
			$fighterData = Fighter::model()->findAll($criteria);

			echo $form->dropDownList($model,'fighter1Id',
				CHtml::listData($fighterData,'id', function($fighterData) {
					return CHtml::encode($fighterData->lastName .', '. $fighterData->firstName);
				}),
				array('empty'=>'Select Fighter'));
			?>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="name">Select Fighter 2</div>
			<?php
			$criteria = new CDbCriteria();
			$criteria->with = array('matchups');
//			$criteria->together = true;
			$criteria->select = 't.id, t.firstName, t.lastName, matchups.id'; // you can only select relation.id
			$criteria->order = 't.lastName ASC';
			$fighterData = Fighter::model()->findAll($criteria);

			echo $form->dropDownList($model,'fighter2Id',
				CHtml::listData($fighterData,'id', function($fighterData) {
					return CHtml::encode($fighterData->lastName .', '. $fighterData->firstName);
				}),
				array('empty'=>'Select Fighter'));
			?>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="name">Select Event</div>
			<?php
			$criteria = new CDbCriteria();
			$criteria->with = array('matchups');
//			$criteria->together = true;
			$criteria->select = 't.id, t.name, matchups.id'; // you can only select relation.id
			$criteria->order = 't.date DESC';
			$criteria->addInCondition('round', array(Yii::app()->user->getState("activeRound")));
			$eventData = Event::model()->findAll($criteria);

			echo $form->dropDownList($model,'eventId',
				CHtml::listData($eventData,'id', function($matchupWidthEventData) {
					return CHtml::encode($matchupWidthEventData->name);
				}),
				array('empty'=>'Select Event'));
			?>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hasTiebreaker'); ?>
		<?php echo $form->textField($model,'hasTiebreaker'); ?>
		<?php echo $form->error($model,'hasTiebreaker'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->