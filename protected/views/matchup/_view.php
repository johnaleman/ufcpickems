<?php
/* @var $this MatchupController */
/* @var $data Matchup */
?>

<div class="view">
	<b><?php echo CHtml::encode($data->fighter1->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->fighter1->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->fighter1->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->fighter1->lastName); ?>
	<br />

	<div><h4>VS</h4></div>

	<b><?php echo CHtml::encode($data->fighter2->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->fighter2->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->fighter2->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->fighter2->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hasTiebreaker')); ?>:</b>
	<?php echo CHtml::encode($data->hasTiebreaker); ?>
	<br />

	<hr/>
</div>