<?php
/* @var $this MatchupController */
/* @var $model Matchup */

$this->menu=array(
	array('label'=>'List Matchup', 'url'=>array('index')),
	array('label'=>'Create Matchup', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#matchup-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Matchups</h1>

<!--<p>-->
<!--You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>-->
<!--or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.-->
<!--</p>-->
<!---->
<?php echo CHtml::link('Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>
<!-- search-form -->

<?php $this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'matchup-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'eventId',
		array(
		  'header'=>'Fighter 1',
			'value'=>'$data->fighter1->firstName.\' \'.$data->fighter1->lastName',
		),
		array(
			'header'=>'Fighter 2',
			'value'=>'$data->fighter2->firstName.\' \'.$data->fighter2->lastName',
		),
		array(
			'header'=>'Event Name',
			'value'=>'$data->event->name',
		),
		array(
			'header'=>'Tiebreaker',
			'value'=>'$data->hasTiebreaker',
		),
		array(
			'class'=>'CButtonColumn',
		)
	),
)); ?>
