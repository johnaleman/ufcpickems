<?php
/**
 * Created by PhpStorm.
 * User: johnaleman
 * Date: 3/31/14
 * Time: 4:22 AM
 */

// vars
$userId = Yii::app()->user->getId(); // get user id

$html = '';
$html .= '<h1>Current Standings</h1>';
echo $html;

$this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'result-standings-grid',
	'summaryText' => '',
	'dataProvider'=>$dataProvider,
	//'filter'=>$dataProvider,
	'type' => BsHtml::GRID_TYPE_STRIPED,
	'columns'=>array(
		array(
			'name' => '',
			//'value' => 'BsHtml::badge(($row + 1))',
			'value' => '($row + 1)',
			'htmlOptions'=>array('width'=>'40px'),
			'type' => 'raw'
		),
		array(
			'header'=>'Name',
			'value'=>'$data->user->firstname ." ".substr($data->user->lastname, 0, 1)',
		),
		array(
			'header'=>'Win',
			'value'=>'$data->win',
		),
		array(
			'header'=>'Loss',
			'value'=>'$data->loss',
		),
		array(
			'header'=>'Tiebreakers',
			'value'=>'$data->tiebreakerWin',
		),
	),
));

?>