<?php
/* @var $this ResultController */
/* @var $data Result */
?>

<div class="view">

<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('id')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
<!--	<br />-->

<!--	<b>--><?php //echo CHtml::encode($data->fighter1->getAttributeLabel('firstName')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->fighter1->firstName); ?>
<!--	<br />-->

	<b><?php echo CHtml::encode($data->fighter->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->fighter->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->fighter->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->fighter->lastName); ?>
	<br />

<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('fighterId')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->fighterId); ?>
<!--	<br />-->

<!--	<b>--><?php //echo CHtml::encode($data->event->getAttributeLabel('name')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->event->name); ?>
<!--	<br />-->

<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('eventId')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->eventId); ?>
	<br />

	<hr/>
</div>