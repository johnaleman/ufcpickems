<?php
/* @var $this ResultController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
	// helpers
	$didWin = function ($selectedFighterId, $winnerId) {
		if ($selectedFighterId == $winnerId) {
			return true;
		} else {
			return false;
		}
	};

	// vars
	$html = '';
	$role = Yii::app()->user->getState("roles");
	$activeRound = Yii::app()->user->getState("activeRound");

	// if admin
	if ($role == '1') {
		$html .= '<div id="results-admin" class="admin row">';
		$html .= '<h3>Admin</h3>';
		$html .= '<div id="admin-options" class="admin-options">';
		$html .= '<div><a href="create">Create</a></div>';
		$html .= '<div><a href="admin">Admin</a></div>';
		$html .= '</div>';
		$html .= '</div>';
	}

	$html .= '<div id="results-page" class="results-page row test">';

	// loop through all of the events
	$criteria = new CDbCriteria;
	$criteria->order = 'date DESC';
	//$eventModel = Event::model()->findAll($criteria);

	// get event for current round
	$eventModel = Event::model()->findAll(array(
		'condition' => 'round=:round',
		'params' => array(
			':round' => $activeRound
		),
		'order' => 'date DESC'
	));

	$index = 1;
	foreach ($eventModel as $event) {
		$html .= '<div class="event-section" data-event-id="'.$event->id.'">';

		$date = date_format(date_create($event->date), 'D M j');
		$html .= '<div class="event-title-holder">';
		$html .= 	'<div class="event-title"> ('.$index.') '.$event->name.'</div>';
		$html .=	'<div class="event-date">' . $date . ' @ ' . $event->simpleTime . ' PST' . '</div>';
		$html .= 	'<div class="event-location">' . $event->location. '</div>';
		$html .= '</div>';

		// matchups
		$html .= '<div class="matchup-container">';

		// get matchups for the event
		$eventId = $event->id;
		$criteria = new CDbCriteria;
		$criteria->addInCondition('eventId', array($eventId));
		$matchupModel = Matchup::model()->findAll($criteria);

		$html .= '</div>'; // end matchup-container
		$html .= '</div>'; // end event-section

		$index++;
	} // end loop

	$html .= '</div>';

	echo $html;
?>