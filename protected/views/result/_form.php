<?php
/* @var $this ResultController */
/* @var $model Result */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget(('bootstrap.widgets.BsActiveForm'), array(
	'id'=>'result-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col">
			<div class="name">Select Fighter </div>
			<?php
			$criteria = new CDbCriteria();
			$criteria->select = 't.id, t.firstName, t.lastName';
			$criteria->order = 't.lastName ASC';
			$fighterData = Fighter::model()->findAll($criteria);
			//$fighterModel = Fighter::model();

			// populate dropdown
			//$model,$attribute,$data,$htmlOptions
			echo $form->dropDownList($model,
				'fighterId',
				CHtml::listData($fighterData, 'id', function($fighter) {
					return CHtml::encode($fighter->lastName .', '. $fighter->firstName);
				}),
				array('name'=>'Result[fighterId]','selected'=>'3', 'empty'=>'Select Fighter'));
			?>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="name">Select Event</div>
			<?php
			// get retailer id, name from i2theme
			$criteria = new CDbCriteria();
			$criteria->select = 't.id, t.name, t.round';
			$criteria->addInCondition('t.round', array(Yii::app()->user->getState("activeRound")));
			$criteria->order = 't.id DESC';
			$eventData = Event::model()->findAll($criteria);
			//$eventModel = Event::model();

			// populate dropdown
			echo $form->dropDownList($model,
				'eventId',
				CHtml::listData($eventData, 'id', function($event) {
					return CHtml::encode($event->name);
				}),
				array('name'=>'Result[eventId]', 'empty'=>'Select Event'));
			?>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<?php
			// get retailer id, name from i2theme
			$criteria = new CDbCriteria();
			$criteria->select = 't.id, t.round';
			$criteria->order = 't.id ASC';
			$roundModel = Round::model()->findAll($criteria);
			echo $form->labelEx($model,'round');

			// populate dropdown
			echo $form->dropDownList($model,
				'roundId',
				CHtml::listData($roundModel, 'id', function($round) {
					return CHtml::encode($round->round);
				}),
				array('name'=>'Result[roundId]', 'empty'=>'Select Round'));
			?>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<?php
			// get retailer id, name from i2theme
			$criteria = new CDbCriteria();
			$criteria->select = 't.id, t.finishName';
			$criteria->order = 't.id ASC';
			$finishTypeModel = FinishType::model()->findAll($criteria);

			echo $form->labelEx($model,'finishType');

			// populate dropdown
			echo $form->dropDownList($model,
				'finishTypeId',
				CHtml::listData($finishTypeModel, 'id', function($finishType) {
					return CHtml::encode($finishType->finishName);
				}),
				array('empty'=>'Select Finish Type'));
			?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->