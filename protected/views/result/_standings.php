<?php
/* @var $this ResultController */
/* @var $data Result */
?>

<div id="standings-view" class="view">

	<b><?php echo CHtml::encode($data->user->getAttributeLabel('firstname')); ?>:</b>
	<?php echo CHtml::encode($data->user->firstname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('win')); ?>:</b>
	<?php echo CHtml::encode($data->win); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loss')); ?>:</b>
	<?php echo CHtml::encode($data->loss); ?>
	<br />


	<br />
	<hr/>
</div>