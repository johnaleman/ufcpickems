<?php
/* @var $this ResultController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$html = '';

$role = Yii::app()->user->getState("roles");
//if($role == '1')
//{
//	$html .= '<div id="results-admin" class="admin row">';
//	$html .= 	'<h3>Admin</h3>';
//	$html .= 	'<div id="admin-options" class="admin-options">';
//	$html .= 		'<div><a href="create">Create</a></div>';
//	$html .= 		'<div><a href="admin">Admin</a></div>';
//	$html .= 	'</div>';
//	$html .= '</div>';
//}

$html .= '<div id="results-page" class="results-page row test">';

// loop through all of the events
//$criteria = new CDbCriteria();
//$criteria->order = 'date DESC';
//$Criteria->condition = "status = 1";
//$eventModel = Event::model()->findAll($criteria);

// condition vars
$statusId = 1;

/*
 * valid when condition is a string and not an object
 * reference: http://www.yiiframework.com/forum/index.php/topic/48287-cactiverecordfindall/
 */
$eventModel = Event::model()->findAll(
	'status=:status',
	array(':status' => $statusId)
);

foreach ($eventModel as $event){
	$eventId = $event->id;

	$html .= '<div class="viewpick-section">';

	$html .= 	'<div class="viewpick-title">';
	$html .= 			'<div class="event-name">';
	$html .= 				$event->name;
	$html .= 			'</div>';
	$html .= 	'</div>';

	$sql = "SELECT
			*
			FROM
			Pick p
			LEFT JOIN Fighter f ON p.fighterId = f.id
			LEFT JOIN `User` u ON p.userId = u.id
			WHERE
			p.eventId = ".$eventId."
			GROUP BY
			userId
			ORDER BY
			u.firstname ASC";

	$pickModel = Pick::model()->findAllBySql($sql);

	foreach($pickModel as $pick){
		$userId = $pick->userId;
		$eventId = $pick->eventId;
		$lastInitial = substr($pick->user->lastname, 0, 1);

		$html .= '<div class="viewpick-title">';
		$html .= 	'<div class="user-name">';
		$html .= 		'<div class="first-name">'.$pick->user->firstname.'&nbsp;</div>';
		$html .= 		'<div class="last-name">'.$lastInitial.'</div>';
		$html .= 	'</div>';
		$html .= '</div>';

		$sql = "
			SELECT
			*
			FROM
			Pick p
			LEFT JOIN Fighter f ON p.fighterId = f.id
			WHERE
			p.userId = ".$userId."
			AND
			p.eventId = ".$eventId."
			";

		$pickModel = Pick::model()->findAllBySql($sql);

		$html .= 	'<div class="fighter-names">';
		foreach($pickModel as $pick){
			$html .= 	'<div class="fighter-name">';
			$html .= 		'<div class="first-name">'.$pick->fighter->firstName.'&nbsp;</div>';
			$html .= 		'<div class="last-name">'.$pick->fighter->lastName. '</div>';

			$finishTypeId = $pick->finishTypeId;
			$finishName = $pick->finishTypeId;
			$roundId = $pick->roundId;
			$roundName = $pick->finishTypeId;

			if(!is_null($finishTypeId) && !is_null($roundId)) {
				$html .= 	'<div class="finish">('.$pick->finishType->finishName.' / ' .$pick->round->round.')</div>';
				//$fightersS .= ' ('.$pick->finishType->finishName.' / ' .$pick->round->round. ')';
			}

			$html .= 	'</div>';
		}
		$html .= 	'</div>';
	}

	$html .= '</div>'; // end matchup-container
	$html .= '</div>'; // end event-section
}

$html .= '</div>';

echo $html;
?>