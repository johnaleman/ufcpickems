<?php
/**
 * Created by PhpStorm.
 * User: johnaleman
 * Date: 3/31/14
 * Time: 4:22 AM
 */

// vars
$html = ''; // empty markup
$userId = Yii::app()->user->getId(); // get user id

// condition vars
$statusId = 1;

/*
 * valid when condition is a string and not an object
 * reference: http://www.yiiframework.com/forum/index.php/topic/48287-cactiverecordfindall/
 */
$eventModel = Event::model()->findAll(
	'status=:status',
	array(':status' => $statusId)
);

// if there is no event to pick from
if(!$eventModel){
	$html = '<div class="error_friendly">Damn, there is no upcoming event selected.</div>';
	echo $html;
	return;
}

// TODO: Creaet #page-make-pick div
//$html .= '<div class="disclaimer"><h2>The main card has not been confirmed yet.  Checkback soon.</h2></div>';
//
//echo $html;
//return;

// display each event found
$eventIndex = 0;
foreach($eventModel as $event) {
	// markup
	$html .= '<div id="make-pick-page-'.$eventIndex.'" class="make-pick-page text-center row">';

	// event section
	$html .= '<div class="event-section" data-event-id="'.$event->id.'">';

	$date = date_format(date_create($event->date), 'D M j');
	$html .= '<div class="event-title-holder">';
	$html .= 	'<div class="event-title">'.$event->name.'</div>';
	$html .=	'<div class="event-date">' . $date . ' @ ' . $event->simpleTime . ' PST' . '</div>';
	$html .= 	'<div class="event-location">' . $event->location. '</div>';
	$html .= '</div>';

	// get the matchup model for a given event id
	$eventId = $event->id;
	$criteria = new CDbCriteria;
	$criteria->addInCondition('eventId', array($eventId));
	$matchupModel = Matchup::model()->findAll($criteria);

	// list matchups
	$index = 1;
	foreach ($matchupModel as $matchup)
	{
		// vars
		$fighter1Id = $matchup->fighter1Id;
		$fighter2Id = $matchup->fighter2Id;
		$hasTiebreaker = $matchup->hasTiebreaker;
		$winnerId = '';

		// list the matchups
		$html .= '<div id="matchup-'.$index.'" class="matchup" data-has-tiebreaker='.$hasTiebreaker.'>';

		// fighter 1
		$criteria = new CDbCriteria;
		$criteria->addInCondition('userId', array($userId));
		$criteria->addInCondition('fighterId', array($fighter1Id));
		$criteria->addInCondition('eventId', array($eventId));
		$pickModel = Pick::model()->findAll($criteria);

		// set values if fighter 1 selected
		if($pickModel){
			foreach($pickModel as $pick){
				$winnerId = $pick->fighterId;
				$roundId = $pick->roundId;
				$finishTypeId = $pick->finishTypeId;
			}
		}

		// fighter 2
		$criteria = new CDbCriteria;
		$criteria->addInCondition('userId', array($userId));
		$criteria->addInCondition('fighterId', array($fighter2Id));
		$criteria->addInCondition('eventId', array($eventId));
		$pickModel = Pick::model()->findAll($criteria);

		// set values if fighter 2 selected
		if($pickModel){
			foreach($pickModel as $pick){
				$winnerId = $pick->fighterId;
				$roundId = $pick->roundId;
				$finishTypeId = $pick->finishTypeId;
			}
		}

		// display all of the matchups
		$html .= 	'<div class="matchup-titles">';
		if($winnerId == $matchup->fighter1->id){
			// if fighter 1 selected then highlight
			$html .= 		'<div class="fighter1 fighter-name selected" data-fighter-id="'.$fighter1Id.'">';
			$html .= 			$matchup->fighter1->firstName .' '.$matchup->fighter1->lastName;
			$html .= 		'</div>';
		} else {
			// no highlight
			$html .= 		'<div class="fighter1 fighter-name" data-fighter-id="'.$fighter1Id.'">';
			$html .= 			$matchup->fighter1->firstName .' '.$matchup->fighter1->lastName;
			$html .= 		'</div>';
		}
		$html .= 		' VS ';
		if($winnerId == $matchup->fighter2->id){
			// if fighter 2 selected then highlight
			$html .= 		'<div class="fighter2 fighter-name selected" data-fighter-id="'.$fighter2Id.'">';
			$html .= 			$matchup->fighter2->firstName .' '.$matchup->fighter2->lastName;
			$html .= 		'</div>';
		} else {
			// no highlight
			$html .= 		'<div class="fighter2 fighter-name" data-fighter-id="'.$fighter2Id.'">';
			$html .= 			$matchup->fighter2->firstName .' '.$matchup->fighter2->lastName;
			$html .= 		'</div>';
		}
		$html .= 	'</div>';

		// if there is a tiebreaker
		if($matchup->hasTiebreaker)
		{
			$html .=  '<div class="matchup-select-round-title">Select Round</div>';
			$roundModel = Round::model()->findAll();

			if(isset($roundId)){
				$selectedValueById = $roundId;
			} else {
				$selectedValueById = 1;
			}

			$html .=  CHtml::dropDownList('matchup-select-round', $selectedValueById,
				CHtml::listData($roundModel,'id', 'round'),
				array('empty'=>'Select Round', 'class' => 'matchup-select-round', 'selected'=>''));

			$html .=  '<div class="matchup-select-finish-title">Select Finish Type</div>';

			if(isset($finishTypeId)){
				$selectedValueById = $finishTypeId;
			} else {
				$selectedValueById = 1;
			}

			$finishTypeModel = FinishType::model()->findAll();

			$html .=  CHtml::dropDownList('matchup-select-finish-type', $selectedValueById,
				CHtml::listData($finishTypeModel,'id', 'finishName'),
				array('empty'=>'Select Finish Type', 'class' => 'matchup-select-finish-type'));
		}

		$html .= '</div>';

		// itterate
		$index++;
	}
	$html .= '</div>';

	// submit button
	$html .= '<div class="btn btn-primary make-pick">I PICK THESE!</div>';

	$html .= '</div>'; // end event-section
	$eventIndex++;
}

// alert
$html .= '<div id="make-pick-alert" class="alert alert-success remove"></div>';

$html .= '</div>';

echo $html;