<?php
/* @var $this PickController */
/* @var $model Pick */

$this->menu=array(
	array('label'=>'List Pick', 'url'=>array('index')),
	array('label'=>'Create Pick', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pick-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Picks</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'pick-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
//		'userId',
//		'fighterId',
//		'eventId',
//		'win',
		array(
			'header'=>'Username',
			'value'=>'$data->user->username',
			//'value'=>'$data->user->username.\' \'.$data->fighter1->lastName',
		),
		array(
			'header'=>'Fighter',
			'value'=>'$data->fighter->firstName.\' \'.$data->fighter->lastName',
		),
		array(
			'header'=>'Event Name',
			'value'=>'$data->event->name',
		),
		array(
			'header'=>'Win',
			'value'=>'$data->win',
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
