<?php
/* @var $this PickController */
/* @var $data Pick */
?>

<div class="view">

<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('id')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
<!--	<br />-->

	<b><?php echo CHtml::encode($data->user->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->user->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->fighter->getAttributeLabel('fighterId')); ?>:</b>
	<?php echo CHtml::encode($data->fighter->firstName .' '.$data->fighter->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->event->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->event->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('win')); ?>:</b>
	<?php echo CHtml::encode($data->win); ?>
	<br />

	<hr/>
</div>