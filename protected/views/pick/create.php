<?php
/* @var $this PickController */
/* @var $model Pick */

$this->breadcrumbs=array(
	'Picks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pick', 'url'=>array('index')),
	array('label'=>'Manage Pick', 'url'=>array('admin')),
);
?>

<h1>Create Pick</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>