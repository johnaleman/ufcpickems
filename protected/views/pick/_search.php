<?php
/* @var $this PickController */
/* @var $model Pick */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fighterId'); ?>
		<?php echo $form->textField($model,'fighterId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eventId'); ?>
		<?php echo $form->textField($model,'eventId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'win'); ?>
		<?php echo $form->textField($model,'win'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->