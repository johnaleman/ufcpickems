<?php
/* @var $this CarInventoryController */
/* @var $data CarInventory */
?>

<div class="view">
	<div class="inventory-item row">
		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
		<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
		<br/>

		<b><?php echo CHtml::encode($data->getAttributeLabel('make')); ?>:</b>
		<?php echo CHtml::encode($data->make); ?>
		<br/>

		<b><?php echo CHtml::encode($data->getAttributeLabel('model')); ?>:</b>
		<?php echo CHtml::encode($data->model); ?>
		<br/>

		<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
		<?php echo CHtml::encode($data->year); ?>
		<br/>

		<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
		<?php echo CHtml::encode($data->price); ?>
		<br/>

		<!--<b><?php /*echo CHtml::encode($data->getAttributeLabel('galleryId')); */?>:</b>
		<?php /*echo CHtml::encode($data->galleryId); */?>
		<br />-->

		<!--<div class="buttons">
			<div class="btn btn-primary form-submit">
				<?php /*echo CHtml::link('Create',
					array('create', 'id'=>$data['id']),
					array(
						'class'=>'tile-make pointer ',
						'rel'=>'tooltip',
						'title'=>'Create'
					)); */?>
			</div>

			<div class="btn btn-primary form-submit">
				<?php /*echo CHtml::link('View',
					array('view', 'id'=>$data['id']),
					array(
						'class'=>'tile-make pointer ',
						'rel'=>'tooltip',
						'title'=>'View'
					)); */?>
			</div>

			<div class="btn btn-primary form-submit">
				<?php /*echo CHtml::link('Update',
					array('update', 'id'=>$data['id']),
					array(
						'class'=>'tile-make pointer ',
						'rel'=>'tooltip',
						'title'=>'Update'
					)); */?>
			</div>

			<div class="btn btn-primary form-submit">
				<?php /*echo CHtml::link('Delete',
					array('delete', 'id'=>$data['id']),
					array(
						'class'=>'tile-make pointer ',
						'rel'=>'tooltip',
						'title'=>'Delete'
					)); */?>
			</div>
		</div>-->

	</div>



</div>