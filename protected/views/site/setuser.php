<?php
$userId = Yii::app()->user->getId(); // get user id
$roles = Yii::app()->user->getState("roles");

$model = User::model()->findAll();
?>

<div class="setuser page">
	<div class="user-info">
		<div class="user-id">User Id: <?php echo $userId ?></div>
		<div class="avatar-id">Avatar Id: <?php echo $userId ?></div>
	</div>

	<h1>Select the user you want to log in as.</h1>
	<!-- CModel $model, string $attribute, array $data, array $htmlOptions=array ( )) -->
	<?php echo CHtml::dropDownList('select-user', 'id', CHtml::listData($model, 'id', 'fullname')); ?>
</div>