<?php
//	$isOpenEnrollment = $model;
?>

<div id="page-home" class="home page">
	<div class="header">
		<h1>UFC Pickems!</h1>
	</div>

	<div class="content">
		<?php if ( isset($model['userId']) && $model['isOpenEnrollment'] == 1 && $model['isActiveUser'] != 1) { ?>
			<h2>Open Enrollment for Round <?php echo $model['round'] ?></h2>
			<div id="btn-enroll" class="btn-primary btn text-center">I'm In!</div>
		<?} else { ?>
			<h2>Rules</h2>
			<div class="rules">
				<p>
					Fight picks are going to be made for each UFC event. It doesn't matter if it's on Fight Pass, Fox Sports,
					Fox, or Pay Per View.
					Each UFC event is broken up into a main card and a preliminary card. The picks being made are only for
					the main card because they consist of the more well known fighters.
					The main card for each UFC event will consist of anywhere between 4-6 fights; it varies for each event. The
					UFC
					pick'em round will last for <strong>10 events</strong>. By the end of the round approximately 50 fights
					will be picked.
				</p>

				<p>
					The picks made for the main card for each event will be done through this website (www.ufcpickems.com) or
					emailed to me at joemommabee@yahoo.com if unable to be done through the site. Picks should be submitted no
					later
					than the <strong>day before the event </strong> is scheduled. As soon as everyone's picks are received, a master
					sheet and will be sent out to all members.
					In the event that there is a tie at the end of the round, tiebreakers will be taken into account.
				</p>

				<h2>Tiebreakers</h2>
				<p>
					Tiebreakers consist of choosing the outcome for the main event. For example, Jon Jones (t/ko, rd4). You will
					get <strong>one point if you choose the correct method</strong> and <strong>one point if you choose the correct round.</strong>
					There are <strong>two points total</strong> you can get for each tiebreaker. If you choose <strong>decision</strong>, you can <strong>only get one
						point</strong> if you are correct. <strong>You do not get any tiebreaker points if you don't choose the winner of the fight correctly.</strong>
					If at the the end of the round there is a tie, tiebreaker points will be counted and the person with the most points will
					win.
				</p>
			</div>

			<h2>Payout</h2>
			<div class="payout text-center">
				<p>
					The payout for each round will be as follows:
				<ul>
					<li>1st place = 50%</li>
					<li>2nd place = 30%</li>
					<li>3rd place = 20%</li>
				</ul>
				</p>
			</div>

			<h2><div id="home-contact-btn" class="contact-btn">Contact</div></h2>

			<div class="contact-holder">
				<div class="contact-info text-center">
					<p class="admin-info">
						Joe<br/>
						(530) 632-2381<br/>
						joemommabee@yahoo.com
					</p>

					<!--				<p class="website-info">-->
					<!--					Website Admin Contact Info:<br/>-->
					<!--					John at johnaleman@gmail.com-->
					<!--				</p>-->
				</div>
			</div>
		<?}?>
	</div>
</div>