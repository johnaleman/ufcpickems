<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
?>

<h1>Login</h1>

<!--<p>Please fill out the following form with your login credentials:</p>-->

<div class="form">
	<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>

<!--	<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<div class="row">
<!--		--><?php //echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email', array('placeholder'=>'Email')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
<!--		--><?php //echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password', array('placeholder'=>'Password')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Login', array('class'=>'login-btn btn btn-primary btn-success')); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row">
		<div id="forgot-pass-btn" class="forgot-pass-btn btn btn-primary btn-danger">Forgot Password</div>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
