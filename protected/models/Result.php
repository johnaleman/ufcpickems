<?php

/**
 * This is the model class for table "Result".
 *
 * The followings are the available columns in table 'Result':
 * @property integer $id
 * @property integer $fighterId
 * @property integer $eventId
 * @property integer $finishTypeId
 * @property integer $roundId
 *
 * The followings are the available model relations:
 * @property Event $event
 * @property Fighter $fighter
 * @property FinishType $finishType
 * @property Round $round
 */
class Result extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Result the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Result';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fighterId, eventId', 'required'),
			array('fighterId, eventId, finishTypeId, roundId', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fighterId, eventId, finishTypeId, roundId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'event' => array(self::BELONGS_TO, 'Event', 'eventId'),
			'fighter' => array(self::BELONGS_TO, 'Fighter', 'fighterId'),
			'finishType' => array(self::BELONGS_TO, 'FinishType', 'finishTypeId'),
			'round' => array(self::BELONGS_TO, 'Round', 'roundId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fighterId' => 'Fighter',
			'eventId' => 'Event',
			'finishTypeId' => 'Finish Type',
			'roundId' => 'Round',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fighterId',$this->fighterId);
		$criteria->compare('eventId',$this->eventId);
		$criteria->compare('finishTypeId',$this->finishTypeId);
		$criteria->compare('roundId',$this->roundId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}