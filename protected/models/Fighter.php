<?php

/**
 * This is the model class for table "Fighter".
 *
 * The followings are the available columns in table 'Fighter':
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 *
 * The followings are the available model relations:
 * @property Matchup[] $matchups
 * @property Matchup[] $matchups1
 * @property Pick[] $picks
 * @property Result[] $results
 */
class Fighter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Fighter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fighter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstName, lastName', 'required'),
			array('firstName, lastName', 'length', 'max'=>255),
			array('firstName', 'unique', 'on'=>'create'),
			//array('firstName, lastName', 'uniqueFirstLast', 'message'=>'custom'),
			//array('lastName', 'unique', 'on'=>'create'),
			//array('lastName', 'unique', 'message'=>'Last name already exists'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, firstName, lastName', 'safe', 'on'=>'search'),
		);
	}

	public function uniqueFirstLast($attribute, $params)
	{
		// Set $emailExist variable true or false by using your custom query on checking in database table if email exist or not.
		// You can user $this->{$attribute} to get attribute value.

		$firstLastExist = true;

		print_r($params);

//		$fighter = Fighter::model()->find(array(
//			'condition'=>'firstName=:firstName AND lastName=:lastName',
//			'params'=>array(
//				':firstName'=>$firstName,
//				':isDeleted'=>0
//			)
//		));


		if($firstLastExist)
			$this->addError('firstName','Fighter already exists... '.$this->{$attribute}.' ');
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'matchups' => array(self::HAS_MANY, 'Matchup', 'fighter2Id'),
			'matchups1' => array(self::HAS_MANY, 'Matchup', 'fighter1Id'),
			'picks' => array(self::HAS_MANY, 'Pick', 'fighterId'),
			'results' => array(self::HAS_MANY, 'Result', 'fighterId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstName' => 'First Name',
			'lastName' => 'Last Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('lastName',$this->lastName,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}