<?php

/**
 * This is the model class for table "Pick".
 *
 * The followings are the available columns in table 'Pick':
 * @property integer $id
 * @property integer $userId
 * @property integer $fighterId
 * @property integer $eventId
 * @property integer $win
 * @property integer $finishTypeId
 * @property integer $roundId
 * @property integer $tiebreakerWin
 *
 * The followings are the available model relations:
 * @property Event $event
 * @property Fighter $fighter
 * @property FinishType $finishType
 * @property Round $round
 * @property User $user
 */
class Pick extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pick the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Pick';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, eventId', 'required'),
			array('userId, fighterId, eventId, win, finishTypeId, roundId, tiebreakerWin', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userId, fighterId, eventId, win, finishTypeId, roundId, tiebreakerWin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'event' => array(self::BELONGS_TO, 'Event', 'eventId'),
			'fighter' => array(self::BELONGS_TO, 'Fighter', 'fighterId'),
			'finishType' => array(self::BELONGS_TO, 'FinishType', 'finishTypeId'),
			'round' => array(self::BELONGS_TO, 'Round', 'roundId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'fighterId' => 'Fighter',
			'eventId' => 'Event',
			'win' => 'Win',
			'finishTypeId' => 'Finish Type',
			'roundId' => 'Round',
			'tiebreakerWin' => 'Tiebreaker Win',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('fighterId',$this->fighterId);
		$criteria->compare('eventId',$this->eventId);
		$criteria->compare('win',$this->win);
		$criteria->compare('finishTypeId',$this->finishTypeId);
		$criteria->compare('roundId',$this->roundId);
		$criteria->compare('tiebreakerWin',$this->tiebreakerWin);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}