<?php

/**
 * This is the model class for table "Matchup".
 *
 * The followings are the available columns in table 'Matchup':
 * @property integer $id
 * @property integer $fighter1Id
 * @property integer $fighter2Id
 * @property integer $eventId
 * @property integer $hasTiebreaker
 *
 * The followings are the available model relations:
 * @property Fighter $fighter2
 * @property Event $event
 * @property Fighter $fighter1
 */
class Matchup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Matchup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Matchup';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fighter1Id, fighter2Id, eventId', 'required'),
			array('fighter1Id, fighter2Id, eventId, hasTiebreaker', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fighter1Id, fighter2Id, eventId, hasTiebreaker', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fighter2' => array(self::BELONGS_TO, 'Fighter', 'fighter2Id'),
			'event' => array(self::BELONGS_TO, 'Event', 'eventId'),
			'fighter1' => array(self::BELONGS_TO, 'Fighter', 'fighter1Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fighter1Id' => 'Fighter1',
			'fighter2Id' => 'Fighter2',
			'eventId' => 'Event',
			'hasTiebreaker' => 'Has Tiebreaker',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fighter1Id',$this->fighter1Id);
		$criteria->compare('fighter2Id',$this->fighter2Id);
		$criteria->compare('eventId',$this->eventId);
		$criteria->compare('hasTiebreaker',$this->hasTiebreaker);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}