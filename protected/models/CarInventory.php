<?php

/**
 * This is the model class for table "CarInventory".
 *
 * The followings are the available columns in table 'CarInventory':
 * @property string $id
 * @property string $make
 * @property string $model
 * @property string $year
 * @property string $vin
 * @property string $stockId
 * @property string $color
 * @property string $interior
 * @property string $transmission
 * @property string $miles
 * @property string $price
 * @property string $mainImage
 *
 * The followings are the available model relations:
 * @property Gallery[] $galleries
 */
class CarInventory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CarInventory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CarInventory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('make, model, year', 'required'),
			array('make, model, color, interior, transmission, mainImage', 'length', 'max'=>255),
			array('year, price', 'length', 'max'=>10),
			array('vin', 'length', 'max'=>17),
			array('stockId', 'length', 'max'=>5),
			array('miles', 'length', 'max'=>6),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, make, model, year, vin, stockId, color, interior, transmission, miles, price, mainImage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'galleries' => array(self::HAS_MANY, 'Gallery', 'carInventoryId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'make' => 'Make',
			'model' => 'Model',
			'year' => 'Year',
			'vin' => 'Vin',
			'stockId' => 'Stock',
			'color' => 'Color',
			'interior' => 'Interior',
			'transmission' => 'Transmission',
			'miles' => 'Miles',
			'price' => 'Price',
			'mainImage' => 'Main Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('make',$this->make,true);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('vin',$this->vin,true);
		$criteria->compare('stockId',$this->stockId,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('interior',$this->interior,true);
		$criteria->compare('transmission',$this->transmission,true);
		$criteria->compare('miles',$this->miles,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('mainImage',$this->mainImage,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}