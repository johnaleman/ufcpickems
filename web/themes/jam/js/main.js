app = {
    isProduction: false,
    DIRPATH: '/sandbox/pickems/web/',
    DEVPATH: '/sandbox/pickems/dev/web/',
    ROOTPATH: '',

    // vars
    eventId: null,
    fighterListArr: [],
    isEventCreated: false,
    isFightersCreated: false,
    followersData: {},

    /**** initialize ****/
    /*
    	 *
    	 */
    init: function() {
        var self = this;

        this.updateDirPath();
        this.addFormaterToDate();
        this.addListeners();

        if (this.isResultsPage()) {
            this.loadFollowers(function() {
                self.populateEventSections();
            });
        }
    },

    loadFollowers: function(callback) {
        var self = this;
        this.fetchFollowers(function(data) {
            self.followersData = data;

            if (callback) {
                callback();
            }
        });
    },

    populateEventSections: function() {
        var self = this,
        $eventSections = $('.event-section'),
        html = '';

        // loop through all event sections
        $eventSections.each(function(index, eventSection) {
            var $eventSection = $(eventSection),
            $matchupContainer = $eventSection.find('.matchup-container'),
            eventId = $(eventSection).attr('data-event-id');

            //helpers.log('fetch eventId('+eventId+')');
            self.fetchResultsByEventId(eventId, function(data) {
                // success
                //helpers.log('success data('+data.eventId+'): ' + data);

                var eventStatus = data.eventStatus;

                // loop through each matchup
                html = ''; // clear
                $(data.matchups).each(function(index, matchup) {
                    // vars
                    var fighter1 = matchup.fighter1;
                    var fighter2 = matchup.fighter2;
                    var winnerFinishName = matchup.winnerFinishName;
                    var winnerRoundId = matchup.winnerRoundId;
                    var hasTiebreaker = matchup.hasTiebreaker;
                    var picks = matchup.picks;
                    var hasWinner = false;
                    var followBtnClass = '';

                    // matchup
                    html += '<div class="matchup">';

                    // fighter 1
                    if (matchup.winnerId == fighter1.id) {
                        hasWinner = true;

                        // highlight the winner
                        html += '<div class="matchup-titles">';
                        html += 	'<div class="fighter1 fighter-name winner">' + fighter1.firstName + ' ' + fighter1.lastName + '</div>';
                        //html += '</div>';
                    } else {
                        html += '<div class="matchup-titles">';
                        html += 	'<div class="fighter1 fighter-name">' + fighter1.firstName + ' ' + fighter1.lastName + '</div>';
                        //html += '</div>';
                    }

                    // vs
                    html += ' VS ';

                    // fighter 2
                    if (matchup.winnerId == fighter2.id) {
                        hasWinner = true;

                        // highlight the winner
                        //html += '<div class="matchup-titles">';
                        html += 	'<div class="fighter2 fighter-name winner">' + fighter2.firstName + ' ' + fighter2.lastName + '</div>';
                        html += '</div>';
                    } else {
                        //html += '<div class="matchup-titles">';
                        html += 	'<div class="fighter2 fighter-name">' + fighter2.firstName + ' ' + fighter2.lastName + '</div>';
                        html += '</div>';
                    }
                    html += '</div>'; // end matchup

                    // event is closed - display eveyone's picks
                    if (eventStatus != 1) {
                        // if winner
                        if (winnerFinishName && winnerRoundId) {
                            // result
                            html += '<div class="matchup-result">';
                            html += 'Round: ' + winnerRoundId + ' - ' + winnerFinishName;
                            html += '</div>';
                        }

                        // actions
                        html += '<div class="matchup-result-actions">';
                        html += 	'<div class="action action-all btn btn-primary" data-action="all">View All</div>';
                        html += 	'<div class="action action-following btn btn-primary" data-action="following">View Following</div>';
                        html += 	'<div class="action action-hide-all btn btn-primary" data-action="hide">Hide All</div>';
                        //html += 	'<div class="action action-followers btn btn-primary" data-action="followers">Followers</div>';
                        html += '</div>';

                        // picks
                        html += '<div class="table-view">';

                        // all view
                        html += '<div class="view-section" data-view="all">';
                        $(picks).each(function(index, pick) {
                            followBtnClass = '';

                            html += '<div class="user-pick">';
                            html += 	'<div class="user-name-holder" data-user-id="' + pick.userId + '">';

                            // do not show follow button if self
                            if (self.getMyUserId() == pick.userId) {
                                followBtnClass = 'opacity-hide';
                            }

                            // follow btn
                            if (self.isFollowingUser(pick.userId)) {
                                html += 	'<div class="user-follow-btn btn btn-success' + followBtnClass + '" data-follow-action="unfollow">Following</div>';
                            } else {
                                html += 	'<div class="user-follow-btn btn btn-primary ' + followBtnClass + '" data-follow-action="follow">Follow</div>';
                            }

                            html += 	'<div class="user-name">' + pick.firstName + ' ' + pick.lastInitial + '</div>';
                            html += 	'<div class="user-selected-fighter">' + pick.selectedFighter + '</div>';
                            html += 	'<div class="user-win-col">Point: ' + pick.win + '</div>';
                            if (hasTiebreaker == '1') {
                                html += 	'<div class="user-tb-round">T. Round: ' + pick.selectedRoundName + '</div>';
                                html += 	'<div class="user-tb-finishtype">T. Method: ' + pick.selectedFinishTypeName + '</div>';
                                html += 	'<div class="user-tb-points">T. Points(s): ' + pick.totalTbp + '</div>';
                            }
                            html += '</div>';

                            html += '</div>'; // end user-pick
                        });

                        html += '</div>'; // end view-all
                        html += '</div>'; // close table-view
                    }
                });

                $matchupContainer.append(html);

                // view complete
                self.addResultActionClick();
                self.addFollowUserClick();
            });
        })
    },

    addFollowUserClick: function() {
        var self = this;
        var $userFollowBtn = $('.user-follow-btn');

        $userFollowBtn.unbind('click');
        $userFollowBtn.click(function(event) {
            var $target = $(event.target);
            var followAction = $target.attr('data-follow-action');
            var userId = $target.closest('.user-name-holder').attr('data-user-id');
            var $userNameHolder = '';

            // do follow
            self.followUserById(userId, followAction, function(data) {
                // success
                $userNameHolder = $('.user-name-holder[data-user-id="' + userId + '"]');

                if (followAction == 'follow') {
                    $userNameHolder.find('.user-follow-btn').removeClass('btn-primary');
                    $userNameHolder.find('.user-follow-btn').html('Following');
                    $userNameHolder.find('.user-follow-btn').addClass('btn-success');
                    $userNameHolder.find('.user-follow-btn').attr('data-follow-action', 'unfollow');
                } else {
                    // unfollow
                    $userNameHolder.find('.user-follow-btn').removeClass('btn-success');
                    $userNameHolder.find('.user-follow-btn').html('Follow');
                    $userNameHolder.find('.user-follow-btn').addClass('btn-primary');
                    $userNameHolder.find('.user-follow-btn').attr('data-follow-action', 'follow');
                }

                // update data
                self.loadFollowers();
            });
        })
    },

    followUserById: function(followUserId, followAction, callback) {
        var datastring = '';
        var url = '';
        var userId = this.getMyUserId();
        var followUserId = followUserId;

        if (followAction == 'follow') {
            url = app.ROOTPATH + 'following/followuser';
        } else {
            // unfollow
            url = app.ROOTPATH + 'following/unfollowuser';
        }

        datastring +='&userId=' + userId;
        datastring +='&followUserId=' + followUserId;

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: datastring,
            success: function(response, status) {
                if (response.state) {
                    callback(response.data);
                } else {
                    helpers.log('-Response: ' + response.msg);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                helpers.log('AJAX error:' + textStatus);
            }
        });
    },

    fetchResultsByEventId: function(eventId, callback) {
        var datastring = '';
        var url = app.ROOTPATH + 'matchup/getall';
        var data,
        fighter1,
        fighter2;

        datastring +='&eventId=' + eventId + '';

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: datastring,
            success: function(response, status) {
                if (response.state) {
                    callback(response.data);
                } else {
                    helpers.log('-Response: ' + response.msg);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                helpers.log('AJAX error:' + textStatus);
            }
        });
    },

    isResultsPage: function() {
        var pathName = document.location.pathname,
        //get rid of the trailing / before doing a simple split on /
        url_parts = pathName.replace(/\/\s*$/,'').split('/').reverse(), // reverse results
        controller = url_parts[1],
        action = url_parts[0];

        return (controller == 'result' && action == 'index');
    },

    updateDirPath:function() {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }

        var origin = window.location.origin;
        var pathName = document.location.pathname;
        var fullUri = origin + pathName;
        var dirPath = helpers.getStringBetweenText(fullUri, origin, '/web/');
        dirPath = dirPath + '/web/';
        app.DIRPATH = dirPath;

        if (origin.indexOf("ufcpickems.com") > -1) {
            app.DIRPATH = app.DIRPATH.replace('/web/', '/');
        }

        if (app.isProduction) {
            app.ROOTPATH = app.DIRPATH;
        } else {
            app.ROOTPATH = app.DEVPATH;
        }

        //helpers.log('app.DIRPATH: ' + app.DIRPATH);
    },

    addFormaterToDate:function() {
        Date.prototype.yyyymmdd = function() {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd  = this.getDate().toString();
            return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]); // padding
        };
    },

    /**** listeners ****/
    /*
    	 *
    	 */
    addListeners:function() {
        this.addNavClickTracking();
        this.addAdminListeners();
        this.addResultTitleClick();
        this.addFighterSelectClick();
        this.addMakePickSubmit();
        this.addFinishTypeChange();
        this.addForgotPassClick();
        this.addContactClick();
        this.addCreateEventClick();
        this.addEnrollBtnClick();

        //this.addCreateEventCompleteListener();
    },

    addResultActionClick:function() {
        var self = this;
        var $actions = $('.matchup-result-actions .action');
        var userId = $('#page').attr('data-user-id');

        $actions.unbind('click');
        $actions.click(function(event) {
            var $target = $(event.target);
            var $actions = $target.parent(); // .matchup-result-actions
            var actionType = $target.attr('data-action');
            var $eventSection = $target.closest('.event-section');
            var $tableView = $target.parent().next(); // .user-picks

            // hide all views
            $tableView.find('.view-section').removeClass('active');

            // set active table view
            var $targetTableView = $tableView.find(".view-section[data-view='" + actionType + "']");
            $targetTableView.addClass('active');
            $tableView.find('.user-pick .user-name-holder').show();

            if (actionType == 'following') {
                var followersData = self.getFollowers();

                actionType = 'all';
                $targetTableView = $tableView.find(".view-section[data-view='" + actionType + "']");
                $targetTableView.addClass('active');

                $tableView.find('.user-pick .user-name-holder').hide(); // hide all

                // display following
                $tableView.find('.user-pick .user-name-holder[data-user-id="' + userId + '"]').show(); // add self

                // loop through following
                $(followersData.following).each(function(index, user) {
                    $tableView.find('.user-pick .user-name-holder[data-user-id="' + user.userId + '"]').show();
                });
            }
        })
    },

    fetchFollowers: function(callback) {
        var userId = $('#page').attr('data-user-id');

        var datastring = '';
        var url = app.ROOTPATH + 'following/getall';
        datastring +='&userId=' + userId + '';

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: datastring,
            success: function(response, status) {
                if (response.state) {
                    callback(response.data);
                } else {
                    helpers.log('-Response: ' + response.msg);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                helpers.log('AJAX error:' + textStatus);
            }
        });
    },

    isFollowingUser: function(userId) {
        //helpers.log('isFollowingUser('+userId+')');
        var followersData = this.getFollowers();
        var isFollowing = false;

        $(followersData.following).each(function(index, followerObj) {
            var followingUserId = followerObj.userId;

            if (followingUserId == userId) {
                isFollowing = true;
                return true;
            }
        });

        return isFollowing;
    },

    displayPartialAll: function($targetDiv) {
        helpers.log('displayPartialAll');
        if ($targetDiv) {
            $targetDiv.removeClass('hide');
        }
    },

    hidePartialAll: function($targetDiv) {
        helpers.log('hidePartialAll');
        if ($targetDiv) {
            $targetDiv.addClass('hide');
        }
    },

    getPartialFollowingHtml: function() {
        helpers.log('getPartialFollowingHtml');
        var html = '' +
        '<div>{FOLLOWING VIEW}</div>';



        return html;
    },

    addResultTitleClick:function() {
        var $resultEventTitleHolder = $('#results-page .event-title-holder');
        $resultEventTitleHolder.click(function(event) {
            var $target = $(event.target);
            var $eventSection = $($target).closest('.event-section');
            var $matchupContainer = $eventSection.find('.matchup-container');
            $matchupContainer.fadeToggle();
        })
    },
    /*
    	 *
    	 */
    addNavClickTracking:function() {
        $('#main-nav li').click(function(event) {
            var opt_label = '';
            var $target = $(event.target);
            var href = '';

            try {
                opt_label = $($target).find('a').html();
                href = $($target).find('a').attr('href');
            } catch (error) {
                opt_label = '';
            }

            // track
            helpers.trackClick(opt_label);
            document.location.href = href; //relative to domain
        })
    },
    /*
    	 *
    	 */
    addAdminListeners:function() {
        var $admin = $('.admin');
        var $adminOptions = $('.admin-options');
        $admin.click(function() {
            $adminOptions.fadeToggle();
        })
    },

    /*
    	 *
    	 */
    addFighterSelectClick:function() {
        $('.make-pick-page .fighter-name').click(function(event) {
            var $target = $(event.target);
            var $matchup = $target.closest('.matchup');
            app.clearSelectedFighterByMatchup($matchup);
            app.markSelectedFighter($target);
        });
    },

    /*
    	 *
    	 */
    addMakePickSubmit:function() {
        var $makePick = $('#make-pick');
        $makePick.click(function(event) {
            event.preventDefault();
            app.submitUserPicks();
        })
    },

    /*
    	 *
    	 */
    addFinishTypeChange:function() {
        var $matchSelectFinishType = $('#matchup-select-finish-type');

        $matchSelectFinishType.on('change', function() {
            var value = $(this).val();
            helpers.log('value: ' + value);
            if (value == 3) {
                app.setRoundTo5();
                app.disableRoundSelect();
            } else {
                app.enableRoundSelect();
            }
        });
    },

    addForgotPassClick:function() {
        var self = this;
        var msg = '';
        var $forgotPassBtn = $('#forgot-pass-btn');
        $forgotPassBtn.click(function(event) {
            var email = $('#LoginForm_email').val();
            var datastring = '';
            datastring += '&email=' + email;

            $.ajax({
                url: app.DIRPATH + 'site/forgotPassword',
                dataType: 'json',
                type: 'POST',
                data: datastring,
                success: function(response, status) {
                    helpers.log('AJAX success: ' + response);
                    if (response.state) {
                        helpers.log('+Response: ' + response.msg);
                        window.debugg_response = response;
                        self.displaySuccessMessage(response.msg);
                    } else {
                        helpers.log('-Response: ' + response.msg);
                        self.displayErrorMessage(response.msg);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    helpers.log('AJAX error:' + textStatus);
                    self.displaySuccessMessage('ERROR: ' + textStatus);
                }
            });
        })
    },

    addContactClick: function() {
        $('#home-contact-btn').click(function(event) {
            $('#page-home .contact-holder').toggleClass('hide');
        })
    },

    addCreateEventClick:function() {
        var self = this;
        var $submitBtn = $('#submit-create-fighter-btn');
        var wikiLink = '';

        $submitBtn.click(function(event) {
            event.preventDefault();
            wikiLink = $('#input-wiki-link').val();
            self.autoCreateFighters(wikiLink);
        })
    },

    setRoundTo5:function() {
        var $matchupSelectRound = $('#matchup-select-round');
        $matchupSelectRound[0].selectedIndex = 5;
    },

    disableRoundSelect:function() {
        $('#matchup-select-round').attr('disabled', true);
    },
    enableRoundSelect:function() {
        $('#matchup-select-round').attr('disabled', false);
    },

    /**** methods ****/
    // http://en.wikipedia.org/wiki/List_of_UFC_events#Scheduled_events
    autoCreateFighters: function(url) {
        var self = this;
        var url = url || '';

        if (url == '') {
            helpers.log('ERROR: No url value');
            return;
        }

        // use ajaxPrefilter + heroku cors-anywhere
        $.ajaxPrefilter(function(options) {
            if (options.crossDomain && jQuery.support.cors) {
                options.url = 'https://cors-anywhere.herokuapp.com/' + options.url;
            }
        });

        // do cors ajax request
        $.ajax({
            url: url,
            dataType: 'html',
            type: 'POST',
            success: function (response, status) {
                //helpers.log('AJAX success: ' + response);
                // clear
                self.resetCreationLogicValues();
                self.clearOutput();

                // create
                self.createEvent(response);
                self.createFightersList(response);
                self.createFighters(response, self.getFightersListArr());

                // Setup on complete listeners
                self.addCreateEventCompleteListener();
                self.addCreateFightersCompleteListener();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                helpers.log('AJAX error:' + textStatus);
            }
        });
    },

    resetCreationLogicValues:function() {
        app.eventId = null;
        app.isEventCreated = false;
        app.isFightersCreated = false;
    },

    addCreateEventCompleteListener:function() {
        var self = this;
        $(document).unbind('create_event_complete');
        $(document).bind('create_event_complete', function(event, eventId) {
            // handle
            helpers.log('create event complete...' + eventId);
            app.eventId = eventId;
            app.isEventCreated = true;
            self.doCreateMatchups(eventId, self.getFightersListArr());
        })
    },

    addCreateFightersCompleteListener:function() {
        var self = this;
        $(document).unbind('create_fighters_complete');
        $(document).bind('create_fighters_complete', function(event, html) {
            // handle
            helpers.log('create fighters complete...');
            app.isFightersCreated = true;

            // FIXME: The matchup order is not accurate.
            //self.doCreateMatchups(self.getEventId(), self.getFightersListArr());
        })
    },

    // TODO: createMatchups()
    doCreateMatchups:function(eventId, fightersListArr) {
        this.clearOutput();

        if (app.isEventCreated && app.isFightersCreated) {
            var fightersListArr = fightersListArr || [];
            var $createFighterOutput = $('#create-fighter-output');
            var htmlMsg = '';

            var fighter1Name = '';
            var fighter2Name = '';
            var hasTiebreaker = false;
            var fighter1Id = '';
            var fighter2Id = '';
            var numNewMatchup = 0;
            $.each(fightersListArr, function(index, fighterO) {
                // return if fighters are not on the main card
                fighterO.cardType = fighterO.cardType.toLowerCase();
                if (fighterO.cardType.indexOf('main card') != 0) { return;}
                index = parseInt(index) + parseInt(1); // start at 1

                if (index % 2) {
                    fighter1Name = '';
                    fighter2Name = '';
                    fighter1Id = '';
                    fighter2Id = '';
                    hasTiebreaker = false;
                }

                if (index % 2) {
                    fighter1Name = fighterO.firstName + ' ' + fighterO.lastName;
                    fighter1Id = fighterO.fighterId;
                } else {
                    fighter2Name = fighterO.firstName + ' ' + fighterO.lastName;
                    fighter2Id = fighterO.fighterId;
                }

                if (fighter1Name != '' && fighter2Name != '') {
                    helpers.log(fighter1Name + ' vs ' + fighter2Name);
                    if (index == 2) {hasTiebreaker = true;} // first set always has a tiebreaker

                    var datastring = '';
                    datastring += '&eventId=' + eventId;
                    datastring += '&fighter1Id=' + fighter1Id;
                    datastring += '&fighter2Id=' + fighter2Id;
                    datastring += '&hasTiebreaker=' + hasTiebreaker;

                    $.ajax({
                        url: app.ROOTPATH + 'app/creatematchup',
                        dataType: 'json',
                        type: 'POST',
                        data: datastring,
                        success: function(response, status) {
                            htmlMsg = '';
                            //helpers.log('AJAX success: '+response);
                            if (response.state) {
                                //helpers.log('+Response: '+response.msg);
                                numNewMatchup++;
                                appendToMsg(response.msg);
                            } else {
                                //helpers.log('-Response: '+response.msg);
                                appendToMsg(response.msg);
                            }

                            // message
                            appendToMsg('Matchups added: ' + numNewMatchup);

                            // append
                            $createFighterOutput.append(htmlMsg);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            helpers.log('AJAX error:' + textStatus);
                        }
                    });


                }
            });

            //function isOdd(index) { return index % 2;}

            function appendToMsg(msg) {
                htmlMsg += msg + '<br/>';
            }
        } else {
            //helpers.log('WARNING: All required events have not completed.');
            return;
        }
    },

    clearOutput:function() {
        $('#create-fighter-output').html('');
    },

    createEvent: function(html) {
        var o = {}; // will store the values
        var eventName = '';
        var htmlMsg = '';
        var $createFighterOutput = $('#create-fighter-output');
        var $infoRows = $(html).find('.infobox th');

        $.each($infoRows, function(index, element) {
            var $element = $(element);
            var key = helpers.stripHtml($element.html());
            var $valuesDiv = $element.siblings('td');
            $valuesDiv.find('span').remove(); // remove spans
            var value = $valuesDiv.html();
            //helpers.log(key + ' - ' + value);

            // set event name - always the first child
            if (index == 0) {
                key = key.replace('vs.', 'vs');
                key = key.replace('UFC Fight Night', 'UFN');
                o.eventName = key;
            }

            // set event date
            if (key == 'Date') {
                o.eventDate = value.replace(/&nbsp;/g," "); // do not use stripHtml - causes bugs
                o.eventDateFormatted = new Date(o.eventDate).yyyymmdd();
            }

            // set location
            if (key == 'City') {
                o.eventLocation = helpers.stripHtml(value);
            }
        });

        // message
        appendToMsg('Event Name: ' + o.eventName);
        appendToMsg('Date: ' + o.eventDate);
        appendToMsg('Location: ' + o.eventLocation);

        // clear
        //$createFighterOutput.html('');

        var numNewEvent = 0;
        var total = 1;

        var datastring = '';
        datastring += '&eventName=' + o.eventName;
        datastring += '&eventDate=' + o.eventDateFormatted;
        datastring += '&eventLocation=' + o.eventLocation;

        $.ajax({
            url: app.ROOTPATH + 'app/createevent',
            dataType: 'json',
            type: 'POST',
            data: datastring,
            success: function(response, status) {
                $.event.trigger('create_event_complete', [response.data.eventId]);
                htmlMsg = '';

                //helpers.log('AJAX success: '+response);
                if (response.state) {
                    //helpers.log('+Response: '+response.msg);
                    numNewEvent++;
                    appendToMsg(response.msg);
                } else {
                    //helpers.log('-Response: '+response.msg);
                    appendToMsg(response.msg);
                }

                // message
                appendToMsg('Event added: ' + numNewEvent + ' / ' + total);

                // append
                $createFighterOutput.append(htmlMsg);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                helpers.log('AJAX error:' + textStatus);
            }
        });

        function appendToMsg(msg) {
            htmlMsg += msg + '<br/>';
        }
    },

    // private functions
    // returns array of fighters
    // @html - the response from the external site
    createFightersList:function(html) {
        var fightersArr = [];
        var o = {};
        var $targetTable = $(html).find('.toccolours');
        var firstName = '';
        var lastName = '';
        var splitArr = [];

        // check if class for fighter table is found
        if ($targetTable.length > 0) {
            var qResults = $targetTable.find('tr td a'); // fighter names are stored in the anchor tags
        } else {
            helpers.log('ERROR: no table found via class .toccolours');
            return;
        }

        // store fighter name as o.firstName, o.lastName
        $.each(qResults, function(index, element) {
            // TODO: use element.textContent = 'Anthony Johnson' (this will remove replace common string gotchas.)
            var $element = $(element);
            var fighterName = $element.attr('href');
            var cartType = $element.parent().parent().prevUntil().find('th[colspan="8"]').first().find('b').text();

            if (typeof fighterName !== 'undefined') {
                o = {}; // reset

                // strip /wiki/
                fighterName = fighterName.replace('/wiki/', '');
                splitArr = fighterName.split('_');
                firstName = splitArr[0];
                lastName = splitArr[1];

                if (typeof lastName === 'undefined') {
                    helpers.log('ERROR: No last name found.');
                    return;
                }

                // some names might contain a third section (ex: Junior dos Santos)
                try {
                    if (typeof splitArr[2] !== 'undefined') {
                        if (splitArr[2] != '(fighter)') {
                            lastName += ' ' + splitArr[2];
                        }
                    }
                } catch (error) {
                    helpers.log('ERROR: ' + error);
                }

                // replace common string gotchas.
                var find = '(fighter)';
                var re = new RegExp(find, 'g');
                lastName = lastName.replace(re, ''); // results might contain (fighter) - remove

                var find = '_(fighter)';
                var re = new RegExp(find, 'g');
                lastName = lastName.replace(re, ''); // results might contain _(fighter) - remove

                var find = 'ę';
                var re = new RegExp(find, 'g');
                lastName = lastName.replace(re, 'e');

                // a href might return '#cite' - skip this
                if (firstName == '#cite') {
                    //helpers.log('WARNING: Found a cite link. Skip.');
                    return;
                }

                // set object
                o.firstName = firstName;
                o.lastName = lastName;
                o.cardType = cartType;

                // populate array
                fightersArr.push(o);
            }
        });

        // set
        app.fighterListArr = fightersArr;
        //return fightersArr;
    },

    getFightersListArr:function() {
        return app.fighterListArr;
    },

    getEventId:function() {
        return app.eventId;
    },

    createFighters:function(html, fightersArr) {
        var fightersArr = fightersArr;
        var i = 0;
        var total = fightersArr.length;
        var numNewFighters = 0;
        var numFail = 0;

        var $createFighterOutput = $('#create-fighter-output');
        var htmlMsg = '';

        // clear
        //$createFighterOutput.html('');

        // auto create
        $.each(fightersArr, function(index, o) {
            // vars
            var datastring = '';
            datastring += '&firstName=' + o.firstName;
            datastring += '&lastName=' + o.lastName;

            $.ajax({
                url: app.ROOTPATH + 'app/createfighter',
                dataType: 'json',
                type: 'POST',
                data: datastring,
                success: function(response, status) {
                    i++; // iterate
                    if (response.data.fighterId) {o.fighterId = response.data.fighterId;} // set fighterId
                    htmlMsg = '';

                    if (response.state) {
                        numNewFighters++;
                        appendToMsg(response.msg);
                    } else {
                        appendToMsg(response.msg);
                    }

                    appendToMsg('Fighters added: ' + numNewFighters + ' / ' + total); // message
                    $createFighterOutput.append(htmlMsg); // append
                    if (i == total) {$.event.trigger('create_fighters_complete', [html]);} // fire event
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    helpers.log('AJAX error:' + textStatus);
                }
            });
        });

        function appendToMsg(msg) {
            htmlMsg += msg + '<br/>';
        }
    },

    /*
    	 *
    	 */
    clearSelectedFighterByMatchup:function(matchupDiv) {
        var $matchupDiv = $(matchupDiv);
        $matchupDiv.find('.fighter-name').removeClass('selected')
    },
    markSelectedFighter:function(fighterDiv) {
        var $fighterDiv = $(fighterDiv);
        $fighterDiv.addClass('selected');
    },

    addEnrollBtnClick:function() {
        var self = this;
        var $enrollBtn = $('#btn-enroll');

        $enrollBtn.click(function(event) {
            var datastring = '';
            $.ajax({
                url: app.DIRPATH + 'app/enrolluser',
                dataType: 'json',
                type: 'POST',
                data: datastring,
                success: function(response, status) {
                    helpers.log('AJAX success: ' + response);
                    if (response.state) {
                        helpers.log('+Response: ' + response.msg);
                        self.redirectHome();
                    } else {
                        helpers.log('-Response: ' + response.msg);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    helpers.log('AJAX error:' + textStatus);
                },
                complete:function() {}
            });
        })
    },

    redirectHome:function() {
        window.location.href = app.DIRPATH;
    },

    submitUserPicks:function() {
        var $makePickPage = $('.make-pick-page');

        // validation
        if (!this.validateAllFightsSelected()) {return;}

        // get each pick
        $makePickPage.each(function(index, element) {
            var $element = $(element);
            var $fighterNames = $element.find('.fighter-name');
            var $matchups = $element.find('.matchup');
            var eventId = $element.find('.event-section').attr('data-event-id');
            var selectedReq = $fighterNames.length * .5;
            var numSelected = 0;
            var selectedFighters = '';
            var tiebreakers = '';

            $fighterNames.each(function(index, element) {
                var $element = $(element);
                if ($element.hasClass('selected')) {
                    // check for round and finish type
                    var $matchup = $($element).closest('.matchup');
                    var fighterId = $element.attr('data-fighter-id');
                    selectedFighters += fighterId + '_';
                    numSelected++;
                }
            });

            $matchups.each(function(index, element) {
                var $matchup = $(element);
                var hasTiebreaker = $matchup.attr('data-has-tiebreaker');
                if (hasTiebreaker == '1') {
                    // has tiebreaker...
                    var fighterId = $matchup.find('.fighter-name.selected').attr('data-fighter-id');
                    var roundId = $matchup.find('#matchup-select-round :selected').attr('value');
                    var finishTypeId = $matchup.find('#matchup-select-finish-type :selected').attr('value');
                    tiebreakers += fighterId + '_' + roundId + '_' + finishTypeId + '__';
                }
            })

            // replace last instance of character
            var replacement = '';
            selectedFighters = selectedFighters.replace(/_([^_]*)$/,replacement + '$1');

            // replace last instance of character
            var replacement = '';
            tiebreakers = tiebreakers.replace(/__([^_]*)$/,replacement + '$1');

            var datastring = '';
            datastring += '&userId=0';
            datastring += '&eventId=' + eventId;
            datastring += '&selectedFighters=' + selectedFighters;

            if (tiebreakers != '') {
                datastring += '&tiebreakers=' + tiebreakers;
            }

            // submit picks
            $.ajax({
                url: app.DIRPATH + 'pick/submituserpicks',
                dataType: 'json',
                type: 'POST',
                data: datastring,
                success: function(response, status) {
                    helpers.log('AJAX success: ' + response);
                    if (response.state) {
                        //helpers.log('+Response: '+response.msg);
                        app.handleUserPickSuccess(response.data, response.tiebreakers, response.eventId, response.userEmail, response.userName);
                    } else {
                        helpers.log('-Response: ' + response.msg);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    helpers.log('AJAX error:' + textStatus);
                }
            });
        })
    },

    validateAllFightsSelected:function() {
        var value = false;
        var numSelected = 0;
        var $fighterNames = $('.fighter-name');
        var selectedReq = $fighterNames.length * .5;
        helpers.log('selectedReq: ' + selectedReq);

        // calc num selected
        $fighterNames.each(function(index, element) {
            var $element = $(element);
            if ($element.hasClass('selected')) {
                // check for round and finish type
                numSelected++;
            }
        });

        // check if all fighters have been selected
        if (selectedReq == numSelected) {
            //helpers.log('SUCCESS: All fights have been selected');
            value = true;
        } else {
            helpers.log('ERROR: Please select a fighter from all of the matchups');
            app.displayErrorMessage();
        }

        return value;
    },

    sendPickConfirmationEmail:function(data, tiebreakers, eventId, userEmail, userName) {
        var data = data;
        var datastring = '';
        datastring += '&fighters=' + data;
        datastring += '&tiebreakers=' + tiebreakers;
        datastring += '&eventId=' + eventId;
        datastring += '&userEmail=' + userEmail;
        datastring += '&userName=' + userName;

        $.ajax({
            url: app.DIRPATH + 'app/sendemail',
            dataType: 'json',
            type: 'POST',
            data: datastring,
            success: function(response, status) {
                helpers.log('AJAX success: ' + response);
                if (response.state) {
                    helpers.log('+Response: ' + response.msg);
                } else {
                    helpers.log('-Response: ' + response.msg);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                helpers.log('AJAX error:' + textStatus);
            }
        });
    },

    displaySuccessMessage:function(msg) {
        var $makePickAlert = $('#make-pick-alert');
        var delay = 5000;
        var msg = msg || 'You have made your picks!  Check your email for confirmation';

        $makePickAlert.html(msg);
        $makePickAlert.fadeIn(function(event) {
            // animation complete
            $(this).delay(delay).fadeOut();
        });
    },

    displayErrorMessage:function(msg) {
        var $makePickAlert = $('#make-pick-alert');
        var delay = 5000;
        var msg = msg || 'ERROR: Please select a fighter from all matchups.';

        // update class
        $makePickAlert.removeClass('alert-success');
        $makePickAlert.addClass('alert-danger');

        $makePickAlert.html(msg);
        $makePickAlert.fadeIn(function(event) {
            // animation complete
            $(this).delay(delay).fadeOut(function() {
                $makePickAlert.addClass('alert-success');
                $makePickAlert.removeClass('alert-danger');
            });
        });
    },

    /**** handler s****/
    handleUserPickSuccess:function(data, tiebreakers, eventId,userEmail, userName) {
        this.displaySuccessMessage();
        this.sendPickConfirmationEmail(data, tiebreakers, eventId, userEmail, userName);
    },

    /**** getters & setters ****/
    getFollowers: function() {
        return this.followersData;
    },
    getMyUserId: function() {
        return $('#page').attr('data-user-id');
    }
}; // end app

tryLoad();
function tryLoad() {
    try {
        $(document).ready(function() {
            app.init();
        })
    } catch (error) {
        setTimeout(function() {
            tryLoad();
        }, 1000)
    }
}