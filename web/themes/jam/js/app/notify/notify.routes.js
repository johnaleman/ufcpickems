(function() {
    angular.module('app.notify')
        .config(NotifyConfig);

    function NotifyConfig($stateProvider) {
        $stateProvider.state('notify', {
            url: '/notify',
            templateUrl: '/js/app/notify/notify.html',
            controller: 'NotifyController',
            controllerAs: 'vm'
        })
    }
})();