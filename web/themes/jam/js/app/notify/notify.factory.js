(function() {
    angular.module('app.notify')
            .factory('NotifyFactory', NotifyFactory);

    function NotifyFactory($log, $http, APP, growl) {
        return {
            sendEmail: sendEmail
        }

        function sendEmail(data) {
            var data = {
                type: data.type,
                emailMessage: data.content,
                eventId: data.eventId
            };

            return $http.post(APP.webroot + '/app/sendEmail', data)
                    .then(requestComplete)
                    .catch(requestFailed);

            function requestComplete(response) {
                var message = 'SUCCESS!: Message has been sent';
                growl.addSuccessMessage(message);
                return response.data;
            }

            function requestFailed() {
                var message = 'ERROR: Message was not sent';
                growl.addErrorMessage(message);
            }
        }
    }
})();