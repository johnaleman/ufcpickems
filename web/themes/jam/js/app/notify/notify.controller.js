(function() {
    angular.module('app.notify')
            .controller('NotifyController', NotifyController);

    function NotifyController($log, $scope, $filter, NotifyFactory, Event, Fighter) {
        var vm = this;
        vm.name = 'Notify';
        vm.data = {
            type: '',
            content: '',
            eventId: null
        };

        vm.formState = {
            displaySelectEvent: false,
            displaySelectFighter1: false,
            displaySelectFighter2: false
        };

        // event
        vm.eventList = [];
        vm.filteredEventList = [];
        vm.handleEventChange = handleEventChange;

        // fighter 1
        vm.fighterList1 = [];
        vm.filteredFighterList1 = [];
        vm.handleFighter1Change = handleFighter1Change;

        // fighter 2
        vm.fighterList2 = [];
        vm.filteredFighterList2 = [];
        vm.handleFighter2Change = handleFighter2Change;

        vm.isLoading = false;
        vm.submitForm = submitForm;
        vm.handleTypeChange = handleTypeChange;

        vm.EVENT = {
            PICKS_REMINDER: 'picksReminder',
            EVENT_OPEN: 'eventOpen',
            EVENT_CLOSING: 'eventClosing',
            OPEN_ENROLLMENT: 'openEnrollment',
            PAYMENT_REMINDER: 'paymentReminder',
            MATCHUP_CHANGE: 'matchupChange'
        };

        fetchEventList();
        function fetchEventList() {
            var params = {round: 'active'};
            Event.query(params, function(response) {
                vm.eventList = response.data.list;
            });
        }

        fetchFighterList();
        function fetchFighterList() {
            var params = {};
            Fighter.query(params, function(response) {
                vm.fighterList1 = vm.fighterList2 = response.data.list;
            });
        }

        function submitForm(isValid) {
            // quick validate
            if(vm.data.type === '') {
                $log.log('vm.data.type is empty');
                return false;
            }

            if(vm.data.type === 'picksReminder') {
                if(vm.data.eventId === null) {
                    $log.log('vm.data.eventId is null');
                    return false;
                }
            }

            if(isValid) {
                vm.isLoading = true;

                var $promise = NotifyFactory.sendEmail(vm.data);
                $promise.then(function(response) {
                    vm.isLoading = false;
                    vm.data.type = 'picksReminder';
                    vm.data.content = '';
                    vm.data.eventId = vm.eventList[0].id;
                })
            }
        }

        function handleEventChange() {
            updateTemplate();
        }

        function handleFighter1Change() {
            updateTemplate();
        }

        function handleFighter2Change() {
            updateTemplate();
        }

        function handleTypeChange() {
            updateTemplate();
        }

        function updateTemplate() {
            var index = getIndexOf(vm.eventList, vm.data.eventId, 'id'),
                eventName = vm.eventList[index].name,
                simpleTime = vm.eventList[index].simpleTime,
                date = $filter('filterDate')(vm.eventList[index].date),
                todayISO = $filter('filterDate')(new Date(). toISOString()),
                dateS = date === todayISO ? 'today' : 'on ' + date,
                fighterIndex1 = getIndexOf(vm.fighterList1, vm.data.fighterId1, 'id'),
                fighterIndex2 = getIndexOf(vm.fighterList2, vm.data.fighterId2, 'id'),
                fighter1 = vm.fighterList1[fighterIndex1],
                fighter2 = vm.fighterList2[fighterIndex2],
                fighterName1 = fighter1.firstName + ' ' + fighter1.lastName,
                fighterName2 = fighter2.firstName + ' ' + fighter2.lastName;

            // dynamically define templates
            var templates = {};
            templates[vm.EVENT.PICKS_REMINDER] = "Do not forget to make your picks for " + eventName + " at least 1 hour before " + simpleTime + " (PST) "+ dateS +".";
            templates[vm.EVENT.EVENT_OPEN] = eventName + " is now <a href='http://bit.ly/ufcpickems_make_your_picks'>open for picks!</a> " +
                    "<br/><br/> " +
                    "Shit happens and the lineup might change so if 'it' happens you will be emailed. " +
                    "<br/> <br/> " +
                    "The event will close around 1 hour before the main event.";

            templates[vm.EVENT.EVENT_CLOSING] = eventName + " is now closed, check out the <a href='http://bit.ly/ufcpickems_result'> results</a>  and <a href='http://bit.ly/ufcpickems_standings'>current standings</a> in near real time.";
            templates[vm.EVENT.MATCHUP_CHANGE] = "Shit happened! " +
                    "<br/><br/>  " +
                    "An update has been made to " + eventName + ". " +
                    "<br/><br/> " +
                    "<a href='http://bit.ly/ufcpickems_make_your_picks'>Pick on " + fighterName1 + " vs " + fighterName2 + ".</a>";

            templates[vm.EVENT.OPEN_ENROLLMENT] = "";

            // apply template and update state
            vm.data.content = templates[vm.data.type];
            updateFormState();
        }

        function updateFormState() {
            switch(vm.data.type) {
                // fall through
                case vm.EVENT.PICKS_REMINDER:
                case vm.EVENT.EVENT_OPEN:
                case vm.EVENT.EVENT_CLOSING:
                    vm.formState.displaySelectEvent = true;
                    vm.formState.displaySelectFighter1 = false;
                    vm.formState.displaySelectFighter2 = false;
                    break;
                case vm.EVENT.MATCHUP_CHANGE:
                    vm.formState.displaySelectEvent = true;
                    vm.formState.displaySelectFighter1 = true;
                    vm.formState.displaySelectFighter2 = true;
                    break;
                default:
                    vm.formState.displaySelectEvent = false;
                    vm.formState.displaySelectFighter1 = false;
                    vm.formState.displaySelectFighter2 = false;
                    $log.warn('No case for: ' + vm.data.type);
                    break;
            }
        }

        // TODO: add to helpers service
        function getIndexOf(arr, val, prop) {
            var l = arr.length,
                    k = 0;
            for (k = 0; k < l; k = k + 1) {
                if (arr[k][prop] === val) {
                    return k;
                }
            }
            return false;
        }

        $scope.$watchCollection('vm.filteredEventList', function(newVal, oldVal) {
            if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                if(oldVal.length > 0) {
                    vm.data.eventId = newVal[0].id;

                    handleEventChange();
                } else {
                    // first load
                    vm.data.eventId = newVal[0].id;
                }
            }
        });

        $scope.$watchCollection('vm.filteredFighterList1', function(newVal, oldVal) {
            if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                if(oldVal.length > 0) {
                    vm.data.fighterId1 = newVal[0].id;

                    handleEventChange();
                } else {
                    // first load
                    vm.data.fighterId1 = newVal[0].id;
                }
            }
        });

        $scope.$watchCollection('vm.filteredFighterList2', function(newVal, oldVal) {
            if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                if(oldVal.length > 0) {
                    vm.data.fighterId2 = newVal[0].id;

                    handleEventChange();
                } else {
                    // first load
                    vm.data.fighterId2 = newVal[0].id;
                }
            }
        });
    }
})();