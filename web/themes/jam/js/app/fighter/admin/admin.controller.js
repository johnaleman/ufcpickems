(function() {
    angular
        .module('app.fighter')
        .controller('FighterAdminController', FighterAdminController);

    function FighterAdminController($log, $state, $mdDialog, User, Fighter) {
        var vm = this;
        vm.title = 'Fighter Admin';
        vm.user = User;

        // table
        vm.selected = [];
        vm.options = {
            autoSelect: true,
            rowSelection: true
        };
        vm.query = {
            order: '',
            limit: 5,
            page: 1
        };

        vm.fighterList = [];
        vm.fighterListCount = 0;

        loadData();
        function loadData() {
            var params = {};
            Fighter.query(params, function(response) {
                vm.fighterList = response.data.list;
                vm.fighterListCount = response.data.count;
            });
        }

        /**** delete ****/
        vm.status = '  ';
        vm.showConfirm = function(ev, id) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                    .title('Are You Sure?')
                    .textContent('You can not undo this action.')
                    .ariaLabel('You can not undo this action')
                    .targetEvent(ev)
                    .ok('Delete')
                    .cancel('Cancel');

            $mdDialog.show(confirm).then(function(response) {
                // ok
                // do delete
                var params = {
                    id: id
                }
                Fighter.delete(params, function(response) {
                    $log.log('response', response);
                    $state.go('fighter.admin', {}, {notify: true, reload: true});
                })
            }, function() {
                // cancel
                $log.log('canceled...');
            });
        };
    }
})();