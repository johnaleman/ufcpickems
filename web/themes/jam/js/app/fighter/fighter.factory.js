(function() {
    angular
        .module('app.fighter')
        .factory('Fighter', Fighter);

    function Fighter($log, $resource, APP, CoreCache) {
        return $resource(APP.webroot + '/api/fighter/:id', {
            id: '@id'
        }, {
            query: {
                method: 'GET',
                params: {},
                isArray: false,
                cache: false
            },
            save: {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded, charset=utf-8'
                },
                transformRequest: function (data, headersGetter) {
                    var key, result = [], response;
                    if(typeof data == "string") { //$http support
                        response = data;
                    } else {
                        for (key in data) {
                            if (data.hasOwnProperty(key)) {
                                result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
                            }
                        }
                        response = result.join("&");
                    }

                    return response;
                }
            },
            update: {
                method: 'PUT'
            }
        })
    }
})();