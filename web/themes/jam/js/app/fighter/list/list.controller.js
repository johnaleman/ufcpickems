(function() {
    angular
        .module('app.fighter')
        .controller('FighterListController', FighterListController);

    function FighterListController($log, FighterList) {
        var vm = this;
        vm.title = 'Fighter List';
        vm.fighterList = FighterList.data.list;
    }
})();
