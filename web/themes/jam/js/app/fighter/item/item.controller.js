(function() {
    angular
        .module('app.fighter')
        .controller('FighterItemController', FighterItemController);

    /*
     * Event Model
     *
     * id
     * firstName
     * lastName
     */
    function FighterItemController($log, Fighter) {
        var vm = this;
        vm.title = 'Fighter';
        vm.fighter = Fighter;
    }
})();