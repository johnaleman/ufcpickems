(function() {
    angular
        .module('app.fighter')
        .controller('FighterFormController', FighterFormController);

    function FighterFormController($log, $scope, $state, Fighter) {
        var vm = this;
        vm.fighter = Fighter;
        vm.title = angular.isDefined(Fighter.id) ? 'Update Fighter' : 'Create Fighter';

        // form
        vm.form = {};
        vm.error = null;
        vm.updating = false;
        vm.save = save;


        // save portfolio document
        function save(isValid) {
            if (!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'vm.form.eventForm');
                return false;
            }

            vm.updating = true;
            if (vm.fighter.id) {
                vm.fighter.$update(successCallback, errorCallback).then(function(response) {
                    vm.updating = false;
                    vm.fighter = response.data;
                });
            } else {
                vm.fighter.$save({id: vm.fighter.id}, successCallback, errorCallback).then(function(response) {
                    vm.updating = false;
                    $state.go('fighter.create', {}, {notify: true, reload: true});
                });
            }

            function successCallback(fighter) {}
            function errorCallback(res) {
                vm.error = res.data.message;
            }
        }
    }
})();