(function() {
    angular
        .module('app.fighter')
        .config(FighterRouteConfig);

    function FighterRouteConfig($stateProvider) {
        $stateProvider
            .state('fighter', {
                abstract: true,
                url: '/fighter',
                template: '<ui-view/>'
            })
            .state('fighter.list', {
                url: '',
                templateUrl: '/js/app/fighter/list/list.html',
                controller: 'FighterListController',
                controllerAs: 'vm',
                resolve: {
                    FighterList: getFighterList
                }
            }).state('fighter.item', {
                url: '/{id:int}', // only match integer
                templateUrl: '/js/app/fighter/item/item.html',
                controller: 'FighterItemController',
                controllerAs: 'vm',
                resolve: {
                    Fighter: getFighter
                }
            })
            .state('fighter.create', {
                url: '/create',
                templateUrl: '/js/app/fighter/form/form.html',
                controller: 'FighterFormController',
                controllerAs: 'vm',
                resolve: {
                    Fighter: newFighter
                }
            })
            .state('fighter.edit', {
                url: '/{id:int}/edit',
                templateUrl: '/js/app/fighter/form/form.html',
                controller: 'FighterFormController',
                controllerAs: 'vm',
                resolve: {
                    Fighter: getFighter
                }
            })
            .state('fighter.admin', {
                url: '/admin',
                templateUrl: '/js/app/fighter/admin/admin.html',
                controller: 'FighterAdminController',
                controllerAs: 'vm',
                resolve: {
                    User: function(User) {
                        return User.getUser();
                    }
                }
            })
    }

    function getFighterList(Fighter) {
        return Fighter.query().$promise;
    }

    function getFighter($stateParams, Fighter) {
        return Fighter.get({
            id: $stateParams.id
        }).$promise;
    }

    function newFighter(Fighter) {
        return new Fighter();
    }
})();