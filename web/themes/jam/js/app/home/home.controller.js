(function() {
    angular
        .module('app.home')
        .controller('HomeController', HomeController);

    function HomeController($rootScope, $log, UserObj, User, SettingsModel) {
        var vm = this;
        vm.isActiveUser = UserObj.isActiveUser;
        vm.hasPaid = UserObj.hasPaid;
        vm.round = SettingsModel.round;
        vm.isOpenEnrollment = SettingsModel.isOpenEnrollment;
        vm.handleEnrollClick = handleEnrollClick;

        function handleEnrollClick() {
            var $promise = User.enrollUser();
            $promise.then(function(response) {
                vm.isActiveUser = UserObj.isActiveUser = true;
                vm.hasPaid = UserObj.hasPaid = "0";
                User.setUser(UserObj);

                $rootScope.$broadcast('user:updated', UserObj);
            });
        }
    }
})();