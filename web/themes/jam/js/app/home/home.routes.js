(function() {
    angular
        .module('app.home')
        .config(function($stateProvider) {
            $stateProvider.state('home', {
                url: '/home',
                templateUrl: '/js/app/home/home.view.html',
                controller: 'HomeController',
                controllerAs: 'vm',
                resolve: {
                    UserObj: function(User) {
                        return User.getUser();
                    }
                }
            })
        })
})();