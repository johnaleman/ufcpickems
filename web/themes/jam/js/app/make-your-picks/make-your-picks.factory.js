(function() {
    angular
    .module('app.makeyourpicks')
    .factory('MakeYourPicksFactory', MakeYourPicksFactory);

    function MakeYourPicksFactory($log, $http, APP) {
        return {
            fetchEvents: fetchEvents,
            submitPicks: submitPicks
        };

        function fetchEvents() {
            return $http.get(APP.webroot + '/pick/makepick', {cache: false})
            .then(fetchEventsComplete)
            .catch(fetchEventsFailed);

            function fetchEventsComplete(response) {
                return response.data.data;
            }

            function fetchEventsFailed(error) {
                $log.error('XHR Failed for fetchMatchups.' + error.data);
            }
        }

        function submitPicks(data) {
            return $http.post(APP.webroot + '/pick/submituserpicks', data)
                    .then(fetchEventsComplete)
                    .catch(fetchEventsFailed);

            function fetchEventsComplete(response) {
                return response.data.data;
            }

            function fetchEventsFailed(error) {
                $log.error('XHR Failed for fetchMatchups.' + error.data);
            }
        }
    }
})();