(function() {
    'use strict';

    angular
        .module('app.makeyourpicks')
        .directive('makeYourPicksEventItem', makeYourPicksEventItem);

    /* @ngInject */
    function makeYourPicksEventItem() {
        var directive = {
            restrict: 'EA',
            templateUrl: '/js/app/make-your-picks/event-item/event-item.html',
            scope: {
                event: '='
            },
            controller: Controller,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

    function Controller($log, MakeYourPicksFactory, growl) {
        var vm = this;

        vm.displayMatchup = false;
        vm.requestInProgress = false;
        vm.mapToMatchup = mapToMatchup;
        vm.handleSubmit = handleSubmit;
        vm.toggleMatchupDisplay = toggleMatchupDisplay;

        // set round list
        vm.roundList = [
            { value: 1, name: '1' },
            { value: 2, name: '2' },
            { value: 3, name: '3' },
            { value: 4, name: '4' },
            { value: 5, name: '5' }
        ];

        // set finish method list
        vm.finishMethodList = [
            { value: 1, name: 'KO/TKO' },
            { value: 2, name: 'SUB' },
            { value: 3, name: 'DECISION' }
        ];

        activate();

        function activate() {
        }



        function mapToMatchup(matchup, key, value) {
            matchup[key] = value;
        }

        function handleSubmit(event) {
            vm.requestInProgress = true;
            var eventResource  = JSON.stringify({data: event});

            // check to see if all matchups have been selected
            var i, o, hasError = false, hasTiebreaker, selectedFighterID, selectedFinishTypeID, selectedRoundID;
            var matchupList = event.matchup;
            for (i = 0; i < matchupList.length; i++) {
                o = matchupList[i];
                hasTiebreaker = stringToBoolean(o.hasTiebreaker);
                selectedFighterID = o.selectedFighterID;

                if (hasTiebreaker) {
                    selectedFinishTypeID = o.selectedFinishTypeID;
                    selectedRoundID = o.selectedRoundID;

                    if (selectedFinishTypeID === '' ||
                        selectedFinishTypeID === null ||
                        selectedRoundID === '' ||
                        selectedRoundID === null) {
                        hasError = true;
                    }
                } else {
                    if (selectedFighterID === '' || selectedFighterID === null) {
                        hasError = true;
                    }
                }
            }

            // if has error
            if (hasError) {
                vm.requestInProgress = false;
                growl.addErrorMessage('Oops!  Check to make sure you have selected all fights and the tiebreaker.');
                return false;
            } else {
                var $promise = MakeYourPicksFactory.submitPicks(eventResource);
                $promise.then(function(data) {
                    vm.requestInProgress = false;
                })
            }
        }

        function stringToBoolean(string) {
            switch (string.toLowerCase().trim()) {
                case "true":
                case "yes":
                case "1":
                    return true;
                case "false":
                case "no":
                case "0":
                case null:
                    return false;
                default:
                    return Boolean(string);
            }
        }

        function toggleMatchupDisplay() {
            vm.displayMatchup = !vm.displayMatchup;
        }
    }
})();