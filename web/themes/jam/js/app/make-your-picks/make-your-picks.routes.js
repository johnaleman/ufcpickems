(function() {
    angular
        .module('app.makeyourpicks')
        .config(function($stateProvider) {
            $stateProvider
                .state('makeyourpicks', {
                    url: '/makeyourpicks',
                    templateUrl: '/js/app/make-your-picks/make-your-picks.html',
                    controller: 'MakeYourPicksController',
                    controllerAs: 'vm'
                })
        })
})();