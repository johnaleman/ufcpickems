(function() {
    angular
        .module('app.makeyourpicks')
        .controller('MakeYourPicksController', MakeYourPicksController);

    function MakeYourPicksController($log, $timeout, APP, MakeYourPicksFactory, growl) {
        var vm = this;

        vm.title = 'Make Your Picks';
        vm.eventList = []; // resource list
        vm.isLoading = true;

        vm.getOpenEvents = getOpenEvents;
        vm.getClosedEvents = getClosedEvents;

        activate();

        function activate() {
            fetchEvents();
        }

        function getClosedEvents() {
            return vm.eventList.filter(function(obj) {
                return obj.event.status === "0";
            });
        }

        function getOpenEvents() {
            return vm.eventList.filter(function(obj) {
                return obj.event.status === "1";
            });

        }

        function fetchEvents() {
            return MakeYourPicksFactory.fetchEvents().then(function(result){
                vm.eventList = result;

                $timeout(function() {
                    vm.isLoading = false;
                }, APP.settings.PRELOADER_SPEED);
            })
        }
    }
})();