(function() {
    angular
        .module('app.makeyourpicks')
        .factory('selectionFactory', selectionFactory);

    function selectionFactory($log) {
        var selections = [];
        var selectionObj = function() {
            this.matchupId;
            this.fighter1;
        }

        return {
            add: add,
            remove: remove,
            getSelections: getSelections
        };

        function add(id) {
            $log.log('add(' + id + ')');
            selections.push(id);
        }

        function remove(id) {
            $log.log('remove(' + id + ')');
        }

        function getSelections() {
            $log.log(''+selections+'');
            return selections;
        }
    }
})();



