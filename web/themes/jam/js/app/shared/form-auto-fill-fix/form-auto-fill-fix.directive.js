(function() {
    angular
        .module('app.shared')
        .directive('formAutofillFix', formAutofillFix);

    function formAutofillFix() {
        return function(scope, elem, attrs) {
            // Fixes Chrome bug: https://groups.google.com/forum/#!topic/angular/6NlucSskQjY
            elem.prop('method', 'POST');

            // Fix autofill issues where Angular doesn't know about autofilled inputs
            if(attrs.ngSubmit || attrs.$attr.ngSubmit) {
                setTimeout(function() {
                    elem.unbind('submit').bind('submit', function(e) {
                        // does not reach this line
                        e.preventDefault();
                        elem.find('input, textarea, select').trigger('input').trigger('change').trigger('keydown');
                        scope.$apply(attrs.ngSubmit);
//                        scope.$apply(attrs.$attr.ngSubmit);
                    });
                }, 0);
            }
        };
    }
})();