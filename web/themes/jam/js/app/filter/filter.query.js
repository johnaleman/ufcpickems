(function() {
    'use strict';

    angular
        .module('app.result.form')
        .filter('filterQuery', filterQuery);

    function filterQuery($log, $filter) {
        return function(items, input) {
            // if input is empty or undefined return items
            if(input == '' || angular.isUndefined(input)) {return items;}
            var input = input.toLowerCase();
            var filtered = [], fullName;

            angular.forEach(items, function(item) {
                // if full name matches the input
                fullName = item.firstName + ' ' + item.lastName;
                if(fullName.toLowerCase().indexOf(input) >= 0 ) filtered.push(item);
            });

            return filtered;
        }
    }
})();