(function() {
    angular.
    module('app.user').
    controller('UserLogoutController', UserLogoutController);

    function UserLogoutController($log, $q, $state, User) {
        var $promise = User.logout();
        $promise = $promise.then(function() {
            var $promise = User.fetchUser();
            $promise.then(function(response) {
                $state.go('home', {}, {reload: false});
            })
        });
    }
})();