(function() {
    angular
        .module('app.user')
        .controller('UserAdminController', UserAdminController);

    function UserAdminController($log, $state, $mdDialog, UserResource) {
        var vm = this;
        vm.title = 'User Admin';

        // table
        vm.selected = [];
        vm.options = {
            autoSelect: true,
            rowSelection: true
        };
        vm.query = {
            order: '',
            limit: 5,
            page: 1
        };

        vm.userList = [];
        vm.userListCount = 0;

        loadData();
        function loadData() {
            var params = {};
            UserResource.query(params, function(response) {
                vm.userList = response.data.list;
                vm.userListCount = response.data.count;
            });
        }

        /**** delete ****/
        vm.status = '  ';
        vm.showConfirm = function(ev, id) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                    .title('Are You Sure?')
                    .textContent('You can not undo this action.')
                    .ariaLabel('You can not undo this action')
                    .targetEvent(ev)
                    .ok('Delete')
                    .cancel('Cancel');

            $mdDialog.show(confirm).then(function(response) {
                // ok
                // do delete
                var params = {
                    id: id
                }
                UserResource.delete(params, function(response) {
                    $log.log('response', response);
                    $state.go('user.admin', {}, {notify: true, reload: true});
                })
            }, function() {
                // cancel
                $log.log('canceled...');
            });
        };
    }
})();