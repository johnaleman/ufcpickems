(function() {
    angular
        .module('app.user')
        .controller('UserLoginController', UserLoginController);

    function UserLoginController($log, $http, $state, User) {
        var vm = this;
        vm.title = 'Login';

        vm.submitForm = submitForm;
        vm.handleForgotPassword = handleForgotPassword;

        /*$timeout(function() {
            var tempEmail = vm.email;
            vm.email = tempEmail;

            var tempPass = vm.password;
            vm.password = tempPass;
        }, 1000);*/

        function submitForm(isValid) {
            // if form is valid
            if (isValid) {
                vm.submitted = true;

                var params = {
                    email: vm.email,
                    password: vm.password,
                    rememberMe: vm.rememberMe
                };

                // post 'loginForm' so yii can handle
                var LoginForm = {LoginForm: params};

                var $promise = User.login(LoginForm);
                $promise = $promise.then(function() {
                    var $promise = User.fetchUser();
                    $promise.then(function(response) {
                        $state.go('home', {}, {reload: false});
                    })
                });
            }
        }

        function handleForgotPassword(event) {
            event.preventDefault();
            $log.log('handleForgotPassword');

            var $promise = User.forgotPassword(vm.email);
            $promise = $promise.then(function(response) {
                // success
            })
        }
    }
})();