(function() {
    // Authentication service for user variables
    angular.module('app.user').
    service('UserStatusService', UserStatusService);

    function UserStatusService($resource) {
        return $resource('/user/status', {}, {
            update: {
                method: 'PUT'
            }
        });
    }
})();