(function() {
    angular.
    module('app.user').
    config(function($stateProvider) {
        $stateProvider.state('login', {
            url: '/login',
            templateUrl: '/js/app/user/login/user.login.html',
            controller: 'UserLoginController',
            controllerAs: 'vm'
        }).state('logout', {
            url: '/logout',
            template: '',
            controller: 'UserLogoutController'
        }).state('signup', {
            url: '/signup',
            templateUrl: '/js/app/user/signup/user.signup.html',
            controller: 'UserSignupController',
            controllerAs: 'vm'
        })
    })
})();