(function() {
    angular
        .module('app.user')
        .controller('UserFormController', UserFormController);

    // User = UserResource
    // TODO: Merge user resource and user service
    function UserFormController($log, $scope, $state, User) {
        var vm = this;
        vm.user = User;
        vm.title = angular.isDefined(User.id) ? 'Update User' : 'Signup';

        // form
        vm.form = {};
        vm.error = null;
        vm.updating = false;
        vm.submitForm = submitForm;

        // save portfolio document
        function submitForm(isValid) {
            if(vm.updating) {return;}

            if (!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'vm.form.eventForm');
                return false;
            }

            vm.updating = true;
            if (vm.user.id) {
                vm.user.$update(successCallback, errorCallback).then(function(response) {
                    var $updateCopy = vm.user.$update; // copy $update to re-map to resource

                    vm.updating = false;
                    vm.user = response.data;
                    vm.user.$update = $updateCopy; // re-map to resource
                });
            } else {
                vm.user.$save({id: vm.user.id}, successCallback, errorCallback).then(function(response) {
                    vm.updating = false;
                    $state.go('user.create', {}, {notify: true, reload: true});
                });
            }

            function successCallback(user) {}
            function errorCallback(res) {
                vm.error = res.data.message;
            }
        }
    }
})();