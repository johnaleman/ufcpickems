(function() {
    angular.module('app.user').
    service('User', User);

    function User($rootScope, $log, $http, $q) {
        var origin = window.location.origin;
        var pathName = document.location.pathname;
        var webroot = origin + pathName;

        var self = User;
        var _user;

        var fetchUser = function() {
            var deferred = $q.defer();

            $http.get(webroot + '/user/status').then(function(response) {
                var user = response.data.data;
                _user = user;
                deferred.resolve(user);
            }, function error(response) {
                // handle error
            });

            return deferred.promise;
        };

        var forgotPassword = function(email) {
            var deferred = $q.defer();

            var url = 'site/forgotPassword';
            var params = {
                email: email
            }

            $http.post(url, $.param(params), {
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                var error = response.message;
                $log.error('[error]: ', error);
                deferred.reject(error);
            })

            return deferred.promise;
        };

        var enrollUser = function() {
            var deferred = $q.defer();
            var url = webroot + 'app/enrolluser';
            var params = {};

            $http.post(url, params)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(response) {
                    var error = response.msg;
                    $log.error('[error]: ', error);
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        return {
            setUser: function(user) {
                _user = user;
            },

            getUser: function() {
                return _user;
            },

            fetchUser: fetchUser,
            enrollUser: enrollUser,

            signUp: function(UserForm) {
                var deferred = $q.defer();

                var url = 'user/create';
                var UserForm = UserForm; // post 'LoginForm' so yii can handle

                $http.post(url, $.param(UserForm), {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                }).success(function(response) {
                    deferred.resolve(response);

                    /*var $promise = fetchUser();
                    $promise.then(function(response){
                        $rootScope.$broadcast('user:updated', response);
                        deferred.resolve(response);
                    });*/
                }).error(function(response) {
                    $scope.error = response.message;
                    $log.error('[error]: ', $scope.error);
                    deferred.reject($scope.error);
                });

                return deferred.promise;
            },

            login: function(LoginForm) {
                var deferred = $q.defer();

                var url = 'site/login';
                var LoginForm = LoginForm; // post 'LoginForm' so yii can handle

                $http.post(url, $.param(LoginForm), {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                }).success(function(response) {
                    //$log.log('[success]: logged in!', response);

                    // todo: set user object
                    // If successful we assign the response to the global user model
                    //$scope.authentication.user = response;

                    // And redirect to the prev state request or home
                    //$location.path('/');

                    //$state.go('home', {}, {reload: true});

                    //$rootScope.$broadcast('user:updated', response.data);
                    //deferred.resolve(response);

                    var $promise = fetchUser();
                    $promise.then(function(response) {
                        $rootScope.$broadcast('user:updated', response);
                        deferred.resolve(response);
                    });
                }).error(function(response) {
                    $scope.error = response.message;
                    $log.error('[error]: ', $scope.error);
                    deferred.reject($scope.error);
                });

                return deferred.promise;
            },

            forgotPassword: forgotPassword,

            logout: function() {
                var deferred = $q.defer();

                var url = 'site/logout';
                var params = {};

                $http.post(url, $.param(params), {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                }).success(function(response) {
                    //$log.log('[success]: logged out!', response);

                    // todo: set user object
                    // If successful we assign the response to the global user model
                    //$scope.authentication.user = response;

                    // And redirect to the prev state request or home
                    //$location.path('/');

                    var $promise = fetchUser();
                    $promise.then(function(response) {
                        $rootScope.$broadcast('user:updated', response);
                        deferred.resolve(response);
                    });

                    //$rootScope.$broadcast('user:updated', response.data);
                    //deferred.resolve(response);
                    //$state.go('home');
                }).error(function(response) {
                    $scope.error = response.message;
                    $log.error('[error]: ', $scope.error);
                    deferred.reject($scope.error);
                });

                return deferred.promise;
            }
        }
    }
})();