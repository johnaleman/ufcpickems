(function() {
    angular
        .module('app.user')
        .controller('UserListController', UserListController);

    function UserListController($log, UserList) {
        var vm = this;
        vm.title = 'User List';
        vm.userList = UserList.data.list;
    }
})();
