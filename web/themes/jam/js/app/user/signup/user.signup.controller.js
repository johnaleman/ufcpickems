(function() {
    // credit: https://scotch.io/tutorials/angularjs-form-validation
    angular
        .module('app.user')
        .controller('UserSignupController', UserSignupController);

    function UserSignupController($log, User) {
        //$log.log('[register controller]: UserSignupController');
        var vm = this;

        vm.submitForm = function(isValid) {
            // if form is valid
            if (isValid) {
                vm.submitted = true;

                var params = {
                    firstname: vm.firstname,
                    lastname: vm.lastname,
                    username: vm.username,
                    phone: vm.phone,
                    email: vm.email,
                    password: vm.password
                };

                // post 'UserForm' so yii can handle
                var UserForm = {User: params};

                var $promise = User.signUp(UserForm);
                $promise.then(function(result) {
                    $log.log('[success] user signed up...', result);
                })
            }
        };
    }
})();