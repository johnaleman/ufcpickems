(function() {
    angular.module('app.user').config(UserResourceConfig);

    function UserResourceConfig($stateProvider) {
        $stateProvider
            .state('user', {
                abstract: true,
                url: '/user',
                template: '<ui-view/>'
            })
            .state('user.list', {
                url: '',
                templateUrl: '/js/app/user/list/list.html',
                controller: 'UserListController',
                controllerAs: 'vm',
                resolve: {
                    UserList: getUserList
                }
            })
            .state('user.item', {
                url: '/{id:int}', // only match integer
                templateUrl: '/js/app/user/item/item.html',
                controller: 'UserItemController',
                controllerAs: 'vm',
                resolve: {
                    User: getUser
                }
            })
            .state('user.create', {
                url: '/signup',
                templateUrl: '/js/app/user/form/form.html',
                controller: 'UserFormController',
                controllerAs: 'vm',
                resolve: {
                    User: newUser
                }
            })
            .state('user.edit', {
                url: '/{id:int}/edit',
                templateUrl: '/js/app/user/form/form.html',
                controller: 'UserFormController',
                controllerAs: 'vm',
                resolve: {
                    User: getUser
                }
            })
            .state('user.admin', {
                url: '/admin',
                templateUrl: '/js/app/user/admin/admin.html',
                controller: 'UserAdminController',
                controllerAs: 'vm'
            })
    }

    function getUserList(UserResource) {
        return UserResource.query().$promise;
    }

    function getUser($stateParams, UserResource) {
        return UserResource.get({
            id: $stateParams.id
        }).$promise;
    }

    function newUser(UserResource) {
        return new UserResource();
    }
})();