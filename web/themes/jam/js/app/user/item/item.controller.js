(function() {
    angular
        .module('app.user')
        .controller('UserItemController', UserItemController);

    /*
     * Event Model
     *
     * id
     * firstName
     * lastName
     */
    // User = UserResource
    // TODO: Merge user resource and user service
    function UserItemController($log, User) {
        var vm = this;
        vm.title = 'User';
        vm.user = User;
    }
})();