(function() {
    angular
        .module('app.auto')
        .controller('AutoController', AutoController);

    function AutoController($log, $scope, AutoFactory, Event) {
        var vm = this;

        vm.EVENT = {
            NEW: 'new',
            EXISTING: 'existing'
        };

        vm.data = {
            type: '',
            eventId: null
        };

        vm.formState = {
            displaySelectEvent: false
        };

        // settings
        vm.title = "Auto Create Events";
        vm.mobileUrl = '';
        vm.numMainMatchups = '';
        vm.submitForm = submitForm;

        // event
        vm.eventList = [];
        vm.filteredEventList = [];
        vm.handleEventChange = handleEventChange;
        vm.handleEventTypeChange = handleEventTypeChange;

        var $output = angular.element('#auto-output');

        fetchEventList();
        function fetchEventList() {
            var params = {round: 'active'};
            Event.query(params, function(response) {
                vm.eventList = response.data.list;
            });
        }

        function handleEventChange() {
            console.log('handleEventChange');

            var index = getIndexOf(vm.eventList, vm.data.eventId, 'id'),
                mobileUrl = vm.eventList[index].mobileUrl,
                numMainEventFights = vm.eventList[index].numMainEventFights;

            vm.mobileUrl = mobileUrl;
            vm.numMainMatchups = numMainEventFights;
        }

        function handleEventTypeChange() {
            var type = vm.data.type;

            if (type === vm.EVENT.EXISTING) {
                vm.formState.displaySelectEvent = true;
                handleEventChange();
            } else {
                vm.formState.displaySelectEvent = false;
                vm.mobileUrl = '';
                vm.numMainMatchups = '';
            }
        }

        function submitForm(isValid) {
            // quick validate
            if(vm.mobileUrl === '') {
                $log.log('vm.mobileUrl is empty');
                return false;
            }

            if(isValid) {
                $output.html(''); // clear output

                // generate
                var $promise = AutoFactory.generate(vm.mobileUrl, vm.numMainMatchups);
                $promise.then(function(response) {
                    fetchEventList();

                    var message = '<h1>Generation Complete!</h1>';
                    $output.html(message);
                });
            }
        }

        // TODO: add to helpers service
        function getIndexOf(arr, val, prop) {
            var l = arr.length,
                k = 0;
            for (k = 0; k < l; k = k + 1) {
                if (arr[k][prop] === val) {
                    return k;
                }
            }
            return false;
        }

        $scope.$watchCollection('vm.filteredEventList', function(newVal, oldVal) {
            if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                if(oldVal.length > 0) {
                    vm.data.eventId = newVal[0].id;

                    handleEventChange();
                } else {
                    // first load
                    vm.data.eventId = newVal[0].id;
                }
            }
        });
    }
})();