(function() {
    angular
        .module('app.auto')
        .config(AutoConfig);

    function AutoConfig($stateProvider) {
        $stateProvider.state('auto', {
            abstract: true,
            url: '/auto',
            template: '<ui-view/>'
        })
        .state('auto.createAll', {
            url: '',
            templateUrl: '/js/app/auto/auto.html',
            controller: 'AutoController',
            controllerAs: 'vm'
        })
    }
})();