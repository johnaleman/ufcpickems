(function() {
    angular
        .module('app.auto')
        .factory('AutoFactory', AutoFactory);

    function AutoFactory($log, $http, $q, APP, Event, Fighter, Matchup, ResultFactory, growl) {
        var vm = this;

        // set map for fight metrics
        var roundMap = {
            1: 1,
            2: 2,
            3: 3,
            4: 4,
            5: 5
        }

        // set finish method list
        var methodMap = {
            "KO/TKO": 1,
            "Submission": 2,
            "Decision": 3
        }

        var mobileUrl = '';
        var numMainMatchups = 1;

        var eventId = null;
        var eventModel = {};
        var matchupModel = {};

        return {
            generate: generate
        }

        /*
         * init the generation sequence
         */
        function generate(url, numMainMatchups) {
            var deferred = $q.defer();

            setMobileUrl(url);
            setNumMainMatchups(numMainMatchups);

            enableCorsAnywhere();

            // init promise sequence
            getURLContents(url)
                .then(fetchEventDetails)
                .then(fetchMatchupModel)
                .then(createSingleEvent)
                .then(removeMatchupsByEvent)
                .then(createMatchups)
                .then(function() {
                    deferred.resolve();
                })
                .catch(function(error) {
                    $log.log(error);
                });

            return deferred.promise;
        }

        function getURLContents(url) {
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: APP.webroot + '/app/getcontents',
                params: {
                    url: url
                }
            })
                .then(fetchComplete)
                .catch(fetchFailed);

            function fetchComplete(response) {
                var data = decodeHtml(response.data.data);
                deferred.resolve(data);
            }

            function fetchFailed(error) {
                $log.error('[error]: XHR Failed for getURLContent.');
                deferred.reject($scope.error);
            }

            return deferred.promise;
        }

        function createSingleEvent() {
            var deferred = $q.defer();

            var response = getEventModel();

            var eventObj = {};

            eventObj.name = response.Name;
            eventObj.name = eventObj.name.replace('vs.', 'vs');

            eventObj.date = response.Date;
            eventObj.location = response.Location.City +', '+ response.Location.Country;
            eventObj.simpleTime = getFormattedTime(response.Time);
            eventObj.fightCard = response.FightCard;

            // create event resource
            var event = new Event();
            event.name = eventObj.name;
            event.date = eventObj.date;
            event.location = eventObj.location;
            event.mobileUrl = getMobileUrl();
            event.numMainEventFights = getNumMainMatchups();
            event.simpleTime = eventObj.simpleTime;
            event.status = 1;

            // save event
            event.$save(successCallback, errorCallback).then(function(response) {
                var isDuplicate = response.isDuplicate || false;
                var data;

                // complete
                if(isDuplicate == true) {
                    data = response.data[0];
                } else {
                    data = response.data;
                }

//                response['data'] = data;
//                response['eventObj'] = eventObj;
                deferred.resolve({data: data, eventObj: eventObj});
            });

            function successCallback(response) {
                if(response.status === false) {
                    growl.addErrorMessage(response.message);
                } else {
                    // success
                }
            }
            function errorCallback(message) {
                var error = message;
            }

            return deferred.promise;
        }

        function removeMatchupsByEvent(response) {
            var eventObj = response.eventObj;
            var eventId = eventObj['id'] = parseInt(response.data.id);

            var deferred = $q.defer();

            var data = {eventId: parseInt(eventId)}
            $http.post(APP.webroot + '/matchup/removeAllByEventId', data)
                    .then(fetchEventsComplete)
                    .catch(fetchEventsFailed);

            function fetchEventsComplete(response) {
                deferred.resolve({data: response, eventObj: eventObj});
            }

            function fetchEventsFailed(error) {
                $log.error('XHR Failed for removeMatchupsByEvent.');
            }

            return deferred.promise;
        }

        function createMatchups(response) {
            var matchupModel = getMatchupModel().FMLiveFeed.Fights;

            var eventObj = response.eventObj;
            $log.log('Fight Card', matchupModel);

            // loop through main card and create matchups
            var deferred = $q.defer();
            var i = 0;
            var length = getNumMainMatchups();

            create(i, length, eventObj, matchupModel); // init recursively

            function create(i, length, eventObj, matchupModel) {
                var matchupObj = matchupModel[i];

                var fighter1, fighter2;
                if(typeof matchupObj.Fighters[0].Name !== 'undefined') {
                    fighter1 = matchupObj.Fighters[0];
                    fighter2 = matchupObj.Fighters[1];
                } else {
                    // has results
                    fighter1 = matchupObj.Fighters[1];
                    fighter2 = matchupObj.Fighters[0];
                }

                var firstNameFighter1,
                    lastNameFighter1,
                    firstNameFighter2,
                    lastNameFighter2;

                // if has results
                if(typeof fighter1.Name === 'undefined') {
                    firstNameFighter1 = fighter1.FirstName;
                    lastNameFighter1 = fighter1.LastName;
                    firstNameFighter2 = fighter2.FirstName;
                    lastNameFighter2 = fighter2.LastName;

                    // check result matchup order against the original matchup order
                    // because of matchup changes the matchup order might be different
                    var j,
                        x = eventObj.fightCard.length,
                        checkFighter1FirstName,
                        checkFighter1LastName,
                        checkFighter2FirstName,
                        checkFighter2LastName;

                    for(j = 0; j < x; j++) {
                        checkFighter1FirstName = eventObj.fightCard[j].Fighters[0].Name.FirstName;
                        checkFighter1LastName = eventObj.fightCard[j].Fighters[0].Name.LastName;
                        checkFighter2FirstName = eventObj.fightCard[j].Fighters[1].Name.FirstName;
                        checkFighter2LastName = eventObj.fightCard[j].Fighters[1].Name.LastName;

                        if(checkFighter1FirstName === firstNameFighter1 &&
                            checkFighter1LastName === lastNameFighter1 &&
                            checkFighter2FirstName === firstNameFighter2 &&
                            checkFighter2LastName === lastNameFighter2) {

                            // found match
                            fighter1 = eventObj.fightCard[i].Fighters[0];
                            fighter2 = eventObj.fightCard[i].Fighters[1];
                            break;
                        }
                    }
                }

                // get fighter ids
                $promise = createFightersForMatchup(fighter1, fighter2, matchupObj, eventObj, matchupModel);

                // create matchups
                $promise.then(function(response) {
                    var fighter1 = response.data.matchup.fighter1;
                    var fighter2 = response.data.matchup.fighter2;
                    var eventObj = response.data.eventObj;
                    var matchupModel = response.data.matchupModel;
                    var hasTiebreaker = i === 0 ? 1 : 0;

                    // create matchup resource
                    var matchup = new Matchup();
                    matchup.fighter1Id = fighter1.response.id;
                    matchup.fighter2Id = fighter2.response.id;
                    matchup.eventId = eventObj.id;
                    matchup.hasTiebreaker = hasTiebreaker;

                    // check if has result
                    var result = getResultObj(fighter1, fighter2, matchupObj, eventObj, matchupModel);
                    if(result) {
                        // create result
                        var model = JSON.stringify({data: result});
                        $promise = ResultFactory.submitResult(model);
                        $promise.then(function(data) {
                            // success created result
                        })
                    }

                    // save matchup
                    $promise = matchup.$save();
                    $promise.then(function(response) {
                        i++;

                        var isDuplicate = response.isDuplicate || false;
                        if (isDuplicate) {
                            response['data'] = response.data[0];
                        } else {
                            response['data'] = response.data;
                        }

                        // if not reached the end
                        if(i < length) {
                            create(i, length, eventObj, matchupModel);
                        } else {
                            // complete
                            deferred.resolve(response);
                        }
                    });
                });
            }
        }

        function getResultObj(fighter1, fighter2, matchupObj, eventObj, matchupModel) {
            var response = false;
            var result = {selectedFighterID: 0, selectedEventID: 0, selectedRoundID: 0, selectedFinishTypeID: 0};

            // check if there is a result
            if(fighter1.Outcome.Outcome === "Win") {
                // fighter 1 winner
                response = true;

                result.selectedFighterID = fighter1.response.id;
            } else if(fighter2.Outcome.Outcome === "Win") {
                // fighter 2 winner
                response = true;
                result.selectedFighterID = fighter2.response.id;
            }

            if(response) {
                result.selectedEventID = eventObj.id;

                // method
                var methodKey = matchupObj.Method;

                if(methodKey.toLowerCase().indexOf('sub') > -1) {
                    methodKey = 'Submission';
                } else if(methodKey.toLowerCase().indexOf('dec') > -1 ) {
                    methodKey = 'Decision';
                }
                result.selectedFinishTypeID = methodMap[methodKey];

                // round
                var roundKey = matchupObj.EndingRoundNum;
                result.selectedRoundID = roundMap[roundKey];

                response = result;
            }

            return response;
        }

        function enableCorsAnywhere() {
            (function() {
                var cors_api_host = 'cors-anywhere.herokuapp.com';
                var cors_api_url = 'https://' + cors_api_host + '/';
                var slice = [].slice;
                var origin = window.location.protocol + '//' + window.location.host;
                var open = XMLHttpRequest.prototype.open;
                XMLHttpRequest.prototype.open = function() {
                    var args = slice.call(arguments);
                    var targetOrigin = /^https?:\/\/([^\/]+)/i.exec(args[1]);
                    if (targetOrigin && targetOrigin[0].toLowerCase() !== origin &&
                            targetOrigin[1] !== cors_api_host) {
                        args[1] = cors_api_url + args[1];
                    }
                    return open.apply(this, args);
                };
            })();
        }

        function fetchEventDetails(html) {
            var deferred = $q.defer();

            // http://m.ufc.com/fm/api/event/detail/787.json
            var baseUrl = 'http://m.ufc.com/fm/api/event/detail/',
                eventId = JSON.parse($(html).find('#fm-event-result').attr('data-event_id')),
                cacheBust = '?c=' + (+new Date()),
                url = baseUrl + eventId + '.json' + cacheBust;

            setEventId(eventId); // set for global use

            $http({
                method: 'GET',
                url: url
            })
            .then(fetchComplete)
            .catch(fetchFailed);

            function fetchComplete(response) {
                setEventModel(response.data);

                var Fights = response.data.FightCard;

                response.data = {
                    "FMLiveFeed": {
                        "Fights": Fights
                    }
                }

                setMatchupModel(response.data);
                deferred.resolve();
            }

            function fetchFailed(error) {
                $log.error('[error]: XHR Failed for fetchEventDetails');
                deferred.reject(error);
            }

            return deferred.promise;
        }

        function fetchMatchupModel() {
            var deferred = $q.defer();

            enableCorsAnywhere();

            // http://liveapi.fightmetric.com/V1/789/Fnt.json
            var baseUrl = 'http://liveapi.fightmetric.com/V1/',
                    eventId = getEventId(),
                    cacheBust = '?c=' + (+new Date()),
                    url = baseUrl + eventId + '/Fnt.json' + cacheBust;

            $http({
                method: 'GET',
                url: url
            })
            .then(fetchComplete)
            .catch(fetchFailed);

            function fetchComplete(response) {
                setMatchupModel(response.data);
                deferred.resolve();
            }

            function fetchFailed(error) {
                var error = error || {};

                $log.error('[error]: XHR Failed for fetchMatchupModel.');
                deferred.resolve();
//                deferred.reject(error);
            }

            return deferred.promise;
        }

        function createFightersForMatchup(fighter1, fighter2, matchupObj, eventObj, matchupModel) {
            var deferred = $q.defer();

            // vars
            var firstNameFighter1,
                lastNameFighter1,
                firstNameFighter2,
                lastNameFighter2;

            if(typeof fighter1.Name !== 'undefined') {
                firstNameFighter1 = fighter1.Name.FirstName;
                lastNameFighter1 = fighter1.Name.LastName;
                firstNameFighter2 = fighter2.Name.FirstName;
                lastNameFighter2 = fighter2.Name.LastName;
            } else {
                // has results
                firstNameFighter1 = fighter1.FirstName;
                lastNameFighter1 = fighter1.LastName;
                firstNameFighter2 = fighter2.FirstName;
                lastNameFighter2 = fighter2.LastName;
            }

            // get resource
            var fighter = new Fighter();
            fighter.firstName = firstNameFighter1;
            fighter.lastName = lastNameFighter1;

            // save
            var $promise;

            $promise = fighter.$save();
            $promise.then(function(response) {
                var isDuplicate = response.isDuplicate || false;
                var data;

                if (isDuplicate) {
                    data = response.data[0];
                } else {
                    data = response.data;
                }

                // set fighter 1
                fighter1['response'] = data;

                // deferred.resolve(response);
                $promise = createFighter2(firstNameFighter2, lastNameFighter2);
                $promise.then(function(response) {
                    var isDuplicate = response.isDuplicate || false;
                    var data;

                    if (isDuplicate) {
                        data = response.data[0];
                    } else {
                        data = response.data;
                    }

                    // set fighter 2
                    fighter2['response'] = data;

                    // response model
                    response['data'] = {
                        matchup: {
                            fighter1: 0,
                            fighter2: 0
                        }
                    }

                    // set response
                    response['data']['matchup']['fighter1'] = fighter1;
                    response['data']['matchup']['fighter2'] = fighter2;
                    response['data']['matchupObj'] = matchupObj;
                    response['data']['eventObj'] = eventObj;
                    response['data']['matchupModel'] = matchupModel;

                    deferred.resolve(response);
                });
            });

            function createFighter2(firstNameFighter2, lastNameFighter2) {
                var deferred = $q.defer();

                // get resource
                var fighter = new Fighter();
                fighter.firstName = firstNameFighter2;
                fighter.lastName = lastNameFighter2;

                // save
                fighter.$save().then(function(response) {
                    deferred.resolve(response);
                });

                return deferred.promise;
            }

            return deferred.promise;
        }

        function toTitleCase(str) {
            return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }

        function decodeHtml(html) {
            var txt = document.createElement("textarea");
            txt.innerHTML = html;
            return txt.value;
        }

        function getFormattedTime(fourDigitTime) {
            var time = fourDigitTime.split(':'); // convert to array

            // fetch
            var hours = Number(time[0]);
            var minutes = Number(time[1]);
            var seconds = Number(time[2]);

            // calculate
            var timeValue = "" + ((hours >12) ? hours - 12 : hours);  // get hours
            timeValue += (minutes < 10) ? ":0" + minutes : ":" + minutes;  // get minutes
            // timeValue += (seconds < 10) ? ":0" + seconds : ":" + seconds;  // get seconds
            timeValue += (hours >= 12) ? "pm" : "am";  // get AM/PM

            return timeValue;
        }

        function getNumMainMatchups() {
            return numMainMatchups;
        }

        function setNumMainMatchups(value) {
            numMainMatchups = parseInt(value);
        }

        function getMobileUrl() {
            return mobileUrl;
        }

        function setMobileUrl(value) {
            mobileUrl = value;
        }

        function setEventId(value) {
            eventId = value;
        }

        function getEventId() {
            return eventId;
        }

        function setEventModel(value) {
            eventModel = value;
        }

        function getEventModel() {
            return eventModel;
        }

        function setMatchupModel(value) {
            matchupModel = value;
        }

        function getMatchupModel() {
            return matchupModel;
        }
    }
})();