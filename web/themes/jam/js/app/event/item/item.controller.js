(function() {
    angular
        .module('app.event')
        .controller('EventItemController', EventItemController);

    /*
     * Event Model
     *
     * id
     * name
     * date
     * location
     * status
     * round
     * sendReminder
     * simpleTime
     */
    function EventItemController($log, Event) {
        var vm = this;
        vm.title = 'Event';
        vm.event = Event;
    }
})();