(function() {
    angular
        .module('app.event')
        .controller('EventFormController', EventFormController);

    function EventFormController($log, $scope, $timeout, $state, APP, Event) {
        var vm = this;
        vm.event = Event;
        vm.title = angular.isDefined(Event.id) ? 'Update Event' : 'Create Event';

        // form
        vm.form = {};
        vm.error = null;
        vm.updating = false;
        vm.save = save;
        
        // callendar
        vm.myDate = new Date();
        vm.minDate = new Date(
                vm.myDate.getFullYear(),
                vm.myDate.getMonth() - 2,
                vm.myDate.getDate());
        vm.maxDate = new Date(
                vm.myDate.getFullYear(),
                vm.myDate.getMonth() + 2,
                vm.myDate.getDate());
        vm.onlyWeekendsPredicate = function(date) {
            var day = date.getDay();
            return day === 0 || day === 6;
        }

        vm.isLoading = true;

        $timeout(function() {
            vm.isLoading = false;
        }, APP.settings.PRELOADER_SPEED);

        // save portfolio document
        function save(isValid) {
            if (!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'vm.form.eventForm');
                return false;
            }

            vm.updating = true;
            if (vm.event.id) {
                vm.event.$update(successCallback, errorCallback).then(function(response) {
                    var $update = vm.event.$update; // store $update to fix undefined bug

                    vm.updating = false;
                    vm.event = response.data;
                    vm.event.$update = $update; // remap $update
                });
            } else {
                vm.event.$save({id: vm.event.id}, successCallback, errorCallback).then(function(response) {
                    vm.updating = false;
                    $state.go('event.create', {}, {notify: true, reload: true});
                });
            }

            function successCallback(event) {}
            function errorCallback(res) {
                vm.error = res.data.message;
            }
        }
    }
})();