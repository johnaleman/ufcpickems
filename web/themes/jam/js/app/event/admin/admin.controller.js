(function() {
    angular
        .module('app.event')
        .controller('EventAdminController', EventAdminController);

    function EventAdminController($log, $state, $mdDialog, User, Event) {
        var vm = this;
        vm.title = 'Event Admin';
        vm.user = User;

        // table
        vm.selected = [];
        vm.options = {
            autoSelect: true,
            rowSelection: true
        };
        vm.query = {
            order: '',
            limit: 5,
            page: 1
        };

        vm.eventList = [];
        vm.eventListCount = 0;

        loadData();
        function loadData() {
            // do fetch all events
            var params = {round: 'active'}
            Event.query(params, function(response) {
                vm.eventList = response.data.list;
                vm.eventListCount = response.data.count;
            });
        }

        /**** delete ****/
        vm.status = '  ';
        vm.showConfirm = function(ev, id) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                    .title('Are You Sure?')
                    .textContent('You can not undo this action.')
                    .ariaLabel('You can not undo this action')
                    .targetEvent(ev)
                    .ok('Delete')
                    .cancel('Cancel');

            $mdDialog.show(confirm).then(function(response) {
                // ok
                // do delete
                var params = {
                    id: id
                }
                Event.delete(params, function(response) {
                    $log.log('response', response);
                    $state.go('event.admin', {}, {notify: true, reload: true});
                })
            }, function() {
                // cancel
                $log.log('canceled...');
            });
        };
    }
})();