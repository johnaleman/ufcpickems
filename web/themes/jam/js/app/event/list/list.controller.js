(function() {
    angular
        .module('app.event')
        .controller('EventListController', EventListController);

    function EventListController($log, Event) {
        var vm = this;
        vm.title = 'Event List';
        vm.eventList = [];

        loadData();
        function loadData() {
            var params = {round: 'active'}
            Event.query(params, function(response) {
                vm.eventList = response.data.list;
            });
        }
    }
})();