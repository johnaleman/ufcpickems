(function() {
    angular
        .module('app.event')
        .config(EventRouteConfig);

    function EventRouteConfig($stateProvider) {
        $stateProvider
            .state('event', {
                abstract: true,
                url: '/event',
                template: '<ui-view/>'
            })
            .state('event.list', {
                url: '',
                templateUrl:'/js/app/event/list/list.html',
                controller: 'EventListController',
                controllerAs: 'vm'
            })
            .state('event.item', {
                url: '/{id:int}', // only match integer
                templateUrl: '/js/app/event/item/item.html',
                controller: 'EventItemController',
                controllerAs: 'vm',
                resolve: {
                    Event: getEvent
                }
            })
            .state('event.create', {
                url: '/create',
                templateUrl: '/js/app/event/form/form.html',
                controller: 'EventFormController',
                controllerAs: 'vm',
                resolve: {
                    Event: newEvent
                }
            })
            .state('event.edit', {
                url: '/{id:int}/edit',
                templateUrl: '/js/app/event/form/form.html',
                controller: 'EventFormController',
                controllerAs: 'vm',
                resolve: {
                    Event: getEvent
                }
            })
            .state('event.admin', {
                url: '/admin',
                templateUrl: '/js/app/event/admin/admin.html',
                controller: 'EventAdminController',
                controllerAs: 'vm',
                resolve: {
                    User: function(User) {
                        return User.getUser();
                    }
                }
            })
    }

    function getEvent($stateParams, Event) {
        return Event.get({
            id: $stateParams.id
        }).$promise;
    }

    function newEvent(Event) {
        return new Event();
    }
})();