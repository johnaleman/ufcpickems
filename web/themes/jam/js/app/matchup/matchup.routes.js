(function() {
    angular.module('app.matchup').config(MatchupRouteConfig);

    function MatchupRouteConfig($stateProvider) {
        $stateProvider
            .state('matchup', {
                abstract: true,
                url: '/matchup',
                template: '<ui-view/>'
            })
            .state('matchup.list', {
                url: '',
                templateUrl: '/js/app/matchup/list/list.html',
                controller: 'MatchupListController',
                controllerAs: 'vm'
            })
            .state('matchup.item', {
                url: '/{id:int}', // only match integer
                templateUrl: '/js/app/matchup/item/item.html',
                controller: 'MatchupItemController',
                controllerAs: 'vm',
                resolve: {
                    Matchup: getMatchup
                }
            })
            .state('matchup.create', {
                url: '/create',
                templateUrl: '/js/app/matchup/form/form.html',
                controller: 'MatchupFormController',
                controllerAs: 'vm',
                resolve: {
                    Matchup: newMatchup,
                    FighterList: getFighterList,
                    EventList: getEventList
                }
            })
            .state('matchup.edit', {
                url: '/{id:int}/edit',
                templateUrl: '/js/app/matchup/form/form.html',
                controller: 'MatchupFormController',
                controllerAs: 'vm',
                resolve: {
                    Matchup: getMatchup,
                    FighterList: getFighterList,
                    EventList: getEventList
                }
            })
            .state('matchup.admin', {
                url: '/admin',
                templateUrl: '/js/app/matchup/admin/admin.html',
                controller: 'MatchupAdminController',
                controllerAs: 'vm',
                resolve: {
                    User: function(User) {
                        return User.getUser();
                    }
                }
            })
    }

    function getMatchup($stateParams, Matchup) {
        return Matchup.get({
            id: $stateParams.id
        }).$promise;
    }

    function newMatchup(Matchup) {
        return new Matchup();
    }

    function getFighterList(Fighter) {
        return Fighter.query().$promise;
    }

    function getEventList(Event) {
        return Event.query().$promise;
    }
})();