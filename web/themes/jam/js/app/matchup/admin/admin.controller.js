(function() {
    angular
        .module('app.matchup')
        .controller('MatchupAdminController', MatchupAdminController);

    function MatchupAdminController($log, $state, $mdDialog, User, Matchup) {
        var vm = this;
        vm.title = 'Matchup Admin';
        vm.user = User;

        // table
        vm.selected = [];
        vm.options = {
            autoSelect: true,
            rowSelection: true
        };
        vm.query = {
            order: '',
            limit: 5,
            page: 1
        };

        vm.matchupList = [];
        vm.matchupListCount = 0;

        loadData();
        function loadData() {
            var params = {};
            Matchup.query(params, function(response) {
                vm.matchupList = response.data.list;
                vm.matchupListCount = response.data.count;
            });
        }

        /**** delete ****/
        vm.status = '  ';
        vm.showConfirm = function(ev, id) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                    .title('Are You Sure?')
                    .textContent('You can not undo this action.')
                    .ariaLabel('You can not undo this action')
                    .targetEvent(ev)
                    .ok('Delete')
                    .cancel('Cancel');

            $mdDialog.show(confirm).then(function(response) {
                // ok
                // do delete
                var params = {
                    id: id
                }
                Matchup.delete(params, function(response) {
                    $log.log('response', response);
                    $state.go('matchup.admin', {}, {notify: true, reload: true});
                })
            }, function() {
                // cancel
                $log.log('canceled...');
            });
        };
    }
})();