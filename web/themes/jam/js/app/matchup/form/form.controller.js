(function() {
    angular
        .module('app.matchup')
        .controller('MatchupFormController', MatchupFormController);

    function MatchupFormController($log, $scope, $state, Matchup, FighterList, EventList, growl) {
        var vm = this;
        vm.matchup = Matchup;
        vm.matchup.hasTiebreaker = angular.isDefined(Matchup.id) ? vm.matchup.hasTiebreaker : 0;
        vm.title = angular.isDefined(Matchup.id) ? 'Update Matchup' : 'Create Matchup';
        vm.fighterList = FighterList.data.list;
        vm.filteredFighter1List = [];
        vm.filteredFighter2List = [];
        vm.eventList = EventList.data.list;
        vm.filteredEventList = [];
        var requiredList = ['fighter1Id', 'fighter2Id', 'eventId'];

        // form
        vm.form = {};
        vm.error = null;
        vm.updating = false;
        vm.save = save;

        initWatchers();

        function initWatchers() {
            $scope.$watchCollection('vm.filteredFighter1List', function(newVal, oldVal) {
                if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                    if(oldVal.length > 0) {
                        vm.matchup.fighter1Id = newVal[0].id;
                    } else {
                        // first load
                        // vm.matchup.fighter1Id = angular.isDefined(Matchup.id) ? vm.matchup.fighter1Id : newVal[0].id;
                    }
                }
            });

            $scope.$watchCollection('vm.filteredFighter2List', function(newVal, oldVal) {
                if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                    if(oldVal.length > 0) {
                        vm.matchup.fighter2Id = newVal[0].id;
                    } else {
                        // first load
                        // vm.matchup.fighter2Id = angular.isDefined(Matchup.id) ? vm.matchup.fighter2Id : newVal[0].id;
                    }
                }
            });

            $scope.$watchCollection('vm.filteredEventList', function(newVal, oldVal) {
                if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                    if(oldVal.length > 0) {
                        vm.matchup.eventId = newVal[0].id;
                    } else {
                        // first load
                        // vm.matchup.eventId = angular.isDefined(Matchup.id) ? vm.matchup.eventId : newVal[0].id;
                    }
                }
            });
        }

        // save portfolio document
        function save() {
            var errorList = [];

            // validate
            angular.forEach(requiredList, function(value) {
                // if no value
                if(vm.matchup.hasOwnProperty(value) === false) {
                    errorList.push(value);
                }
            });
            $log.log('errorList', errorList);

            // if no errors
            if(errorList.length == 0) {
                vm.updating = true;
                if (vm.matchup.id) {
                    vm.matchup.$update(successCallback, errorCallback).then(function(response) {
                        vm.updating = false;
                        vm.matchup = response.data;
                    });
                } else {
                    vm.matchup.$save(successCallback, errorCallback).then(function(response) {
                        vm.updating = false;
                        $state.go('matchup.create', {}, {notify: true, reload: true});
                    });
                }
            } else {
                // has error
                growl.addErrorMessage('Please fill out all fields');
            }

            function successCallback(fighter) {}
            function errorCallback(res) {
                vm.error = res.data.message;
            }
        }
    }
})();