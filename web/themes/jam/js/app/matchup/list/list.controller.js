(function() {
    angular
        .module('app.matchup')
        .controller('MatchupListController', MatchupListController);

    function MatchupListController($log, Matchup) {
        var vm = this;
        vm.title = 'Matchup List';
        vm.matchupList = [];

        loadData();
        function loadData() {
            Matchup.query(function(response) {
                vm.matchupList = response.data.list;
            })
        }
    }
})();
