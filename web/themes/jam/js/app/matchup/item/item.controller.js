(function() {
    angular
        .module('app.matchup')
        .controller('MatchupItemController', MatchupItemController);

    /*
     * Event Model
     *
     * id
     * fighter1Id
     * fighter2Id
     * eventId
     * hasTiebreaker
     */
    function MatchupItemController($log, Matchup) {
        var vm = this;
        vm.title = 'Matchup';
        vm.matchup = Matchup;
    }
})();