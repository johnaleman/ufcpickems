(function() {
    angular
        .module('app.result')
        .config(function($stateProvider) {
            $stateProvider
                .state('result', {
                    abstract: true,
                    url: '/result',
                    template: '<ui-view/>'
                })
                .state('result.list', {
                    url: '',
                    templateUrl: '/js/app/result/result.html',
                    controller: 'ResultController',
                    controllerAs: 'vm',
                    resolve: {
                        User: function($log, $state, User) {
                            return User.getUser();
                        }
                    }
                })
                .state('result.create', {
                    url: '/create',
                    templateUrl: '/js/app/result/form/form.html',
                    controller: 'ResultFormController',
                    controllerAs: 'vm',
                    resolve: {
                        User: function($log, $state, $stateParams, User) {
                            return User.getUser();
                        },
                        resultId: function() {
                            return false;
                        }
                    }
                })
                .state('result.update', {
                    url: '/update?id',
                    templateUrl: '/js/app/result/form/form.html',
                    controller: 'ResultFormController',
                    controllerAs: 'vm',
                    resolve: {
                        User: function(User) {
                            return User.getUser();
                        },
                        resultId: function($stateParams) {
                            return $stateParams.id;
                        }
                    }
                })
        })
})();