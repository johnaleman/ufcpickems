(function() {
    angular
        .module('app.result', [
            'ui.router',
            'app.result.admin',
            'app.result.form'
        ]);
})();
