(function() {
    angular.module('app.result')
            .controller('ResultController', ResultController);

    function ResultController($log, $timeout, APP, User, ResultFactory) {
        var vm = this;
        vm.title = 'Results';
        vm.user = User;
        vm.resultList = [];

        // table
        vm.options = {
            autoSelect: true,
            rowSelection: true
        };

        vm.isLoading = true;

        // load data
        ResultFactory.fetchRoundResults().then(function(response) {
            vm.resultList = response.data.resultList;

            $timeout(function() {
                vm.isLoading = false;
            }, APP.settings.PRELOADER_SPEED);
        });
    }
})();