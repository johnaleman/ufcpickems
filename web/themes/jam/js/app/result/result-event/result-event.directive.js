(function() {
    angular
        .module('app.result')
        .directive('resultEvent', resultEvent);

    function resultEvent() {
        var directive = {
            controller: controller,
            controllerAs: 'vm',
            templateUrl: '/js/app/result/result-event/result-event.html',
            restrict: 'E',
            scope: {
                eventNumber: '@',
                result: '=',
                user: '='
            }
        };
        return directive;

        function controller($log, $scope, Analytics, ResultFactory) {
            var vm = this;
            vm.display = false;
            vm.eventNumber = $scope.eventNumber;
            vm.user = $scope.user;
            vm.handleEventClick = handleEventClick;

            // table
            vm.options = {
                autoSelect: true,
                rowSelection: true
            };
            vm.resultList = [];
            vm.logItem = logItem;
            vm.logOrder = logOrder;
            vm.logPagination = logPagination;

            function getUserPicks() {
                var eventId = $scope.result.event.id;

                $scope.result['query'] = {
                    order: 'userName',
                    limit: 10,
                    page: 1
                };

                $scope.result['promise'] = ResultFactory.fetchUserPickResultsByEventId(eventId);
                $scope.result['promise'].then(function(response) {
                    $scope.result['query'] = {
                        order: response.data.userList.name,
                        limit: response.data.userList.length, // display all on one page
                        page: 1
                    };
                    $scope.result['userPicksData'] = response.data; // add property to result
                });

                return $scope.result['promise'];
            }

            function toggleDisplay() {
                vm.display = !vm.display;

                // trackEvent(category, action, label, value, non-interaction, custom dimension)
                var action = vm.display ? 'view' : 'hide';
                Analytics.trackEvent('result', action, $scope.result.event.name, 1, false, {
                    dimension1: vm.user.id,
                    dimension2: vm.user.firstName,
                    dimension3: vm.user.lastName,
                    dimension4: vm.user.role,
                    dimension5: vm.user.isActiveUser.toString()
                });
            }

            function logItem(item) {}
            function logOrder(order) {}
            function logPagination(page, limit) {}

            function handleEventClick() {
                toggleDisplay();
                getUserPicks();
            }
        }
    }
})();