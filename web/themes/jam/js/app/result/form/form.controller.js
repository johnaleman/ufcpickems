(function() {
    angular
        .module('app.result.form')
        .controller('ResultFormController', ResultFormController);

    function ResultFormController($log, $scope, $timeout, APP, resultId, ResultFactory, growl) {
        var vm = this;
        vm.title = resultId === false ? 'Create Result' : 'Update Result';
        vm.status = false;
        vm.result = {selectedFighterID: 0, selectedEventID: 0, selectedRoundID: 0, selectedFinishTypeID: 0};
        vm.handleSubmit = handleSubmit;
        vm.fighterList = [];
        vm.eventList = [];
        vm.roundList = [];
        vm.finishTypeList = [];
        vm.filteredFighterList = [];
        vm.filteredEventList = [];
        vm.filteredRoundList = [];
        vm.filteredFinishTypeList = [];
        vm.isLoading = true;
        vm.isSaving = false;

        // set round list
        vm.roundList = [
            { value: 1, name: '1' },
            { value: 2, name: '2' },
            { value: 3, name: '3' },
            { value: 4, name: '4' },
            { value: 5, name: '5' }
        ];

        // set finish method list
        vm.finishMethodList = [
            { value: 1, name: 'KO/TKO' },
            { value: 2, name: 'SUB' },
            { value: 3, name: 'DECISION' }
        ];

        var resultResolve = false;

        if (resultId !== false) {
            // load data
            ResultFactory.fetchResult(resultId).then(function(response) {
                resultResolve = response;

                vm.result.selectedRoundID = resultResolve.data.roundId;
                vm.result.selectedFinishTypeID = resultResolve.data.finishTypeId;

                ResultFactory.fetchResultView().then(onCreateResultsSuccess);
            });
        } else {
            ResultFactory.fetchResultView().then(onCreateResultsSuccess);
        }

        function onCreateResultsSuccess(response) {
            initFighterList(response);
            initEvntList(response);

            $timeout(function() {
                vm.isLoading = false;
            }, APP.settings.PRELOADER_SPEED);
        }

        function initFighterList(response) {
            vm.fighterList = response.data.fighterList;

            if (resultResolve !== false) {
                vm.result.selectedFighterID = resultResolve.data.fighterId;
            }

            // listen for filtered fighter list change
            $scope.$watchCollection('vm.filteredFighterList', function(newVal, oldVal) {
                if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                    if(oldVal == '' || oldVal.length == 0) {
                        if (resultResolve !== false) {
                            vm.result.selectedFighterID = resultResolve.data.fighterId;
                        }
                    } else {
                        vm.result.selectedFighterID = newVal[0].id;
                    }
                }
            });
        }

        function initEvntList(response) {
            vm.eventList = response.data.eventList;

            if (resultResolve !== false) {
                vm.result.selectedEventID = resultResolve.data.eventId;
            }

            // listen for filtered fighter list change
            $scope.$watchCollection('vm.filteredEventList', function(newVal, oldVal) {
                if (typeof newVal !== 'undefined' && newVal != oldVal && newVal.length > 0 && newVal[0].hasOwnProperty('id')) {
                    if(oldVal == '' || oldVal.length == 0) {
                        if (resultResolve !== false) {
                            vm.result.selectedEventID = resultResolve.data.eventId;
                        }
                    } else {
                        vm.result.selectedEventID = newVal[0].id;
                    }
                }
            });
        }

        function handleSubmit(model) {
            if(vm.isSaving) {
                return;
            }

            var model = JSON.stringify({data: model});
            var errorList = [];
            var isValid = false;

            // validate
            angular.forEach(vm.result, function(value, key) {
                // if no value
                if(value <= 0) {
                    errorList.push(key);
                }
            });
            $log.log('errorList', errorList);
            // TODO: Use error list to dislay error message in view.

            // if no errors
           if(errorList.length == 0) {
               // check if Result resource exists
               if(resultResolve === false) {
                   vm.isSaving = true;

                   // create
                   $promise = ResultFactory.submitResult(model);
                   $promise.then(function(data) {
                       if(data.status) {
                           // clear form
                           angular.forEach(vm.result, function(value, key) {
                               vm.result[key] = 0;
                           })
                       }

                       vm.isSaving = false;
                   })
               } else {
                   vm.isSaving = false;

                   // update
                   ResultFactory.updateResult(resultResolve.data.id, model);
               }
           } else {
               vm.isSaving = false;

               // has error
               growl.addErrorMessage('Please fill out all fields');
           }
        }
    }
})();