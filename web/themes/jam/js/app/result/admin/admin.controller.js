(function() {
    angular
        .module('app.result.admin')
        .controller('ResultAdminController', ResultAdminController);

    function ResultAdminController($log, $mdDialog, User, ResultFactory) {
        var vm = this;
        vm.title = 'Results Admin';
        vm.user = User;
        vm.selected = [];
        vm.options = {
            autoSelect: true,
            rowSelection: true
        };
        vm.query = {
            order: 'name',
            limit: 5,
            page: 1
        };
        vm.resultList = [];
        vm.resultListCount = 0;

        loadStuff();
        function loadStuff() {
            vm.promise = ResultFactory.fetchResults(vm.query).then(function(response) {
                if(response.data) {
                    vm.resultList = response.data;
                    vm.resultListCount = response.data.length;
                }
            });
        }

        /**** delete ****/
        vm.status = '  ';
        vm.showConfirm = function(ev, id) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                    .title('Are You Sure?')
                    .textContent('You can not undo this action.')
                    .ariaLabel('You can not undo this action')
                    .targetEvent(ev)
                    .ok('Delete')
                    .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                // ok
                ResultFactory.deleteResultById(id).then(function(response) {
                    loadStuff();

                    // use when managing cache manually
                    // vm.resultList = response.resultList;
                    // vm.resultListCount = response.resultList.length;
                });
            }, function() {
                // cancel
                $log.log('canceled...');
            });
        };

        vm.logItem = function (item) {};
        vm.logOrder = function (order) {};
        vm.logPagination = function (page, limit) {}
    }
})();