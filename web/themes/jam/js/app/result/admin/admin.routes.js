(function() {
    angular
        .module('app.result.admin')
        .config(routesConfig);

    function routesConfig($stateProvider) {
        $stateProvider
            .state('result.admin', {
                url: '/admin',
                templateUrl: '/js/app/result/admin/admin.html',
                controller: 'ResultAdminController',
                controllerAs: 'vm',
                resolve: {
                    User: function(User) {
                        return User.getUser();
                    }
                }
            })
    }
})();