(function() {
    angular
        .module('app.result.admin')
        .factory('ResultFactory', ResultFactory);

    function ResultFactory($log, $http, APP, CoreCache) {
        /*
         * fetches all events for the open round
         */
        function fetchRoundResults() {
            return $http.get(APP.webroot + '/result/', {cache: false})
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                return response.data;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        function fetchResults(query) {
            return $http.get(APP.webroot + '/result/admin', {cache: false})
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                return response.data;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        function fetchResult(id) {
            return $http.get(APP.webroot + '/result/update?id=' + id, {cache: false})
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                return response.data;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        function fetchResultView() {
            return $http.get(APP.webroot + '/result/create', {cache: true})
                    .then(fetchCreateReultsViewComplete)
                    .catch(fetchCreateReultsViewFailed);

            function fetchCreateReultsViewComplete(response) {
                return response.data;
            }

            function fetchCreateReultsViewFailed(error) {
                $log.error('XHR Failed for fetchMatchups.' + error.data);
            }
        }

        function submitResult(data) {
            return $http.post(APP.webroot + '/result/create', data)
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                response = response.data;

                var cacheData = CoreCache.getData(APP.webroot + '/result/admin');
                if(cacheData) {
                    // update cache
                    cacheData.data.push(response.data);
                    CoreCache.setData(APP.webroot + '/result/admin', cacheData);
                }

                return response;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        function updateResult(id, data) {
            return $http.put(APP.webroot + '/result/update?id=' + id, data)
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                response = response.data.data;
                CoreCache.updateData(APP.webroot + '/result/admin', response);
                CoreCache.updateData(APP.webroot + '/result/update?id=' + id, response);

                return response;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        function deleteResultById(id) {
            return $http.post(APP.webroot + '/result/delete?id=' + id)
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                //response = response.data;
                //response['resultList'] = CoreCache.removeItem(APP.webroot + '/result/admin', response.data).data;
                return response;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        function fetchUserPickResultsByEventId(eventId) {
            return $http.get(APP.webroot + '/pick/getAllPicksByEvent?eventId=' + eventId, {cache: false})
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                return response.data;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        return {
            fetchRoundResults: fetchRoundResults,
            fetchResultView: fetchResultView,
            fetchResults: fetchResults,
            fetchResult: fetchResult,
            submitResult: submitResult,
            updateResult: updateResult,
            deleteResultById: deleteResultById,
            fetchUserPickResultsByEventId: fetchUserPickResultsByEventId
        };
    }
})();