(function() {
    angular
        .module('app.standings')
        .factory('StandingsFactory', StandingsFactory);

    function StandingsFactory($log, $http, APP, CoreCache) {
        function fetchStandings() {
            return $http.get(APP.webroot + '/result/standings', {cache: false})
                    .then(fetchComplete)
                    .catch(fetchFailed);

            function fetchComplete(response) {
                return response.data;
            }

            function fetchFailed(error) {
                $log.error('XHR Failed for submit result.' + error.data);
                return false;
            }
        }

        return {
            fetchStandings: fetchStandings
        }
    }
})();