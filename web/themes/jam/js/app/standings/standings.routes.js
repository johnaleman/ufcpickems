(function() {
    angular
        .module('app.standings')
        .config(standingsRouteConfig);

    function standingsRouteConfig($stateProvider) {
        $stateProvider
            .state('standings', {
                abstract: true,
                url: '/standings',
                template: '<ui-view/>'
            })
            .state('standings.list', {
                url: '',
                templateUrl: '/js/app/standings/standings.html',
                controller: 'StandingsController',
                controllerAs: 'vm'
            })
    }
})();