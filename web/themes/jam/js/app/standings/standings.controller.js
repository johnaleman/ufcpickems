(function() {
    angular
        .module('app.standings')
        .controller('StandingsController', StandingsController);

    function StandingsController($log, $timeout, APP, StandingsFactory) {
        var vm = this;
        vm.title = 'Standings';

        // table properties
        vm.options = {
            autoSelect: true,
            rowSelection: true
        };
        vm.query = {
            order: '', // response will be ordered
            limit: 0,
            page: 1
        };
        vm.resultList = [];
        vm.resultListCount = 0;
        vm.logItem = logItem;
        vm.logOrder = logOrder;
        vm.logPagination = logPagination;

        vm.isLoading = true;

        // load data
        StandingsFactory.fetchStandings().then(function(response) {
            $timeout(function() {
                vm.resultList = response.data;
                vm.resultListCount = response.count;
                vm.query.limit = response.count;

                vm.isLoading = false;
            }, APP.settings.PRELOADER_SPEED);
        });

        function logItem(item) {}
        function logOrder(order) {}
        function logPagination(page, limit) {}
    }
})();