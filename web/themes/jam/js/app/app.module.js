(function() {
    // initialize
    angular.element(document).ready(function() {
        // fixing facebook bug with redirect
        if (window.location.hash === '#_=_') window.location.hash = '#!';

        // load user
        angular.bootstrap(document.getElementById('loadUser'), ['app.loadUser']);
    });

    angular
        .module('app.loadUser', [])
        .run(preAppRun);

    function preAppRun($log, $http, $q) {
        getUserStatus().then(function(user) {
            // create a one value module that stores the user data
            angular.module('app.userModel', []).value('userModel', user);

            fetchSettings().then(function(settingsO) {
                angular.module('app.settingsModel', []).value('SettingsModel', settingsO);

                // bootstrap our main module, which is dependent on user data
                angular.bootstrap(document, ['app']);
            })
        });

        function getUserStatus() {
            var deferred = $q.defer();

            // get the user data from our API
            $http.get(getWebroot().origin + '/user/status').then(function(response) {
                var user = response.data.data;
                deferred.resolve(user);
            });

            return deferred.promise;
        }

        function fetchSettings() {
            var webroot = APP().webroot;
            var path = '/api/setting/1';
            var url = webroot + path;

            var deferred = $q.defer();

            $http.get(url).then(function(response) {
                // set _settingO
                var _settingsO = response.data;
                deferred.resolve(_settingsO);
            }, function error(resposne) {
                // handle error
                deferred.reject(resposne.message);
            });

            return deferred.promise;
        }
    }

    angular
        .module('app', [
            'app.userModel', // bootstrap user model
            'app.settingsModel', // boostrap settings model
            'app.user', // user module
            'app.core', // 3rd party dependencies
            'app.shared',
            'app.auto',
            'app.notify',
            'app.home',
            'app.makeyourpicks',
            'app.result',
            'app.standings',
            'app.event',
            'app.fighter',
            'app.matchup'
        ])
        .constant({'APP': APP()})
        .config(appConfig)
        .run(appRun);

    function APP() {
        return {
            date: '20170722',
            version: '2.2.4',
            webroot: getWebroot().origin,
            settings: {
                PRELOADER_SPEED: 100
            }
        }
    }

    function appConfig($locationProvider, $mdThemingProvider, AnalyticsProvider, APP) {
        $locationProvider.hashPrefix('!');

        // setup theme
        $mdThemingProvider.theme('default')
            .primaryPalette('deep-purple')
            .accentPalette('orange')
            .warnPalette('deep-orange')
            .backgroundPalette('grey');

        // google analytics
        AnalyticsProvider.logAllCalls(true);
        AnalyticsProvider.setAccount('UA-41005632-4');
        AnalyticsProvider.trackPages(true);

        // RegEx to scrub location before sending to analytics.
        // Internally replaces all matching segments with an empty string.
        AnalyticsProvider.setRemoveRegExp(/\/\d+?$/);

        // set domain name. use the string 'none' for testing on localhost.
        var domain = (APP.webroot.indexOf('dev') > -1) ? 'none' : 'ufcpickems.com';
        AnalyticsProvider.setDomainName(domain);

        // Calling this method will enable debugging mode for Universal Analytics. Supplying a truthy value for the
        // optional parameter will further enable trace debugging for Universal Analytics. More information on this
        // is available here: https://developers.google.com/analytics/devguides/collection/analyticsjs/debugging.
        AnalyticsProvider.enterDebugMode(false);
    }

    function appRun(userModel, User, Analytics) {
        User.setUser(userModel);

        if(User.getUser().isGuest) {
            // trackEvent(category, action, label, value, non-interaction, custom dimension)
            Analytics.trackEvent('app', 'view', Analytics.getUrl(), 1, true, {dimension1: '-1'});
        } else {
            // trackEvent(category, action, label, value, non-interaction, custom dimension)
            Analytics.trackEvent('app', 'view', Analytics.getUrl(), 1, true, {
                dimension1: User.getUser().id,
                dimension2: User.getUser().firstName,
                dimension3: User.getUser().lastName,
                dimension4: User.getUser().role,
                dimension5: User.getUser().isActiveUser.toString()
            });
        }
    }

    function getWebroot() {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }

        return {
            origin: window.location.origin,
            pathName: document.location.pathname,
            webroot: window.location.origin + document.location.pathname
        }
    }
})();
