(function() {
    angular
        .module('app.core')
        .factory('CoreCache', CoreCache);

    function CoreCache($log, $cacheFactory) {
        var ACTION = {
            UPDATE: 'update',
            DELETE: 'delete'
        }

//        return $cacheFactory('myData');
        var $httpDefaultCache = $cacheFactory.get('$http');

        // TODO: refactor to just return the cacheObj
        var manipulateData = function(key, newData, action) {
            if(typeof $httpDefaultCache.get(key) !== 'undefined') {
                var cacheData = this.getData(key);
                if(cacheData) {
                    var cacheObj;
                    if (angular.isArray(cacheData.data)) {
                        for (var i = 0; i < cacheData.data.length; i++) {
                            cacheObj = cacheData.data[i];
                            if(newData.id === cacheObj.id) {
                                if(action == ACTION.UPDATE) {
                                    angular.extend(cacheObj, newData); // update cache cacheObj
                                } else if (action == ACTION.DELETE) {
                                    cacheData.data.splice(i, 1);
                                    this.setData(key, cacheData);
                                }
                            }
                        }
                    } else {
                        cacheObj = cacheData.data;
                        if(newData.id === cacheObj.id) {
                            if(action == ACTION.UPDATE) {
                                angular.extend(cacheObj, newData); // update cache cacheObj
                            }
                        }
                    }
                }

                return $httpDefaultCache.put(key, cacheData);
            } else {
                return false;
            }
        }

        return {
            getData: function(key) {
                if(typeof $httpDefaultCache.get(key) !== 'undefined') {
                    // result can be angular cache string or data array
                    if(typeof $httpDefaultCache.get(key)['data'] !== 'undefined') {
                        return {data: $httpDefaultCache.get(key)['data']};
                    } else {
                        // returned string
                        return JSON.parse($httpDefaultCache.get(key)[1]);
                    }
                } else {
                    return false;
                }
            },

            removeItem: function(key, item) {
                return manipulateData.apply(this, [key, item, 'delete']);
            },

            /*
             * Update a cache item
             */
            updateData: function(key, newData) {
                if(typeof $httpDefaultCache.get(key) !== 'undefined') {
                    var cacheData = this.getData(key);
                    if(cacheData) {
                        var cacheObj;
                        if (angular.isArray(cacheData.data)) {
                            for (var i = 0; i < cacheData.data.length; i++) {
                                cacheObj = cacheData.data[i];
                                if(newData.id === cacheObj.id) {
                                    angular.extend(cacheObj, newData); // update cache cacheObj
                                }
                            }
                        } else {
                            cacheObj = cacheData.data;
                            if(newData.id === cacheObj.id) {
                                angular.extend(cacheObj, newData); // update cache cacheObj
                            }
                        }
                    }

                    return $httpDefaultCache.put(key, cacheData);
                } else {
                    return false;
                }
            },

            /*
             * Set/update the cache list
             */
            setData: function(key, value) {
                if(typeof $httpDefaultCache.get(key) !== 'undefined') {
                    return $httpDefaultCache.put(key, value);
                } else {
                    return false;
                }
            },

            invalidate: function (key) {
                $httpDefaultCache.remove(key);
            }
        }
    }
})();