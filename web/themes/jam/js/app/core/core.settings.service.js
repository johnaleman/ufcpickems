(function() {
    angular.module('app.core').service('SettingsService', SettingService);

    function SettingService($log, $http, $q, APP) {
        $log.log('APP', APP);

        var name = 'SettingService';
        var webroot = APP.webroot;
        var path = '/api/setting/1';
        var url = webroot + path;

        var _settingO = {};

        function fetchSettings() {
            var deferred = $q.defer();

            $http.get(url).then(function(response) {
                // set _settingO
                _settingO = response.data;
                deferred.resolve(_settingO);
            }, function error(resposne) {
                // handle error
                deferred.reject(resposne.message);
            });

            return deferred.promise;
        }

        return {
            getName: function() {
                return name;
            },

            fetchSettings: fetchSettings,

            getSettingO: function() {
                return _settingO;
            }
        }
    }
})();