(function() {
    angular
        .module('app.core', [
            'app.user',
            'ui.router',
            'ngResource',
            'ngSanitize',
            'ngAnimate',
            'ngMaterial',
            'ngMessages',
            'ngMdIcons',
            'angular-google-analytics',
            'ui.bootstrap',
            'md.data.table',
            'angular-growl',
            'angular-click-outside'
        ])
        .config(config)
        .run(run);

    function config($httpProvider, growlProvider) {
        growlProvider.onlyUniqueMessages(false);
        growlProvider.globalTimeToLive(5000);
        growlProvider.globalEnableHtml(true);

        $httpProvider.interceptors.push('httpInterceptor');
    }

    function run(statePermissionService) {}
})();
