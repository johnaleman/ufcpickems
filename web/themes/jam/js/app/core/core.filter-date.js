(function() {
    'use strict';

    angular
        .module('app.core')
        .filter('filterDate', filterDate);

    function filterDate() {
        return filterDateFilter;

        function filterDateFilter(input, format) {
            if(angular.isUndefined(input) || input === '') {
                return false;
            }

            var d = parseDate(input); //credit: http://stackoverflow.com/questions/4622732/new-date-using-javascript-in-safari

            var dayOfWeek = getDayOfWeek(d.getDay());
            var month = getMonth(d.getMonth());
            var shortMonth = getShortMonth(d.getMonth());
            var day = d.getDate();

            function getDayOfWeek(index) {
                var weekday = new Array(7);
                weekday[0] = "Sunday";
                weekday[1] = "Monday";
                weekday[2] = "Tuesday";
                weekday[3] = "Wednesday";
                weekday[4] = "Thursday";
                weekday[5] = "Friday";
                weekday[6] = "Saturday";

                return weekday[index];
            }

            function getMonth(index) {
                var month = new Array(12);
                month[0] = "January";
                month[1] = "February";
                month[2] = "March";
                month[3] = "April";
                month[4] = "May";
                month[5] = "June";
                month[6] = "July";
                month[7] = "August";
                month[8] = "September";
                month[9] = "October";
                month[10] = "November";
                month[11] = "December";

                return month[index];
            }

            function getShortMonth(index) {
                var month = new Array(12);
                month[0] = "Jan";
                month[1] = "Feb";
                month[2] = "March";
                month[3] = "April";
                month[4] = "May";
                month[5] = "June";
                month[6] = "July";
                month[7] = "Aug";
                month[8] = "Sept";
                month[9] = "Oct";
                month[10] = "Nov";
                month[11] = "Dec";

                return month[index];
            }

            function parseDate(input) {
                if(angular.isDefined(input) === false || input === '') {
                    return;
                }

                var parts = input.match(/(\d+)/g);
                return new Date(parts[0], parts[1]-1, parts[2]);
            }

            return dayOfWeek + ' ' + shortMonth + ' ' + day;
        }
    }
})();
