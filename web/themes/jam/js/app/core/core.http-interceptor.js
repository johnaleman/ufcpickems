(function() {
    angular
        .module('app.core')
        .factory('httpInterceptor', httpInterceptor);

    function httpInterceptor($q, growl) {
        var deferred = $q.defer();

        // optional methods
        return {
            'request': function(config) {
                // do something on success
                return config;
            },

            'requestError': function(rejection) {
                // do something on error
            },

            'response': function(response) {
                // do something on success
                if(response.data.status === true && response.data.message !== '') {
                    growl.addSuccessMessage(response.data.message);
                }

                return response;
            },

            'responseError': function(rejection) {
                growl.addErrorMessage(rejection.data.message);
                deferred.reject(rejection.data.message);

                return deferred.promise;
            }
        };
    }
})();