(function() {
    angular
        .module('app.core')
        .config(appCoreConfig);

    function appCoreConfig($urlRouterProvider) {
        $urlRouterProvider.otherwise('/home'); // Redirect to home view when route not found
    }
})();
