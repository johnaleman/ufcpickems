(function() {
    angular
        .module('app.core')
        .controller('CoreController', CoreController);

    function CoreController($log, $scope, $interval, $mdSidenav, $mdDialog, $state, APP, User, growl) {
        //$log.log('[register controller]: CoreController');
        $scope.APP = APP;
        $scope.user = User.getUser();
        $scope.version = APP.date + '_' + APP.version;
        $scope.shortVersion = APP.version;
        $log.log("version: " + $scope.version);

        $scope.menu = [{
            link: '',
            title: 'Home',
            icon: 'home',
            state: 'home',
            roles: ['*'],
            rules: {},
            canView: false
        }, {
            link: '',
            title: 'Signup',
            icon: '',
            state: 'user.create',
            roles: ['*'],
            rules: {isGuest: true},
            canView: false
        },{
            link: '',
            title: 'Make Your Picks',
            icon: 'tv',
            state: 'makeyourpicks',
            roles: [1, 2],
            rules: {isGuest: false},
            canView: false
        },{
            link: '',
            title: 'Results',
            icon: '',
            state: 'result.list',
            roles: [1, 2],
            rules: {isGuest: false},
            canView: false
        },{
            link: '',
            title: 'Standings',
            icon: '',
            state: 'standings.list',
            roles: [1, 2],
            rules: {isGuest: false},
            canView: false
        },{
            link: '',
            title: 'Update Profile',
            icon: '',
            state: 'user.edit',
            roles: ['*'],
            rules: {isGuest: false},
            canView: false
        },{
            link: '',
            title: 'Admin',
            type: 'dropdown',
            subMenu: [{
                title: 'Auto Create',
                icon: '',
                state: 'auto.createAll',
                roles: [1],
                rules: {isGuest: false},
                canView: false
            }, {
                title: 'Notify',
                icon: '',
                state: 'notify',
                roles: [1],
                rules: {isGuest: false},
                canView: false
            },{
                title: 'Results',
                icon: '',
                state: 'result.admin',
                roles: [1],
                rules: {isGuest: false},
                canView: false
            },{
                title: 'Events',
                icon: '',
                state: 'event.admin',
                roles: [1],
                rules: {isGuest: false},
                canView: false
            },{
                title: 'Fighters',
                icon: '',
                state: 'fighter.admin',
                roles: [1],
                rules: {isGuest: false},
                canView: false
            }, {
                title: 'Matchups',
                icon: '',
                state: 'matchup.admin',
                roles: [1],
                rules: {isGuest: false},
                canView: false
            }, {
                title: 'Users',
                icon: '',
                state: 'user.admin',
                roles: [1],
                rules: {isGuest: false},
                canView: false
            }],
            isActive: false,
            icon: '',
            state: false,
            roles: [1],
            rules: {isGuest: false},
            canView: false
        },{
            link: '',
            title: 'Login',
            icon: '',
            state: 'login',
            roles: ['*'],
            rules: {isGuest: true},
            canView: false
        },{
            link: '',
            title: 'Logout',
            icon: '',
            state: 'logout',
            roles: ['*'],
            rules: {isGuest: false},
            canView: false
        }];

        upateNavBar();

        var udpateMenu = function() {
            // set permissions
            angular.forEach($scope.menu, function(menuItem) {
                var canView = false;
                var state = menuItem.state;
                var rules = menuItem.rules;

                if (menuItem.roles == '*') {
                    // TODO: add role for guest (-1) check against that also
                    canView = true;

                    // if there are guest rules
                    if (Object.keys(rules).length > 0) {
                        canView = $scope.user.isGuest == rules.isGuest;
                    }
                } else {
                    // check roles against user
                    var keepGoing = true;
                    angular.forEach(menuItem.roles, function(role) {
                        if (keepGoing) {
                            if (role == $scope.user.role && $scope.user.isActiveUser) {
                                canView = true;
                                keepGoing = false;
                            }
                        }
                    });
                }

                // update menu item
                //$log.log(state + ' - ' +  'canView: ' + canView);
                menuItem.canView = canView;
            });

            onMenuComplete();
        };

        var onMenuComplete = function() {
            // testMenuTrackAnimation();
        }

        function testMenuTrackAnimation() {
            var delay = 2000;
            var constantClass = 'animate';
            var menuArr = ['home', 'make-your-picks', 'results', 'standings', 'update-profile', 'admin', 'logout'];

            var length = menuArr.length;
            var i = 0;
            $interval(function() {
                // debug
                var targetIdx = i % length;
                $log.log("targetIdx: " + targetIdx);

                var $menuTracker = angular.element('#menu-tracker');
                var menuId = menuArr[targetIdx];

                $menuTracker.attr('class', ''); // clear class
                $menuTracker.addClass(constantClass + ' ' + menuId);

                i++
            }, delay);
        }

        $scope.$on('user:updated', function(event, data) {
            // you could inspect the data to see if what you care about changed, or just update your own scope
            $scope.user = data;
            udpateMenu();
        });

        $scope.toggleSidenav = function(itemType, menuId) {
            if(itemType !== 'dropdown') {
                $mdSidenav(menuId).toggle();
            }
        };

        $scope.openChartFeatures = function(ev) {
            $mdDialog.show({
                templateUrl: '',
                controller: ''
            })
        };

        $scope.navigate = function(routeName) {
            if(routeName === false) {
                return;
            }

            if(routeName == 'user.edit') {
                var userId = $scope.user.id;
                $state.go(routeName, {id: userId});
            } else {
                $state.go(routeName);
            }
        };

        $scope.closeThis = function (item) {
            if(item.isActive === true) {
                item.isActive = false;
            }
        };

        function upateNavBar() {
            var $toolbar = angular.element('md-toolbar:not(.md-menu-toolbar)');

            // update toolbar background for dev
            if(APP.webroot.indexOf('dev') > -1) {
                // $toolbar.css('backgroundColor', 'rgb(112, 36, 127)');
                $toolbar.css('backgroundColor', 'rgb(0, 188, 212)');

            }
        }

        udpateMenu();
    }
})();

