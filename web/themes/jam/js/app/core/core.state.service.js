(function() {
    angular
        .module('app.core')
        .service('statePermissionService', statePermissionService);

    // TODO: refactor to pass persmissions from $stateParams
    function statePermissionService($rootScope, $log, $state, Analytics, User) {
        // $log.log('statePermissionService');
        // enumerate routes that don't need authentication
        var routesThatDontRequireAuth = ['home', 'user.create', 'login', 'logout'];

        // users that are guests (user = guest) will be redirected home
        var userPermissionRequired = ['makeyourpicks','result.list','standings.list'];

        // users who might have permission to view the state
        var selfPermissionRequired = ['user.edit'];

        $rootScope.$on('$stateChangeStart', stateChangeStart);

        function stateChangeStart(event, toState, toParams, fromState, fromParams) {
            var toStateName = toState.name;
            var toParams = toParams;
            var fromStateName = fromState.name;
            var fromParams = fromParams;

            var isRedirecting = false;
            var gotoState = toStateName;

            var canGuestView = checkIfGuestCanView(toStateName);
            var canAuthUserView = checkIfAuthCanView(toStateName);
            var canSelfUserView = checkIfSelfCanView(toStateName, toParams);

            if(canGuestView === false && canAuthUserView == false && canSelfUserView == false) {
                isRedirecting = true;
                gotoState = 'home';
            }

            if(isRedirecting) {
                event.preventDefault();
                $state.go(gotoState);
            }

            if(User.getUser().isGuest) {
                // trackEvent(category, action, label, value, non-interaction, custom dimension)
                Analytics.trackEvent('page', 'view', gotoState, 1, true, {dimension1: '-1'});
            } else {
                // trackEvent(category, action, label, value, non-interaction, custom dimension)
                Analytics.trackEvent('page', 'view', gotoState, 1, false, {
                    dimension1: User.getUser().id,
                    dimension2: User.getUser().firstName,
                    dimension3: User.getUser().lastName,
                    dimension4: User.getUser().role,
                    dimension5: User.getUser().isActiveUser.toString()
                });
            }

            updateMenuTracker(gotoState, fromStateName);
        }

        function updateMenuTracker(toStateName, fromStateName) {
            var className = toStateName;

            switch(toStateName) {
                case 'home':
                case 'logout':
                case 'login':
                    // no change
                    break;
                case 'makeyourpicks':
                    className = 'make-your-picks';
                    break;

                case 'result.list':
                    className = 'results';
                    break;

                case 'standings.list':
                    className = 'standings';
                    break;

                case 'user.edit':
                    className = 'update-profile';
                    break;

                case 'user.create':
                    className = 'signup';
                    break;

                case 'auto.createAll':
                case 'notify':
                case 'result.admin':
                case 'event.admin':
                case 'fighter.admin':
                case 'matchup.admin':
                case 'user.admin':
                case 'event.create':
                case 'fighter.create':
                case 'result.create':
                case 'matchup.create':
                case 'result.update':
                case 'event.edit':
                case 'fighter.edit':
                case 'matchup.edit':
                    className = 'admin';
                    break;

                default:
                    $log.log('No case for: ' + toStateName);
                    break;
            }

            if(toStateName == 'user.create' && fromStateName == 'user.admin') {
                className = 'admin';
            }

            handleMenuItemClick(className);
        }

        function handleMenuItemClick(itemTitle) {
            var title = itemTitle.toLowerCase().replace(/ /gi, '-');
            goto(title);

            function goto(title) {
                var constantClass = 'animate';
                var $menuTracker = angular.element('#menu-tracker');
                $menuTracker.attr('class', '');
                $menuTracker.addClass('');
                $menuTracker.addClass(constantClass + ' ' + title);
            }
        }

        // check if current location matches route
        var checkIfGuestCanView = function (toStateName) {
            var matchFound = false;

            var keepGoing = true;
            angular.forEach(routesThatDontRequireAuth, function(value) {
                if(keepGoing) {
                    var indexOfVal = value.indexOf(toStateName);

                    if(indexOfVal > -1) {
                        matchFound = true;
                        keepGoing = false;
                    }
                }
            });

            return matchFound;
        };

        // check if current location matches route
        var checkIfAuthCanView = function (toStateName) {
            var value = false;

            // user vars
            var user = User.getUser();
            var isGuest = user.isGuest;
            var role = user.role;
            var userId = user.id;

            if(angular.isUndefined(role)){
                value = false;
            } else if(role == 1) {
                value = true;
            } else {
                // check
                var keepGoing = true;
                angular.forEach(userPermissionRequired, function(stateName) {
                    if(keepGoing) {
                        var indexOfVal = stateName.indexOf(toStateName);

                        if(indexOfVal > -1 && role == 2) {
                            value = true;
                            keepGoing = false;
                        }
                    }
                });
            }

            return value;
        };

        // check if user is the owner of the resource
        var checkIfSelfCanView = function (toStateName, toParams) {
            var value = false;

            // user vars
            var user = User.getUser();
            var isGuest = user.isGuest;
            var role = user.role;
            var userId = user.id;

            var checkUserId = toParams.id;

            if(angular.isUndefined(role)){
                value = false;
            } else if(role == 1) {
                value = true;
            } else {
                // check
                var keepGoing = true;
                angular.forEach(selfPermissionRequired, function(stateName) {
                    if(keepGoing) {
                        var indexOfVal = stateName.indexOf(toStateName);

                        if(indexOfVal > -1 && userId == checkUserId) {
                            value = true;
                            keepGoing = false;
                        }
                    }
                });
            }

            return value;
        };
    }
})();