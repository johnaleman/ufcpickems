/**
 * Created by johnaleman on 10/12/14.
 */

var FM = (function ($) {
	function getData(){
		var datastring = '';
		var url = 'http://liveapi.fightmetric.com/V1/681/Fnt.json';

		$.ajax({
			url: url,
			dataType: 'json',
			type: 'GET',
			data: datastring,
			success: function (response, status) {
				helpers.log('AJAX success: ' + response);
				handleDataSuccess(response);
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				helpers.log('AJAX error:' + textStatus);
			}
		});
	}

	function handleDataSuccess(data){
		window.debug_data = data;
		var city = data.FMLiveFeed.City;
		var country = data.FMLiveFeed.Country;
		var fights = data.FMLiveFeed.Fights;

		helpers.log('location: '+city+','+country);
		$(fights).each(function(index, fight){
			var fight = fight;
			var fightId = fight.FightID;
			var fightersArr = fight.Fighters;
			var endingRoundNum = fight.EndingRoundNum;
			var weightClassName = fight.WeightClassName;
			var method = fight.Method;

			helpers.log('');
			helpers.log('===================');
			helpers.log('fightId: ' + fightId);
			helpers.log('weight class: ' + weightClassName);
			$(fightersArr).each(function(index, fighter){
				var firstName = fighter.FirstName;
				var lastName= fighter.LastName;
				helpers.log(firstName + ' ' + lastName);
				//helpers.log('vs');
			});

			helpers.log('round: ' + endingRoundNum + ' - ' + method);
		})
	};

	fm = {
		property: 2,
		init: function(){
			helpers.log('fm init');
			getData();
		}
	};

	return fm;
}(jQuery));
FM.init();