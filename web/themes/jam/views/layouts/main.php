<?php
	/*
	 * technology stack
	 *
	 * yii: 1.1.13
	 * angular: 1.4.6
	 */
?>

<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" />

	<!-- vendor -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/vendor.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/vendor.js"></script>

	<!-- js/css -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/app.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/app.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/templates.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<!-- angular: app -->
<body layout="column" class="omni-noselect" ng-controller="CoreController" ng-cloak>
<div id="loadUser"></div>

<!-- alerts -->
<div id="make-pick-alert" class="alert alert-success remove"></div>

<!-- toolbar -->
<md-toolbar>
    <div class="md-toolbar-tools">
        <h1 ng-if="!user" layout-align-gt-sm="center">Pickems</h1>
        <h1 ng-if="user" layout-align-gt-sm="center">Welcome&nbsp;{{user.username}}</h1>

        <span flex></span>

        <md-list id="menu-container" layout="row" hide-xs hide-sm>
            <!-- menu -->
            <md-item ng-repeat="item in menu">
                <!-- item user can view -->
                <a ng-if="item.canView == true">
                    <!-- not dropdown -->
                    <md-item-content ng-if="item.type !== 'dropdown'" ng-click="$parent.navigate(item.state)">
                        <span class="inset">
                            <span>{{item.title}}</span>
                        </span>
                    </md-item-content>

                    <!-- is dropdown -->
                    <md-item-content ng-if="item.type === 'dropdown'" class="dropdown" ng-click="item.isActive = !item.isActive">
                        <span class="inset">
                            <span>{{item.title}}</span>
                        </span>

                        <div class="dropdown-container" ng-class="{active: item.isActive}" click-outside="$parent.closeThis(item)" outside-if-not="dropdown, sub-item">
                            <ul>
                                <li class="sub-item" ng-repeat="subItem in item.subMenu" ng-click="$parent.navigate(subItem.state)">{{subItem.title}}</li>
                            </ul>
                        </div>
                    </md-item-content>
                </a>

            </md-item>

            <!-- tracker -->
            <div id="menu-tracker" class="home"></div>
        </md-list>

        <ng-md-icon icon="menu" hide-gt-sm ng-click="toggleSidenav(item.type, 'left')"></ng-md-icon>
    </div>
</md-toolbar>

<div layout="row" flex>
	<!-- side navigation -->
    <md-sidenav layout="column" class="md-sidenav-left md-whiteframe-z2" md-component-id="left">
		<md-list layout="column">

            <md-item ng-click="$parent.navigate(item.state); toggleSidenav(item.type, 'left');"
                     ng-repeat="item in menu">
                <a ng-if="item.canView == true">
                    <md-item-content ng-if="item.type !== 'dropdown'" ng-click="$parent.navigate(item.state)">
                        <span class="inset">
                            <span>{{item.title}}</span>
                        </span>
                    </md-item-content>

                    <md-item-content ng-if="item.type === 'dropdown'" class="dropdown" ng-click="item.isActive = !item.isActive">
                        <span class="inset">
                            <span>{{item.title}}</span>
                        </span>

                        <div class="dropdown-container" ng-class="{active: item.isActive}" click-outside="$parent.closeThis(item)" outside-if-not="dropdown, sub-item">
                            <ul>
                                <li class="sub-item" ng-repeat="subItem in item.subMenu" ng-click="$parent.navigate(subItem.state)">{{subItem.title}}</li>
                            </ul>
                        </div>
                    </md-item-content>
                </a>
            </md-item>
		</md-list>
	</md-sidenav>

	<div layout="column" flex id="content">
		<md-content layout="column" flex class="md-padding">
			<ui-view></ui-view>
            <div id="growl-holder" growl></div>

			<!-- debug -->
			<div id="debug-holder">
				<div class="debug-content">
					<div>version: {{shortVersion}}</div>
					<div>id: {{user.id}}</div>
					<div>role: {{user.role}}</div>
					<div>isGuest: {{user.isGuest}}</div>
					<div>isActiveUser: {{user.isActiveUser}}</div>
					<div>isOpenEnrollment: <?php echo $isOpenEnrollment; ?></div>
				</div>

				<div class="debug-bg"></div>
			</div>
		</md-content>
	</div>
</div>

<!-- google analytics -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	/*ga('create', 'UA-41005632-4', 'auto');
	ga('send', 'pageview');*/
</script>
</body>
</html>
