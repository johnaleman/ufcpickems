var gulp = require('gulp');
var jscs = require('gulp-jscs');
var stylish = require('gulp-jscs-stylish');
var gulpif = require('gulp-if');
var insert = require('gulp-insert');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');
var templateCache = require('gulp-angular-templatecache');
var watch = require('gulp-watch');

//var karma = require('karma').server;

var config = {
    vendor: 'bower',
    source: 'web/themes/jam',
    dest: 'web/themes/jam/dist'
};

var maps = {
    relativePath: '../maps/',
    absolutePath: 'http://localhost:8888/workspace/ufcpickems/web/maps/'
};

var source_css = [
    '!dist/css/*.css',
    'css/main.css',
    'js/**/*.css',
    'js/**/*.scss'
];

var source_js = [
    'js/**/app.module.js', // load main module first
    'js/**/*.module.js', // load all other modules next
    'js/**/*.js',
    '!dest',
    '!js/**/*.spec.js',
    '!js/**/*.mock.js'
];

var vendor_css = [
    'bootstrap/dist/css/bootstrap.min.css',
    'angular-bootstrap/ui-bootstrap-csp.css',

    // table
    'angular-material-data-table/dist/md-data-table.min.css',

    // materialize
    'Materialize/dist/css/materialize.min.css',
    'angular-material/angular-material.min.css',

    // materialize icons
    'angular-material-icons/angular-material-icons.css',

    // angular growl
    'angular-growl/build/angular-growl.min.css',

    // normalize
    'normalize-css/normalize.css'
];

var vendor_js = [
    // jquery
    'jquery/dist/jquery.min.js',

    // bootstrap
    'bootstrap/dist/js/bootstrap.min.js',

    // angular
    'angular/angular.min.js',

    // analyatics
    'angular-google-analytics/dist/angular-google-analytics.min.js',

    // angular components
    'angular-bootstrap/ui-bootstrap.min.js',
    'angular-bootstrap/ui-bootstrap-tpls.min.js',
    'angular-animate/angular-animate.min.js',
    'angular-route/angular-route.min.js',
    'angular-resource/angular-resource.min.js',
    'angular-ui-router/release/angular-ui-router.min.js',
    'angular-aria/angular-aria.min.js',
    'angular-sanitize/angular-sanitize.min.js',
    'angular-animate/angular-animate.min.js',
    'angular-growl/build/angular-growl.min.js',
    'angular-material-data-table/dist/md-data-table.min.js',
    'angular-messages/angular-messages.min.js',
    'angular-click-outside/clickoutside.directive.js',

    // materialize
    'Materialize/dist/js/materialize.min.js',
    'angular-material/angular-material.min.js',
    'angular-material-icons/angular-material-icons.min.js'
];

gulp.task('concat-css', function() {
    return gulp.src(source_css, {cwd: config.source})
        .pipe(gulpif(/[.]scss$/, sass().on('error', sass.logError)))
        .pipe(concat('app.css'))
        .pipe(gulp.dest(config.dest + '/css'))
});

gulp.task('concat-js', function() {
    return gulp.src(source_js, {cwd: config.source})
        .pipe(concat('app.js', {newLine: ';\n'}))
        .pipe(gulp.dest(config.dest + '/js'))
});

gulp.task('templates', function() {
    return gulp.src(config.source + '/js/**/*.html')
        .pipe(templateCache({
            module: 'app',
            root: '/js'
        }))
        .pipe(gulp.dest(config.dest + '/js'));
});

gulp.task('vendor-css', function() {
    return gulp.src(vendor_css, {cwd: config.vendor})
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(config.dest + '/css'))
});

gulp.task('vendor-fonts', function() {
    return gulp.src([config.vendor + '/bootstrap/fonts/*'])
        .pipe(gulp.dest(config.dest + '/fonts'));
});

gulp.task('vendor-font', function() {
    return gulp.src([config.vendor + '/Materialize/dist/font/*/**'])
        .pipe(gulp.dest(config.dest + '/font'));
});

gulp.task('vendor-js', function() {
    return gulp.src(vendor_js, {cwd: config.vendor})
        .pipe(insert.append(';\n'))
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(config.dest + '/js'));
});

gulp.task('minify-css', ['concat-css'], function() {
    var minFileName = 'app.min.css';
    return gulp.src(config.dest + '/css/app.css')
        .pipe(sourcemaps.init())
        .pipe(minifyCss())
        .pipe(rename(minFileName))
        .pipe(sourcemaps.write(maps.relativePath))
        .pipe(replace(maps.relativePath + minFileName, maps.absolutePath + minFileName))
        .pipe(gulp.dest(config.dest + '/css'));
});

gulp.task('uglify-js', ['concat-js'], function() {
    var minFileName = 'app.min.js';
    return gulp.src(config.dest + '/js/app.js')
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename(minFileName))
        .pipe(sourcemaps.write(maps.relativePath))
        .pipe(replace(maps.relativePath + minFileName, maps.absolutePath + minFileName))
        .pipe(gulp.dest(config.dest + '/js'));
});

gulp.task('jscs', function() {
    return gulp.src(source_js, {cwd: config.source})
        .pipe(jscs({fix: true}))
        .pipe(stylish())
        .pipe(jscs.reporter())

        // Modify file in place (same dest) using gulp.js and a globbing pattern
        // source: http://stackoverflow.com/questions/29793894/how-to-set-gulp-dest-in-same-directory-as-pipe-inputs
        .pipe(gulp.dest(function(file) {
            return file.base;
        }));
});


gulp.task('build', ['concat-css', 'concat-js', 'templates', 'vendor-css', 'vendor-fonts', 'vendor-font', 'vendor-js', 'minify-css', 'uglify-js']);

gulp.task('watch', ['build'], function() {
    // Note: supposedly we aren't supposed to use gulp.start because it is not part of the gulp public API but it is
    // the most elegant way to accomplish our goal and the recommended approach by the folks at gulp-watch.
    // https://github.com/floatdrop/gulp-watch/blob/master/docs/readme.md
    function watchAndStart(glob, tasks) {
        watch(glob, function() {
            gulp.start(tasks);
        });
    }

    watchAndStart(config.source + '/**/*.html', ['templates']);
    watchAndStart([config.source + '/**/*.scss', config.source + '/**/*.css', '!' + config.source + '/dist/css/*.css'], ['concat-css']);
    //watchAndStart([config.source + '/css/main.css', config.source + '/js/**/*.scss', config.source + '/js/**/*.css'], ['concat-css']);
    watchAndStart(config.source + '/js/**/*.js', ['concat-js']);
    watchAndStart(config.vendor + '/**/*.css', ['vendor-css']);
    watchAndStart(config.vendor + '/**/*.js', ['vendor-js']);
});

/*
 gulp.task('test', ['build'], function(done) {
 karma.start({
 configFile: __dirname + '/karma.conf.js',
 singleRun: false
 //    singleRun: true
 }, done);
 });
 */

gulp.task('default', ['watch']);
